import os
import re
import time

from . import common, logger

js = common.JOB_STATES

DB_NAME = 'coalite.db'

WARNING_LIMIT = 2 # seconds

# COMMON
# =============================================================================

def execute(txn, sql, *unnamed, **named):
    params = unnamed or named # can't have both !
    ts = time.time()
    txn.execute(sql, params)
    t = time.time() - ts
    if t > WARNING_LIMIT:
        logger.debug('long query! ({:.02f}s)'.format(t))
        logger.cprint(logger.MAGENTA, 'SQL:', sql)
        logger.cprint(logger.MAGENTA, 'params:', params)


def fetchall(txn, sql, *unnamed, **named):
    """ returns a query result as a dictionaries """
    execute(txn, sql, *unnamed, **named)
    desc = tuple(col[0] for col in txn.description)
    return [ dict(zip(desc, record)) for record in txn.fetchall() ]


def fetchsingle(txn, sql, *unnamed, **named):
    """ returns a single value from a single row """
    execute(txn, sql, *unnamed, **named)
    data = txn.fetchone()
    if data:
        return data[0]


def fetchrow(txn, sql, *unnamed, **named):
    """ returns a single row as a dict """
    execute(txn, sql, *unnamed, **named)
    data = txn.fetchone()
    if data:
        return { txn.description[i][0] : value for i, value in enumerate(data) }


def fetchcolumn(txn, sql, *unnamed, **named):
    """ returns a single column query as a list """
    execute(txn, sql, *unnamed, **named)
    return [ record[0] for record in txn.fetchall() ]


# AFFINITIES
# =============================================================================


def collectAffinities(txn):
    return fetchall(txn, "SELECT id, name FROM affinities")


def updateAffinities(txn, affinities):
    for aff_id, aff_name in affinities.items():
        execute(txn, "UPDATE affinities SET name = ? WHERE id = ?",  aff_name, int(aff_id))


# JOBS
# =============================================================================


def collectJobs(txn, filters):
    conditions = []
    params = []

    def addInCondition(name, values):
        if values:
            conditions.append("{} IN ({})".format(name, ", ".join(["?"]*len(values))))
            params.extend(values)

    def addLikeCondition(name, pattern):
        operator = 'LIKE' if '%' in pattern else '='
        conditions.append("{} {} ?".format(name, operator))
        params.append(pattern)

    if "parent" in filters:
        conditions.append("j.parent = ?")
        params.append(filters["parent"])

    if "priority" in filters:
        conditions.append("j.priority {} ?".format(filters["priority"]["op"]))
        params.append(filters["priority"]["value"])

    if "user" in filters:
        addInCondition("j.user", filters["user"])

    if "state_id" in filters:
        addInCondition("j.state_id", filters["state_id"])

    if "affinity" in filters:
        conditions.append("j.affinity | ? = j.affinity")
        params.append(filters['affinity'])

    if "title" in filters:
        addLikeCondition("title", filters["title"])

    if "worker_id" in filters:
        addLikeCondition("j.worker_id", filters["worker_id"])

    if "command" in filters:
        addLikeCondition("j.command", filters["command"])

    if "start_time" in filters:
        conditions.append("j.start_time {} ?".format(filters["start_time"]["op"]))
        params.append(filters["start_time"]["value"])

    # if "dependencies" in filters:
    #     sql = "SELECT DISTINCT j.* FROM jobs AS j INNER JOIN dependencies AS d ON d.job_id = j.id"
    #     addInCondition("dependencies", filters["dependencies"])
    # else:
    sql = "SELECT j.*, w.name AS worker_name FROM jobs AS j LEFT JOIN workers AS w ON j.worker_id = w.id"

    if conditions:
        sql += " WHERE " + " AND ".join(conditions)
    return fetchall(txn, sql, *params)


def listJobChildrenIds(txn, job_id):
    return fetchcolumn(txn, "SELECT id FROM jobs WHERE parent = ?", job_id)


def collectJobChildrenStats(txn, job_id):
    sql = """
        SELECT
            state_id, is_group, total_working, total_errors, total_finished, total, start_time, duration
        FROM jobs
        WHERE parent = ?
    """
    return fetchall(txn, sql, job_id)


def listDependentJobIds(txn, job_id):
    sql = """
        SELECT jobs.id FROM jobs
        INNER JOIN dependencies AS dep ON jobs.id = dep.job_id
        WHERE dep.dep_id = ?
    """
    return fetchcolumn(txn, sql, job_id)


def getJobProps(txn, job_id, *fields):
    if fields:
        fmt_fields = '`' + '`, `'.join(fields) + '`'
    else:
        fmt_fields = '*'
    sql = "SELECT {} FROM jobs WHERE id = ?".format(fmt_fields)
    return fetchrow(txn, sql, job_id)


def getJobProp(txn, job_id, field):
    return fetchsingle(txn, "SELECT `{}` FROM jobs WHERE id = ?".format(field), job_id)


def collectDependentJobs(txn, job_id):
    sql = """
        SELECT j.* FROM jobs AS j
        INNER JOIN dependencies AS d ON j.id = d.dep_id
        WHERE d.job_id = ?
    """
    return fetchall(txn, sql, job_id)


def getChildrenDependencyIds(txn, job_id):
    sql = """
        SELECT j.id AS id, d.dep_id AS dependency
        FROM dependencies AS d
        INNER JOIN jobs AS j
        ON j.id = d.job_id
        WHERE j.parent = ?
    """
    return fetchall(txn, sql, job_id)


def getJobHierarchyData(txn, job_id):
    sql = """
        SELECT
            j.parent, j.affinity, j.priority, j.state_id,
            ifnull(p.h_depth, -1) AS parent_depth,
            ifnull(p.h_affinity, 0) AS parent_affinity,
            ifnull(p.h_priority, 0) AS parent_priority,
            ifnull(p.h_paused, 0) AS parent_paused
        FROM jobs as j
        LEFT JOIN jobs AS p ON p.id = j.parent
        WHERE j.id = ?
    """
    return fetchrow(txn, sql, job_id)


def getRunningJob(txn, worker_id):
    sql = """
        SELECT j.id FROM jobs AS j
        WHERE j.worker_id = ? AND j.state_id = ?
    """
    return fetchsingle(txn, sql, worker_id, js.WORKING)


def insertJob(txn, params):
    fields = ", ".join(params.keys())
    qmarks = ", ".join(["?"] * len(params))
    sql = """
        INSERT INTO jobs
            ({})
        VALUES
            ({})
    """.format(fields, qmarks)
    execute(txn, sql, *params.values())
    return txn.lastrowid


def moveJob(txn, job_id, parent_id):
    execute(txn, "UPDATE jobs SET parent = ? WHERE id = ?", parent_id, job_id)


def deleteJob(txn, job_id):
    execute(txn, "DELETE FROM jobs WHERE id = ?", job_id)


def clearJob(txn, job_id):
    execute(txn, 'DELETE FROM blacklist WHERE job_id = ?', job_id)


def updateJob(txn, job_id, **data):
    sql_set = ", ".join((field + " = ?") for field in data.keys())
    sql = "UPDATE jobs SET {} WHERE id = ?".format(sql_set)
    params = list(data.values())
    params.append(job_id)
    execute(txn, sql, *params)


def updateJobDependencies(txn, job_id, dependencies):
    # remove existing dependencies to job from dependencies table
    execute(txn, "DELETE FROM dependencies WHERE job_id = ?", job_id)
    # add ids to dependencies table
    for dep_id in dependencies:
        execute(txn, "INSERT INTO dependencies (job_id, dep_id) VALUES (?, ?)", job_id, dep_id)


def isJobPending(txn, job_id):
    sql = """
        SELECT 1 FROM dependencies AS d
        INNER JOIN jobs AS j ON j.id = d.dep_id
        WHERE d.job_id = ? AND j.state_id != ?
    """
    return fetchrow(txn, sql, job_id, js.FINISHED) is not None


def pickJob(txn, worker_id):
    sql = """
        WITH
        -- get jobs that this worker can't pick anymore
        exclude AS (
            SELECT b.job_id FROM blacklist AS b WHERE b.worker_id = :worker_id
        ),
        -- get jobs candidates
        candidates AS (
            SELECT j.* FROM jobs AS j
            WHERE NOT j.is_group
            AND NOT j.h_paused
            AND j.state_id IN (:waiting, :error)
            AND j.id NOT IN exclude
        )
        -- select job candidates that match the 1st worker affinity list
        SELECT j.*, 0 AS aff_order FROM candidates AS j
        INNER JOIN workers AS w
            ON j.h_affinity > 0 AND j.h_affinity & w.affinity = j.h_affinity
        WHERE w.id = :worker_id
        UNION ALL
        -- select job candidates that match the 1st + 2nd worker affinity lists
        SELECT j.*, 1 AS aff_order FROM candidates AS j
        INNER JOIN workers AS w
            ON j.h_affinity > 0 AND j.h_affinity & (w.affinity | w.affinity2) = j.h_affinity
        WHERE w.id = :worker_id
        -- select job candidates that don't have affinity
        UNION ALL
        SELECT j.*, 2 AS aff_order FROM candidates AS j
        WHERE j.h_affinity = 0
        -- return the 1st candidate matching one of those "affinity conditions" in order, then by priority, then by ID
        ORDER BY aff_order ASC, j.h_priority DESC, j.id ASC
        LIMIT 1
    """
    return fetchrow(txn, sql, waiting=js.WAITING, error=js.ERROR, worker_id=worker_id)


def insertAttempt(txn, job_id, worker_id, state_id, start_time):
    sql = """
        INSERT INTO attempts
            (job_id, worker_id, state_id, start_time)
        VALUES
            (?, ?, ?, ?)
    """
    execute(txn, sql, job_id, worker_id, state_id, start_time)
    return txn.lastrowid



def collectAttempts(txn, job_id):
    sql = """
        SELECT a.*, w.name AS worker_name FROM attempts AS a
        INNER JOIN workers AS w ON a.worker_id = w.id
        WHERE job_id = ?
    """
    return fetchall(txn, sql, job_id)


def setAttemptState(txn, attempt_id, state_id):
    sql = """
        UPDATE attempts SET state_id = ?
        WHERE id = ?
    """
    execute(txn, sql, state_id, attempt_id)


# WORKERS
# =============================================================================


def collectWorkers(txn):
    return fetchall(txn, "SELECT * FROM workers")


def getWorkerId(txn, worker_name):
    return fetchsingle(txn, "SELECT id FROM workers WHERE name = ?", worker_name)


def getWorkerActivity(txn, worker_id):
    return fetchrow(txn, "SELECT is_working, is_paused, schedule FROM workers WHERE id = ?", worker_id)


def getWorkerAffinities(txn, worker_id):
    return fetchrow(txn, "SELECT affinity, affinity2 FROM workers WHERE id = ?", worker_id)


def insertWorker(txn, name):
    execute(txn, "INSERT INTO workers (name) VALUES (?)", name)
    return txn.lastrowid


def updateWorker(txn, worker_id, **data):
    sql_set = ", ".join('`{}` = ?'.format(set_field) for set_field in data.keys())
    sql = "UPDATE workers SET {} WHERE id = ?".format(sql_set)
    params = list(data.values())
    params.append(worker_id)
    execute(txn, sql, *params)


def deleteWorker(txn, worker_id):
    execute(txn, "DELETE FROM workers WHERE id = ?", worker_id)


def blacklistWorker(txn, worker_id, job_id):
    sql = ''' INSERT INTO blacklist (job_id, worker_id) VALUES (?, ?) '''
    execute(txn, sql, job_id, worker_id)


def clearWorker(txn, worker_id):
    '''
        Clear a worker blacklist: the worker will be able to pick the jobs again.
    '''
    sql = ''' DELETE FROM blacklist WHERE worker_id = ? '''
    execute(txn, sql, worker_id)


def collectBlacklistedWorkers(txn, job_id):
    '''
        Collect "blacklisted" workers for a given job.
        "blacklisted" workers can't pick the job
    '''
    sql = """ SELECT worker_id FROM blacklist WHERE job_id = ? """
    return fetchcolumn(txn, sql, job_id)


def collectWithdrawnJobs(txn, worker_id):
    '''
        Collect jobs "withdrawn" from a worker.
        "withdrawn" jobs can't be picked by the worker.
    '''
    sql = """ SELECT job_id FROM blacklist WHERE worker_id = ? """
    return fetchcolumn(txn, sql, job_id)


def isWorkerRunningJob(txn, worker_id, job_id):
    sql = """
        SELECT 1 FROM workers as w
        INNER JOIN jobs AS j ON j.worker_id = w.id
        WHERE j.id = ?
          AND j.state_id = ?
          AND j.h_paused = 0
          AND w.last_job = ?
          AND w.id = ?
    """
    return fetchsingle(txn, sql, job_id, js.WORKING, job_id, worker_id) == 1


# MISC
# =============================================================================


def listJobsUsers(txn):
    return fetchcolumn(txn, "SELECT DISTINCT user FROM jobs ORDER BY user")


def listJobsStates(txn):
    return fetchcolumn(txn, "SELECT DISTINCT state_id FROM jobs")


# def mktime(db_timestamp):
#     t = time.strptime(db_timestamp, '%Y-%m-%d %H:%M:%S')
#     return time.mktime(t)


def init():
    import sqlite3
    # create tables and indexes
    sql_path = os.path.join(os.path.dirname(__file__), 'db_sqlite.sql')
    with open(sql_path, 'r') as f:
        sql = f.read()
    conn = sqlite3.connect(DB_NAME)
    cursor = conn.cursor()
    cursor.executescript(sql)
    # populate 'affinities' table
    for i in range(64):
        cursor.execute("INSERT INTO affinities VALUES (?, '')", [i+1])
    conn.commit()
    conn.close()