SERVER_MESSAGE = b'rexxar'
CLIENT_MESSAGE = b'misha'
SQLITE_DB = 'coalite.db'


class Enum():
	''' custom "enum" for Coalite '''
	def __init__(self, names):
		self._names = names
		for i, name in enumerate(names):
			setattr(self, name, i)

	def getname(self, i):
		try:
			return self._names[i]
		except:
			return 'UNDEFINED'

	def getvalue(self, name):
		return getattr(self, name, 0)


JOB_STATES = Enum([
	'WAITING',
	'WORKING',
	'PAUSED',
	'FINISHED',
	'ERROR',
	'CANCELED',
	'PENDING',
	'UNFINISHED',	# special status for attempts stopped prematurely
])


RUN_STATES = Enum([
	'OK',
	'WORKER_PAUSED',		# worker is paused
	'JOB_DELETED',			# job not found in database
	'JOB_ENDED',			# job has been marked as finished
	'JOB_PAUSED',			# job hierarchy is paused
	'WRONG_STATE',			# job state is {}'.format(job['state'])
	'WRONG_WORKER',			# job is already assigned to {}'.format(job['worker'])
	'WORKER_MISSING',		# worker is missing in database
])

LOGFILTER_TYPES = Enum([
	'NULL',
	# filter types
	'STOP',
	'PROGRESS',
	# progress types
	'MAPPING',
	'MILESTONE',
	# stop types
	'ERROR',
	'CANCEL',
])

# job parameters (with default values)
JOB_PARAMS = {
    'parent': 0,
    'title': '',
    'command': '',
    'dir': '',
    'environment': '',
    'state': 'WAITING',
    'timeout': 0,
    'priority': 50,
    'affinity': '',
    'user': '',
    'url': '',
    'dependencies': '',
    'is_group': 0,
    'is_subtask': 0,
    'subtask_start': 0,
    'subtask_end': 0,
}

# job parameters (no default values)
WORKER_PARAMS = [
	"description",
	"affinity",
	"affinity2",
	"schedule",
]