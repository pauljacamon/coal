import re
import bisect

from PySide2 import QtCore, QtGui


class CSSPropertiesMixin():
    """
        Adds some CSS "qproperty" properties to a Qt widget
    """

    _bordercolor = QtGui.QColor()
    _namedcolors = {}
    _heat_pos = []
    _heat_colors = []
    _priority_pos = []
    _priority_colors = []


    def _get_bordercolor(self):
        return self._bordercolor


    def _set_bordercolor(self, value):
        self._bordercolor = QtGui.QColor(value)


    def _get_namedcolors(self):
        return self._namedcolors


    def _set_namedcolors(self, value):
        regex = re.compile(r'([A-Z]+)\s*=\s*(#[0-9A-Fa-f]+)')
        self._namedcolors = { m.group(1) : QtGui.QColor(m.group(2)) for m in regex.finditer(value) }


    def _get_heatramp(self):
        return self._heat_pos, self._heat_colors


    def _set_heatramp(self, value):
        self._parse_gradient(value, self._heat_pos, self._heat_colors)


    def _get_priorityramp(self):
        return self._priority_pos, self._priority_colors


    def _set_priorityramp(self, value):
        self._parse_gradient(value, self._priority_pos, self._priority_colors)


    def _parse_gradient(self, value, positions, colors):
        del positions[:]
        del colors[:]
        pairs = []

        regex = re.compile(r'([0-9]+)%\s*=\s*(#[0-9A-Fa-f]+)')

        for m in regex.finditer(value):
            pairs.append(((float(m.group(1))/100), QtGui.QColor(m.group(2))))

        for pos, color in sorted(pairs, key=lambda x: x[0]):
            positions.append(pos)
            colors.append(color)


    def _value_at(self, pos, positions, colors):
        if not positions:
            return QtGui.QColor()

        i = bisect.bisect_left(positions, pos)

        if i == 0:
            # return first color
            return colors[0]

        if i == len(positions):
            # return last color
            return colors[-1]

        if pos == positions[i]:
            # return exact match
            return colors[i]

        l_pos, l_color = positions[i-1], colors[i-1]
        r_pos, r_color = positions[i], colors[i]

        # calculate interpolation factor `a` between left and right positions
        f = (pos - l_pos) / (r_pos-l_pos)
        # calculate interpolated color between left and right colors with `a`
        return QtGui.QColor(
            l_color.red() * (1-f) + r_color.red() * f,
            l_color.green() * (1-f) + r_color.green() * f,
            l_color.blue() * (1-f) + r_color.blue() * f,
        )


    bordercolor = QtCore.Property(str, _get_bordercolor, _set_bordercolor)
    namedcolors = QtCore.Property(str, _get_namedcolors, _set_namedcolors)
    heatramp = QtCore.Property(str, _get_heatramp, _set_heatramp)
    priorityramp = QtCore.Property(str, _get_priorityramp, _set_priorityramp)


    def namedColor(self, name, default=QtCore.Qt.gray):
        return self._namedcolors.get(name, default)


    def heatColorAt(self, f):
        if self._heat_colors:
            return self._value_at(f, self._heat_pos, self._heat_colors)
        return QtCore.Qt.gray


    def priorityColorAt(self, f):
        if self._priority_colors:
            return self._value_at(f, self._priority_pos, self._priority_colors)
        return QtCore.Qt.gray

