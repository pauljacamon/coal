import os, sys
import random
import subprocess

from PySide2.QtGui import *
from PySide2.QtCore import *
from PySide2.QtWidgets import *

from .. import logger

from . import mainwindow_ui, models_views, editors, constants

from .csspropertiesmixin import CSSPropertiesMixin


AUTO_REFRESH_INTERVAL = 2000

# filters that will be automatically wrapped by '%' (SQL `LIKE` wildcards)
LIKE_FILTERS = [
    'title',
    'worker',
]

class ColorTest(QDialog, CSSPropertiesMixin):

    def paintEvent(self, event):

        painter = QPainter(self)

        painter.setPen(Qt.NoPen)

        rect = self.rect()

        for pos, col in zip(*self.heatramp):
            r = rect.adjusted(rect.width() * pos*.9, 0, 0, 0)
            painter.setPen(Qt.NoPen)
            painter.setBrush(col)
            painter.drawRect(r)
            painter.setPen(0)
            painter.drawText(r, 0, str(pos))

        rect.adjust(0, rect.height()/3, 0, 0)

        for pos, col in zip(*self.priorityramp):
            r = rect.adjusted(rect.width() * pos*.9, 0, 0, 0)
            painter.setPen(Qt.NoPen)
            painter.setBrush(col)
            painter.drawRect(r)
            painter.setPen(0)
            painter.drawText(r, 0, str(pos))

        rect.adjust(0, rect.height()/2, 0, 0)

        n = float(len(self.namedcolors))
        for i, [name, color] in enumerate(self.namedcolors.items()):
            r = rect.adjusted(rect.width() * i/n, 0, 0, 0)
            painter.setPen(Qt.NoPen)
            painter.setBrush(color)
            painter.drawRect(r)
            painter.setPen(0)
            painter.drawText(r, 0, name)



    def sizeHint(self):
        return QSize(400, 200)


class MainWindow(QMainWindow, mainwindow_ui.Ui_MainWindow):

    JOBS_TAB = 0
    WORKERS_TAB = 1
    LOG_TAB = 2
    AFFINITIES_TAB = 3

    TITLE = 'Coalite Monitor'

    def __init__(self, api, parent=None):

        self.api = api

        self.cut_jobs = []      # current cut (pending re-parenting (moved)) jobs ids
        self.job_id = 0         # current job id
        self.current_attempts = {}
        self.hold_attempts_update = -1
        self.job_table_resized = False
        self.worker_table_resized = False
        self.auto_refresh_paused = False


        self.settings = QSettings("jungler", "coalite")
        self.job_header_state = self.settings.value("jobHeaderState")
        self.worker_header_state = self.settings.value("workerHeaderState")

        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle(self.TITLE)
        self.setWindowIcon(QIcon(":/window-icon.png"))
        # self.centralwidget.layout().setContentsMargins(10,10,10,10)

        self.textEdit_log.setLineWrapMode(QPlainTextEdit.NoWrap)
        self.textEdit_log.setReadOnly(True)

        self.setWindowTitle("Coalite Monitor")
        # self.lbl_logo.setPixmap(QPixmap(":/logo-dark.png"))

        self.tabWidget_main.setCurrentIndex(self.JOBS_TAB)
        self.tabBar_jobHierarchy.setDrawBase(False)

        self.btn_pasteJobs.setEnabled(False)

        self.jobModel = models_views.JobModel(self.cut_jobs, self)
        self.tableView_jobs.setModel(self.jobModel)
        self.tableView_jobs.menuRequested[QPoint].connect(self.showJobMenu)
        self.tableView_jobs.doubleClicked[QModelIndex].connect(self.openJob)

        self.workerModel = models_views.WorkerModel(self)
        self.tableView_workers.setModel(self.workerModel)
        self.tableView_workers.menuRequested[QPoint].connect(self.showWorkerMenu)

        self.affModel = models_views.AffinityModel(self)
        self.tableView_affinities_A.setModel(self.affModel)
        self.tableView_affinities_A.setRange(1,32)
        self.tableView_affinities_B.setModel(self.affModel)
        self.tableView_affinities_B.setRange(33,64)

        self.refreshTimer = JiggleTimer(self)
        self.refreshTimer.setInterval(AUTO_REFRESH_INTERVAL)
        self.refreshTimer.timeout.connect(self.autoRefresh)

        self.createJobTab(0, 'Root')

        # EVENTS
        self.tabWidget_main.currentChanged.connect(self.mainTabChanged)
        self.tabBar_jobHierarchy.currentChanged.connect(self.jobsTabChanged)
        # JOBS BUTTONS
        self.btn_resetJobs.clicked.connect(self.resetJobs)
        self.btn_resetJobErrors.clicked.connect(self.resetErrors)
        self.btn_startJobs.clicked.connect(self.resumeJobs)
        self.btn_pauseJobs.clicked.connect(self.pauseJobs)
        self.btn_newJob.clicked.connect(self.newJob)
        self.btn_editJobs.clicked.connect(self.editJobs)
        self.btn_cutJobs.clicked.connect(self.cutJobs)
        self.btn_pasteJobs.clicked.connect(self.pasteJobs)
        self.btn_deleteJobs.clicked.connect(self.deleteJobs)
        self.btn_filterJobs.clicked.connect(self.initFilterJobs)
        # WORKERS BUTTONS
        self.btn_stopWorkers.clicked.connect(self.stopWorkers)
        self.btn_startWorkers.clicked.connect(self.startWorkers)
        self.btn_editWorkers.clicked.connect(self.editWorkers)
        self.btn_stopWorkers.clicked.connect(self.stopWorkers)
        self.btn_forgetWorkers.clicked.connect(self.forgetWorkers)
        # LOG ATTEMPTS COMBOBOX
        self.cbb_attempts.currentIndexChanged.connect(self.resetLog)
        self.affModel.affinityUpdateRequested.connect(self.updateAffinity)

        self.api.bind(self.api.getInfos(), self.setupModels, self.logError)

        self.refresh()


    def logError(self, message):
        logger.error(message)


    def setupModels(self, infos):
        self.jobModel.setAttemptsLimit(infos['attempts_limit'])
        self.workerModel.setWorkersTimeout(infos['worker_timeout'])


    def shutdown(self):
        # save headers states
        if self.job_table_resized:
            self.settings.setValue("jobHeaderState", self.tableView_jobs.horizontalHeader().saveState())
        if self.worker_table_resized:
            self.settings.setValue("workerHeaderState", self.tableView_workers.horizontalHeader().saveState())


    def event(self, e):
        # call refresh() when main window is maximised
        if e.type() == QEvent.ActivationChange:
            if self.auto_refresh_paused:
                # if refresh was paused and state has changed, force refresh
                index = self.tabWidget_main.currentIndex()
                self.refresh()
            self.auto_refresh_paused = self.windowState() & Qt.WindowMinimized > 0
        return super(MainWindow, self).event(e)


    # def resizeEvent(self, e):
    #     self.wgt_loading.resize(self.geometry().size())
    #     super(MainWindow, self).resizeEvent(e)


    def mainTabChanged(self, index):
        self.refresh()


    def refresh(self, assert_tab_index=None, from_auto_refresh=False):
        # prevent useless auto-refresh if window is 'minimized'
        if from_auto_refresh and self.auto_refresh_paused:
            return
        # prevent refresh of a tab if not actual displayed tab
        index = self.tabWidget_main.currentIndex()
        if assert_tab_index is not None and index != assert_tab_index:
            return
        # prevent refresh queries to stack and overload the server
        if from_auto_refresh and self.api.pending_reply():
            # logger.info('server is overloaded!')
            return
        # refresh jobs
        if index == self.JOBS_TAB:
            if not from_auto_refresh:
                self.jobModel.clear()
            parent_id = self.currentJobId()
            params = self.jobModel.filters(parent_id).copy()
            params['parent'] = parent_id
            for name in params:
                if name in LIKE_FILTERS:
                    params[name] = '%' + params[name] + '%'
            self.api.bind(self.api.collectJobs(params), self.updateJobs, self.logError)
        # refresh workers
        elif index == self.WORKERS_TAB:
            # if not from_auto_refresh:
            #     self.workerModel.clear()
            self.api.bind(self.api.collectWorkers(), self.updateWorkers, self.logError)

        # refresh attempts and logs
        elif index == self.LOG_TAB and self.job_id > 0:
            if self.hold_attempts_update == self.job_id:
                # attempts update for this job has been paused, only refresh log
                self.refreshLog()
            elif self.cbb_attempts.view().isVisible():
                # don't collect attempts while combobox is unfolded
                self.refreshLog()
            else:
                self.api.bind(self.api.collectAttempts(self.job_id), self.updateAttempts, self.logError)

        # refresh affinities
        elif index == self.AFFINITIES_TAB:
            self.api.bind(self.api.collectAffinities(), self.updateAffinities, self.logError)

        if not from_auto_refresh:
            # reset autorefresh timer
            self.refreshTimer.start()


    def refreshTab(self, index):
        self.refresh(assert_tab_index=index)


    def autoRefresh(self):
        self.refresh(from_auto_refresh=True)


    ## JOBS ####################################################################


    def updateJobs(self, jobs):
        parent_id = self.currentJobId()
        if jobs and jobs[0]['parent'] != parent_id:
            # request result is not about the current job tab (anymore)
            return
        self.jobModel.load(parent_id, jobs)
        if not self.job_table_resized:
            if self.job_header_state:
                self.tableView_jobs.horizontalHeader().restoreState(self.job_header_state)
            else:
                self.tableView_jobs.resizeColumnsToContents()
            self.job_table_resized = True


    def createJobTab(self, job_id, job_name):
        index = self.tabBar_jobHierarchy.addTab(job_name)
        self.tabBar_jobHierarchy.setTabData(index, job_id)
        self.tabBar_jobHierarchy.setCurrentIndex(index)


    def jobsTabChanged(self, index):
        # remove tabs right to the current one if any (the previous children of the current group)
        for i in reversed(range(index+1, self.tabBar_jobHierarchy.count())):
            self.tabBar_jobHierarchy.removeTab(i)
        # update focused tab
        self.refreshTab(self.JOBS_TAB)


    def currentJobId(self):
        index = self.tabBar_jobHierarchy.currentIndex()
        return self.tabBar_jobHierarchy.tabData(index)


    def showJobMenu(self, pos):
        # job menu
        multi = self.tableView_jobs.multiSelected()
        s = 's' if multi else ''
        menu = QMenu(self)
        menu.addAction('Edit job'+s+'...', self.editJobs)
        if not multi:
            menu.addAction('Duplicate job...', self.copyJob)
        menu.addAction('Take job'+s, self.cutJobs)
        menu.addSeparator()
        menu.addAction('Resume job'+s, self.resumeJobs)
        menu.addAction('Pause job'+s, self.pauseJobs)
        menu.addSeparator()
        menu.addAction('Reset job'+s, self.resetJobs)
        menu.addAction('Reset job'+s+' errors', self.resetErrors)
        menu.addAction('Finish job'+s, self.finishJobs)
        menu.addSeparator()
        menu.addAction('Delete job'+s, self.deleteJobs)
        menu.addSeparator()
        menu.addAction("Copy to clipboard", self.copyJobsData)
        menu.exec_(pos)


    def initNewJob(self, refjob):

        parent_id = self.currentJobId()

        def affinities_fetched(affinities):
            self.showNewJobDialog(parent_id, affinities, refjob)

        self.api.bind(self.api.collectAffinities(), affinities_fetched, self.logError)


    def showNewJobDialog(self, parent_id, affinities, refjob):

        def job_sent(job_ids):
            self.refreshTab(self.JOBS_TAB)

        dlg = editors.NewJobDialog(affinities, refjob, self)
        if dlg.exec_():
            job_params = dlg.values()
            job_params['parent'] = parent_id
            self.api.bind(self.api.newJobs([job_params]), job_sent, self.logError)


    def initFilterJobs(self):

        parent_id = self.currentJobId()

        def affinities_fetched(affinities):

            def users_fetched(users):
                self.showFilterJobDialog(parent_id, affinities, users)

            self.api.bind(self.api.collectUsers(), users_fetched, self.logError)

        self.api.bind(self.api.collectAffinities(), affinities_fetched, self.logError)


    def showFilterJobDialog(self, parent_id, affinities, users):

        filters = self.jobModel.filters(parent_id)

        dlg = editors.FilterJobDialog(affinities, users, filters, self)
        if dlg.exec_():
            self.jobModel.setFilters(dlg.filters())
            self.refreshTab(self.JOBS_TAB)


    def newJob(self):
        self.initNewJob(None)


    def copyJob(self):
        jobs = self.tableView_jobs.selectedItems()
        assert len(jobs) == 1, "a single job should be selected"
        self.initNewJob(jobs[0])


    def editJobs(self):
        jobs = self.tableView_jobs.selectedItems()
        if not jobs:
            return

        def affinities_fetched(affinities):
            dlg = editors.ModifyJobDialog(affinities, jobs, self)
            if dlg.exec_():
                settings = dlg.values()
                data = { j['id']: settings for j in jobs }
                reply = self.api.editJobs(data)
                self.api.bind(reply, lambda: self.refreshTab(self.JOBS_TAB), self.logError)

        self.api.bind(self.api.collectAffinities(), affinities_fetched, self.logError)


    def cutJobs(self):
        del self.cut_jobs[:]
        self.cut_jobs.extend([j["id"] for j in self.tableView_jobs.selectedItems()])
        self.btn_pasteJobs.setEnabled(len(self.cut_jobs) > 0)
        self.tableView_jobs.viewport().update()


    def pasteJobs(self):
        parent_id = self.currentJobId()
        if parent_id < 0:
            return

        def job_cut():
            del self.cut_jobs[:]
            self.btn_pasteJobs.setEnabled(False)
            self.refreshTab(self.JOBS_TAB)

        reply = self.api.moveJobs(self.cut_jobs, parent_id)
        self.api.bind(reply, job_cut, self.logError)


    def actionOnJobs(self, api_method):
        jobs = self.tableView_jobs.selectedItems()
        reply = api_method([j["id"] for j in jobs])
        self.api.bind(reply, lambda: self.refreshTab(self.JOBS_TAB), self.logError)


    def resetJobs(self):
        self.actionOnJobs(self.api.resetJobs)


    def resetErrors(self):
        self.actionOnJobs(self.api.resetErrors)


    def finishJobs(self):
        self.actionOnJobs(self.api.finishJobs)


    def pauseJobs(self):
        self.actionOnJobs(self.api.pauseJobs)


    def resumeJobs(self):
        self.actionOnJobs(self.api.resumeJobs)


    def deleteJobs(self):
        if self.tableView_jobs.hasSelection():
            warning = "Are you sure you want to delete {} job(s)?".format(len(self.tableView_jobs.selectedItems()))
            btn = QMessageBox.warning(self, self.TITLE, warning, QMessageBox.Yes|QMessageBox.Cancel)
            if btn == QMessageBox.Yes:
                self.actionOnJobs(self.api.deleteJobs)


    def copyJobsData(self):
        lines = []
        jobs = self.tableView_jobs.selectedItems()
        if not jobs:
            return

        keys = list(jobs[0].keys())
        # sort fields (monitor fields then remainder)
        fields = [ k for k in constants.JOB_COLUMNS if k in keys ] # monitor known fields
        fields.extend( k for k in keys if k not in constants.JOB_COLUMNS ) # server data remainder

        lines.append('\t'.join(fields))
        for job in jobs:
            lines.append('\t'.join(str(job[k]) for k in fields))

        clipboard = QGuiApplication.clipboard()
        clipboard.setText('\n'.join(lines))


    def openJob(self, index):
        job = index.data(Qt.UserRole)
        if job['is_group']:
            self.openGroup(job)
        else:
            self.openLog(job)


    def openLog(self, job):
        # self.lbl_log.setText("job #{} - {}".format(job["id"], job["title"]))
        h_names = [ self.tabBar_jobHierarchy.tabText(i) for i in range(1, self.tabBar_jobHierarchy.count()) ]
        h_names.append(job["title"])
        self.lbl_log.setText(' / '.join(h_names))
        self.job_id = job["id"]
        self.tabWidget_main.setCurrentIndex(2)


    def openGroup(self, job):
        self.createJobTab(job["id"], job["title"])

    ## WORKERS #################################################################

    def updateWorkers(self, workers):
        self.workerModel.load(workers)
        if not self.worker_table_resized:
            if self.worker_header_state:
                self.tableView_workers.horizontalHeader().restoreState(self.worker_header_state)
            else:
                self.tableView_workers.resizeColumnsToContents()
            self.worker_table_resized = True


    def showWorkerMenu(self, pos):
        workers = self.tableView_workers.selectedItems()
        s = "s" if len(workers) != 1 else ""
        menu = QMenu(self)
        menu.addAction("Edit worker"+s, self.editWorkers)
        menu.addSeparator()
        menu.addAction("Start worker"+s, self.startWorkers)
        menu.addAction("Stop worker"+s, self.stopWorkers)
        menu.addSeparator()
        menu.addAction("Clear worker"+s, self.clearWorkers)
        menu.addSeparator()
        menu.addAction("Forget worker"+s, self.forgetWorkers)
        menu.addSeparator()
        if len(workers) == 1:
            client_name = workers[0]['name'].partition('~')[0]
            a = menu.addAction("Remote Connect to {}".format(client_name), lambda: self.remoteConnect(client_name))
        menu.addSeparator()
        menu.addAction("Copy to clipboard", self.copyWorkersData)
        menu.exec_(pos)


    def editWorkers(self):
        workers = self.tableView_workers.selectedItems()
        if not workers:
            return

        def affinities_fetched(affinities):
            dlg = editors.ModifyWorkerDialog(affinities, workers, self)
            if dlg.exec_():
                data = dlg.values()
                reply = self.api.editWorkers({w['id']: data for w in workers})
                self.api.bind(reply, lambda: self.refreshTab(self.WORKERS_TAB), self.logError)

        self.api.bind(self.api.collectAffinities(), affinities_fetched, self.logError)


    def actionOnWorkers(self, api_method):
        workers = self.tableView_workers.selectedItems()
        reply = api_method([w['id'] for w in workers])
        self.api.bind(reply, lambda: self.refreshTab(self.WORKERS_TAB), self.logError)



    def startWorkers(self):
        self.actionOnWorkers(self.api.startWorkers)


    def stopWorkers(self):
        self.actionOnWorkers(self.api.stopWorkers)


    def clearWorkers(self):
        self.actionOnWorkers(self.api.clearWorkers)


    def forgetWorkers(self):
        if self.tableView_workers.hasSelection():
            workers = self.tableView_workers.selectedItems()
            warning = ( "Are you sure you want selected worker(s) to be forgotten?\n"
                        "Make sure that workers are OFFLINE before erasing their settings,\n"
                        "or they will automatically reconnect to the server as new workers.\n" )
            btn = QMessageBox.warning(self, "Delete Worker(s)", warning, QMessageBox.Yes|QMessageBox.Cancel)
            if btn == QMessageBox.Yes:
                reply = self.api.deleteWorkers([w['id'] for w in workers])
                self.api.bind(reply, lambda: self.refreshTab(self.WORKERS_TAB), self.logError)


    def remoteConnect(self, name):
        subprocess.Popen(['mstsc', '/v', name])


    def copyWorkersData(self):
        lines = []
        workers = self.tableView_workers.selectedItems()
        if not workers:
            return

        keys = list(workers[0].keys())
        # sort fields (monitor fields then remainder)
        fields = [ k for k in constants.WORKER_COLUMNS if k in keys ] # monitor known fields
        fields.extend( k for k in keys if k not in constants.WORKER_COLUMNS ) # server data remainder

        lines.append('\t'.join(fields))
        for wrk in workers:
            lines.append('\t'.join(str(wrk[k]) for k in fields))

        clipboard = QGuiApplication.clipboard()
        clipboard.setText('\n'.join(lines))


    ## LOG #####################################################################

    def getCurrentAttemptData(self):
        return self.current_attempts.get(self.cbb_attempts.currentData())


    def updateAttempts(self, attempts):

        # reset `hold_attempts_update` id
        self.hold_attempts_update = -1

        # get previous data befor clearing
        previous = self.getCurrentAttemptData()
        prev_count = len(self.current_attempts)

        self.current_attempts.clear()

        if not attempts:
            self.cbb_attempts.clear()
            self.textEdit_log.clear()
            return

        previous_index = None

        # block combobox signals while updating it
        self.cbb_attempts.blockSignals(True)
        self.cbb_attempts.clear()

        # FILL COMBOBOX WITH NEW ATTEMPTS
        for i, attempt in enumerate(sorted(attempts, key=lambda x: -x['id'])):
            title = 'attempt #{}/{} : {worker_name} ({state})'.format(len(attempts)-i, len(attempts), **attempt)
            if previous and previous['id'] == attempt['id']:
                # if attempt has the same id than previous, set its log
                # position and store its index to restore later
                attempt['log_pos'] = previous['log_pos']
                previous_index = i
            else:
                attempt['log_pos'] = 0
            self.cbb_attempts.addItem(title, userData=attempt['id'])
            self.current_attempts[attempt['id']] = attempt

        if previous_index is None:
            self.textEdit_log.clear()
        else:
            # stay on current log (restore index)
            self.cbb_attempts.setCurrentIndex(previous_index)
            # check if attempts count changed
            if prev_count < len(attempts):
                # some attempts were added since last update
                self.hold_attempts_update = self.job_id
                btn = QMessageBox.question(self, self.TITLE, 'Attempts list has been updated, switch to the last attempt ?')
                if btn is QMessageBox.Yes:
                    # unpause attempt list updates
                    self.hold_attempts_update = -1
                    # switch to last attempt log
                    self.cbb_attempts.setCurrentIndex(0)
                    self.textEdit_log.clear()

        # restore combobox signals
        self.cbb_attempts.blockSignals(False)

        self.refreshLog()


    def refreshLog(self, reset=False):

        attempt = self.getCurrentAttemptData()

        if attempt is None:
            self.textEdit_log.clear()
            return

        if reset:
            self.textEdit_log.clear()
            attempt['log_pos'] = 0

        # if some text is selected, don't refresh
        # if not auto or not self.textEdit_log.textCursor().hasSelection():

        # fetch log
        reply = self.api.getLog(attempt['job_id'], attempt['id'], attempt['log_pos'])
        self.api.bind(reply, self.updateLog, self.logError)


    def resetLog(self, index):
        # re-enable attempts update
        self.hold_attempts_update = -1
        # refresh log (full)
        self.refreshLog(reset=True)


    def updateLog(self, log_data):

        attempt = self.getCurrentAttemptData()

        if not attempt:
            # combobox is empty
            return

        if log_data is None:
            # log file doesn't exist
            self.textEdit_log.clear()
            return

        if log_data['job_id'] != attempt['job_id'] or log_data['attempt_id'] != attempt['id']:
            # not the currently displayed attempt, ignore data
            self.textEdit_log.clear()
            return

        # logger.debug('refreshLog', ' - attempt_id:', attempt['id'], ' - chunk:', attempt['log_pos'], '->', log_data['pos'])

        if log_data['pos'] == attempt['log_pos']:
            # same position, no new log
            return

        # store server seek value for later query
        attempt['log_pos'] = log_data['pos']

        # insert log chunk at the end (faster by splitting into chunks)
        # i = 0
        # size = 1024
        # while chunk := log_data['log'][i*size:(i+1)*size]:
        #     self.textEdit_log.moveCursor(QTextCursor.End)
        #     self.textEdit_log.insertPlainText(chunk)
        #     i += 1
        self.textEdit_log.moveCursor(QTextCursor.End)
        self.textEdit_log.insertPlainText(log_data['log'])

        # scroll to bottom
        self.textEdit_log.moveCursor(QTextCursor.End)


    ## AFFINITIES ##############################################################


    def updateAffinities(self, affinities):
        self.affModel.load(affinities)
        self.tableView_affinities_A.resizeColumnToContents(0)
        self.tableView_affinities_B.resizeColumnToContents(0)


    def updateAffinity(self, id, name):
        reply = self.api.setAffinities({id: name})
        self.api.bind(reply, lambda: self.refreshTab(self.AFFINITIES_TAB), self.logError)


class JiggleTimer(QTimer):
    """ A QTimer which apply a variation on each interval """
    def __init__(self, parent=None, variation=0.1):
        super(JiggleTimer, self).__init__(parent)
        self._variation = min(max(variation, 0), 2)
        self._interval = 0


    def start(self, msec=None):
        if msec is not None:
            self._interval = msec
        super(JiggleTimer, self).start(self.shuffle())


    def setInterval(self, msec):
        self._interval = msec


    def timerEvent(self, event):
        # trigger event
        super(JiggleTimer, self).timerEvent(event)
        # shuffle interval
        super(JiggleTimer, self).setInterval(self.shuffle())


    def shuffle(self):
        return self._interval * (1.0 + (random.random()-0.5)*self._variation)

