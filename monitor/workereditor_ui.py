# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'workereditor.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from .schedulewidget import ScheduleWidget


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(382, 447)
        self.verticalLayout_2 = QVBoxLayout(Dialog)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")
        self.edt_desc = QLineEdit(Dialog)
        self.edt_desc.setObjectName(u"edt_desc")

        self.gridLayout.addWidget(self.edt_desc, 0, 1, 1, 1)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.lst_affinities1 = QListWidget(Dialog)
        self.lst_affinities1.setObjectName(u"lst_affinities1")
        self.lst_affinities1.setSelectionMode(QAbstractItemView.ExtendedSelection)

        self.horizontalLayout_2.addWidget(self.lst_affinities1)

        self.widget_aff1_mode = QWidget(Dialog)
        self.widget_aff1_mode.setObjectName(u"widget_aff1_mode")
        self.verticalLayout_5 = QVBoxLayout(self.widget_aff1_mode)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.verticalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.verticalSpacer_2 = QSpacerItem(56, 22, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_5.addItem(self.verticalSpacer_2)

        self.rdb_aff1_set = QRadioButton(self.widget_aff1_mode)
        self.rdb_aff1_set.setObjectName(u"rdb_aff1_set")
        self.rdb_aff1_set.setChecked(True)

        self.verticalLayout_5.addWidget(self.rdb_aff1_set)

        self.rdb_aff1_add = QRadioButton(self.widget_aff1_mode)
        self.rdb_aff1_add.setObjectName(u"rdb_aff1_add")

        self.verticalLayout_5.addWidget(self.rdb_aff1_add)

        self.rdb_aff1_remove = QRadioButton(self.widget_aff1_mode)
        self.rdb_aff1_remove.setObjectName(u"rdb_aff1_remove")

        self.verticalLayout_5.addWidget(self.rdb_aff1_remove)

        self.verticalSpacer = QSpacerItem(56, 22, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_5.addItem(self.verticalSpacer)


        self.horizontalLayout_2.addWidget(self.widget_aff1_mode)


        self.gridLayout.addLayout(self.horizontalLayout_2, 1, 1, 1, 1)

        self.lbl_affinity_2 = QLabel(Dialog)
        self.lbl_affinity_2.setObjectName(u"lbl_affinity_2")
        self.lbl_affinity_2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.lbl_affinity_2, 2, 0, 1, 1)

        self.lbl_affinity = QLabel(Dialog)
        self.lbl_affinity.setObjectName(u"lbl_affinity")
        self.lbl_affinity.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.lbl_affinity, 1, 0, 1, 1)

        self.lbl_desc = QLabel(Dialog)
        self.lbl_desc.setObjectName(u"lbl_desc")
        self.lbl_desc.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.lbl_desc, 0, 0, 1, 1)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.lst_affinities2 = QListWidget(Dialog)
        self.lst_affinities2.setObjectName(u"lst_affinities2")
        self.lst_affinities2.setSelectionMode(QAbstractItemView.ExtendedSelection)

        self.horizontalLayout_4.addWidget(self.lst_affinities2)

        self.widget_aff2_mode = QWidget(Dialog)
        self.widget_aff2_mode.setObjectName(u"widget_aff2_mode")
        self.verticalLayout_8 = QVBoxLayout(self.widget_aff2_mode)
        self.verticalLayout_8.setObjectName(u"verticalLayout_8")
        self.verticalLayout_8.setContentsMargins(0, 0, 0, 0)
        self.verticalSpacer_7 = QSpacerItem(56, 22, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_8.addItem(self.verticalSpacer_7)

        self.rdb_aff2_set = QRadioButton(self.widget_aff2_mode)
        self.rdb_aff2_set.setObjectName(u"rdb_aff2_set")
        self.rdb_aff2_set.setChecked(True)

        self.verticalLayout_8.addWidget(self.rdb_aff2_set)

        self.rdb_aff2_add = QRadioButton(self.widget_aff2_mode)
        self.rdb_aff2_add.setObjectName(u"rdb_aff2_add")

        self.verticalLayout_8.addWidget(self.rdb_aff2_add)

        self.rdb_aff2_remove = QRadioButton(self.widget_aff2_mode)
        self.rdb_aff2_remove.setObjectName(u"rdb_aff2_remove")

        self.verticalLayout_8.addWidget(self.rdb_aff2_remove)

        self.verticalSpacer_8 = QSpacerItem(56, 22, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_8.addItem(self.verticalSpacer_8)


        self.horizontalLayout_4.addWidget(self.widget_aff2_mode)


        self.gridLayout.addLayout(self.horizontalLayout_4, 2, 1, 1, 1)


        self.verticalLayout_2.addLayout(self.gridLayout)

        self.schedulewidget = ScheduleWidget(Dialog)
        self.schedulewidget.setObjectName(u"schedulewidget")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.schedulewidget.sizePolicy().hasHeightForWidth())
        self.schedulewidget.setSizePolicy(sizePolicy)
        self.schedulewidget.setMinimumSize(QSize(0, 160))

        self.verticalLayout_2.addWidget(self.schedulewidget)

        self.buttonBox = QDialogButtonBox(Dialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)

        self.verticalLayout_2.addWidget(self.buttonBox)

        QWidget.setTabOrder(self.edt_desc, self.buttonBox)

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Job Editor", None))
        self.rdb_aff1_set.setText(QCoreApplication.translate("Dialog", u"set", None))
        self.rdb_aff1_add.setText(QCoreApplication.translate("Dialog", u"add", None))
        self.rdb_aff1_remove.setText(QCoreApplication.translate("Dialog", u"remove", None))
        self.lbl_affinity_2.setText(QCoreApplication.translate("Dialog", u"2nd affinity", None))
        self.lbl_affinity.setText(QCoreApplication.translate("Dialog", u"1st affinity", None))
        self.lbl_desc.setText(QCoreApplication.translate("Dialog", u"description", None))
        self.rdb_aff2_set.setText(QCoreApplication.translate("Dialog", u"set", None))
        self.rdb_aff2_add.setText(QCoreApplication.translate("Dialog", u"add", None))
        self.rdb_aff2_remove.setText(QCoreApplication.translate("Dialog", u"remove", None))
    # retranslateUi

