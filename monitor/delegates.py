import time, datetime

from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtCore import Qt

from .constants import DATE_FORMAT


class AbstractRowDelegate(QtWidgets.QStyledItemDelegate):

    def paintBackground(self, painter, option, index):
        # paint border
        painter.fillRect(option.rect, option.widget.bordercolor)
        # shrink paint rect
        option.rect.adjust(0, 0, -1, 0) # leave a 1px column for grid
        # paint content
        selected = option.state & QtWidgets.QStyle.State_Selected > 0
        bgrole = QtGui.QPalette.Highlight if selected else QtGui.QPalette.AlternateBase if index.row() % 2 else QtGui.QPalette.Base
        painter.setPen(Qt.NoPen)
        painter.setBrush(QtGui.QBrush(option.palette.color(bgrole)))
        painter.drawRect(option.rect)


    def paintForeground(self, painter, option, index):
        # draw grid
        # painter.setPen(QtGui.QColor(229,229,229))
        # a = option.rect.topRight() + QtCore.QPoint(1,0)
        # b = option.rect.bottomRight() + QtCore.QPoint(1,0)
        # painter.drawLine(a, b)
        # draw overlay color
        copied = index.data(Qt.UserRole+1)
        if copied:
            # 50% white overlay
            painter.setPen(Qt.NoPen)
            painter.setBrush(QtGui.QColor(255,255,255,128))
            painter.drawRect(option.rect)


    def textColor(self, index, option):

        if option.state & QtWidgets.QStyle.State_Selected > 0:
            # highlighted
            return option.palette.color(QtGui.QPalette.HighlightedText)
        else:
            # default
            return index.data(Qt.ForegroundRole) or option.palette.color(QtGui.QPalette.Text)


    def getDate(self, value):
        if value > 0:
            return time.strftime(DATE_FORMAT, time.localtime(value))
        return '-'


    def getDuration(self, value):
        M, S = divmod(value, 60)
        H, M = divmod(M, 60)
        return '{}:{:02d}:{:02d}'.format(H, M, S)


class RowDelegate(AbstractRowDelegate):

    PADDING = 4

    def paint(self, painter, option, index):
        self.paintBackground(painter, option, index)

        painter.setPen(self.textColor(index, option))
        textrect = option.rect.adjusted( self.PADDING, 0, -self.PADDING, 0)
        alignment = index.data(Qt.TextAlignmentRole) or Qt.AlignCenter
        painter.drawText(textrect, alignment, str(index.data()))

        self.paintForeground(painter, option, index)


    def sizeHint(self, option, index):
        w = option.fontMetrics.width(str(index.data()))
        return QtCore.QSize(w + self.PADDING * 2, 0)


def mix(c1, c2, a):
    ''' return an interpolated color between `c1` and `c2` according to value `a` '''
    return QtGui.QColor(
        c1.red() * (1-a) + c2.red() * a,
        c1.green() * (1-a) + c2.green() * a,
        c1.blue() * (1-a) + c2.blue() * a,
        255
    )

def darken(c, a):
    _a = a * 255
    return QtGui.QColor(
        max(0, (c.red()-_a) / (1-a)),
        max(0, (c.green()-_a) / (1-a)),
        max(0, (c.blue()-_a) / (1-a)),
        255
    )


class TitleDelegate(RowDelegate):

    TITLE_COLORS = {
        0: QtGui.QColor(0, 255, 0),     # single job
        1: QtGui.QColor(0, 128, 255),   # subtask
        3: QtGui.QColor(255, 0, 0),     # invalid
    }

    def paint(self, painter, option, index):
        color_index = index.data(Qt.UserRole+2)
        newcolor = self.TITLE_COLORS.get(color_index)
        if newcolor:
            base = option.palette.color(QtGui.QPalette.Text)
            new = self.TITLE_COLORS.get(color_index)
            option.palette.setColor(QtGui.QPalette.Text, mix(base, new, .5))

        super(TitleDelegate, self).paint(painter, option, index)


class PriorityDelegate(AbstractRowDelegate):

    def paint(self, painter, option, index):
        self.paintBackground(painter, option, index)
        job = index.data(Qt.UserRole)
        painter.setBrush(option.widget.priorityColorAt(job["priority"]/100.))
        painter.drawRect(option.rect)
        painter.setPen(self.textColor(index, option))
        painter.drawText(option.rect, Qt.AlignCenter, str(index.data()))

        self.paintForeground(painter, option, index)


    def sizeHint(self, option, index):
        return QtCore.QSize(20, 0)


class StateDelegate(AbstractRowDelegate):

    def paint(self, painter, option, index):
        self.paintBackground(painter, option, index)
        state = index.data()
        color = option.widget.namedColor(state, None)
        if color:
            painter.setBrush(QtGui.QBrush(color))
            painter.drawRect(option.rect)
        painter.setPen(self.textColor(index, option))
        painter.drawText(option.rect, Qt.AlignCenter, state)

        self.paintForeground(painter, option, index)


    def sizeHint(self, option, index):
        return QtCore.QSize(20, 0)


class ProgressDelegate(AbstractRowDelegate):

    def paint(self, painter, option, index):
        self.paintBackground(painter, option, index)
        R = QtCore.QRectF(option.rect)
        width = R.width()
        job = index.data(Qt.UserRole)
        text = "n/a"
        # draw progress bar
        painter.setPen(Qt.NoPen)
        if job["command"]:
            # job is a command
            if job["progress"] is not None:
                # some progress has been "captured"
                painter.setBrush(option.widget.namedColor('FINISHED'))
                R.setWidth(width * job["progress"])
                painter.drawRect(R)
                text = "{0:.0f}%".format(job["progress"]*100)
            elif job["state"] == "FINISHED":
                painter.setBrush(option.widget.namedColor('FINISHED'))
                painter.drawRect(R)
                text = "100%"

        elif job["total"] > 0:
            # job is a group
            total = float(job["total"])
            # draw finished part
            finished = job["total_finished"] / total
            painter.setBrush(option.widget.namedColor('FINISHED'))
            R.setWidth(width * finished)
            painter.drawRect(R)
            # draw working part
            working = job["total_working"] / total
            R.setLeft(R.right())
            R.setWidth(width * working)
            painter.setBrush(option.widget.namedColor('WORKING'))
            painter.drawRect(R)
            # draw error part
            errors = job["total_errors"] / total
            R.setLeft(R.right())
            R.setWidth(width * errors)
            painter.setBrush(option.widget.namedColor('ERROR'))
            painter.drawRect(R)
            # text is % of finished jobs
            # text = "{0:.0f}%".format(finished*100)
            text = "{}/{}".format(job["total_finished"], job["total"])


        # 3. draw text
        painter.setPen(self.textColor(index, option))
        painter.drawText(option.rect, Qt.AlignCenter, text)

        self.paintForeground(painter, option, index)


class LoadDelegate(AbstractRowDelegate):

    def paintBars(self, painter, option, index, data):
        offline = index.data(Qt.UserRole)['state'] == 'OFFLINE'
        if len(data) > 0:
            width = height = 0
            rectf = QtCore.QRectF(option.rect)
            if len(data) <= 2:
                # horizontal bars
                if len(data) == 1:
                    height = rectf.height() / len(data)                 # for single horizontal bar, fill the whole height
                else:
                    height = (rectf.height()-len(data)) / len(data)     # leave 1px spaces for horizontal separating lines
                rectf.setHeight(height)
            else:
                # vertical bars
                width = (rectf.width()-len(data)+1) / len(data)         # same here, but vertical separating lines (exclude last line)
                rectf.setWidth(width)
            for i, load in enumerate(data):
                # clamp between 0 and 1 in case of erroneous values sent from workers
                load = min(max(load, 0), 1)
                sep = 1 if i > 0 else 0 # 1px offset to separate each bars (except 1st one)
                # position bar
                if len(data) <= 2:
                    # horizontal bars
                    rectf.moveTop(option.rect.top() + height*i + sep*i)
                    width = option.rect.width() * load
                    rectf.setRight(option.rect.left() + width)
                else:
                    # vertical bars

                    rectf.moveLeft(option.rect.left() + width*i + sep*i)
                    height = option.rect.height() * load
                    rectf.setTop(option.rect.bottom() - height)
                # draw bar
                painter.setPen(Qt.NoPen)
                painter.setBrush(Qt.lightGray if offline else option.widget.heatColorAt(load))
                painter.drawRect(rectf)


    def sizeHint(self, option, index):
        return QtCore.QSize(60, 0)


class MemoryDelegate(LoadDelegate):

    def paint(self, painter, option, index):
        self.paintBackground(painter, option, index)
        rectf = QtCore.QRectF(option.rect)
        worker = index.data(Qt.UserRole)
        if worker["total_memory"] > 0:
            total_mem = float(worker["total_memory"])
            free_mem = float(worker["free_memory"])
            used_mem = total_mem - free_mem
            used_mem_ratio = used_mem / total_mem
            # # draw bar
            self.paintBars(painter, option, index, [used_mem_ratio])
            # draw text
            text = "{:.1f}GB / {:.1f}GB".format(used_mem/1024, total_mem/1024)
            painter.setPen(self.textColor(index, option))
            painter.drawText(option.rect, Qt.AlignCenter, text)

        self.paintForeground(painter, option, index)


    def sizeHint(self, option, index):
        return QtCore.QSize(60, 0)


class CpuLoadDelegate(LoadDelegate):

    def paint(self, painter, option, index):
        self.paintBackground(painter, option, index)
        cpus = [ load/100. for load in index.data(Qt.DisplayRole) ]
        self.paintBars(painter, option, index, cpus)
        self.paintForeground(painter, option, index)


class GpuMemoryDelegate(LoadDelegate):

    def paint(self, painter, option, index):
        self.paintBackground(painter, option, index)
        worker = index.data(Qt.UserRole)
        gpus = []
        for free_mem, total_mem in zip(worker["gpu_free_mem"], worker["gpu_total_mem"]):
            if total_mem > 0:
                gpus.append((total_mem - free_mem) / float(total_mem))
        self.paintBars(painter, option, index, gpus)
        self.paintForeground(painter, option, index)


class PingDelegate(LoadDelegate):

    def __init__(self, parent=None):
        super(PingDelegate, self).__init__(parent)
        self.workers_timeout = 0


    def setWorkersTimeout(self, value):
        self.workers_timeout = value


    def paint(self, painter, option, index):
        self.paintBackground(painter, option, index)
        worker = index.data(Qt.UserRole)
        timeout = index.data(Qt.UserRole+3)
        if timeout > 0:
            # draw background color from green to red according to ping / timeout defined on server
            load = min(max(worker["ping"]/timeout, 0), 1)
            painter.setPen(Qt.NoPen)
            painter.setBrush(option.widget.heatColorAt(load))
            painter.drawRect(option.rect)
        # draw text
        painter.setPen(self.textColor(index, option))
        painter.drawText(option.rect, Qt.AlignCenter, self.getDate(index.data()))

        self.paintForeground(painter, option, index)


class DateDelegate(AbstractRowDelegate):

    def paint(self, painter, option, index):
        self.paintBackground(painter, option, index)

        painter.setPen(self.textColor(index, option))
        painter.drawText(option.rect, Qt.AlignCenter, self.getDate(index.data()))

        self.paintForeground(painter, option, index)


class DurationDelegate(AbstractRowDelegate):

    def paint(self, painter, option, index):
        self.paintBackground(painter, option, index)

        painter.setPen(self.textColor(index, option))
        painter.drawText(option.rect, Qt.AlignCenter, self.getDuration(index.data()))

        self.paintForeground(painter, option, index)



class ScheduleDelegate(AbstractRowDelegate):

    def paint(self, painter, option, index):
        self.paintBackground(painter, option, index)

        painter.setPen(self.textColor(index, option))
        schedule = index.data()
        if len(schedule) == 42:
            graph = self.createGraph(schedule, option)
            painter.drawPixmap(option.rect, graph, graph.rect())

        self.paintForeground(painter, option, index)


    def sizeHint(self, option, index):
        return QtCore.QSize(20, 0)


    def createGraph(self, schedule, option):
        canvas = QtGui.QPixmap(24, 7)
        canvas.fill(option.widget.namedColor('ERROR'))
        p = QtGui.QPainter(canvas)

        # draw graph (green)
        p.setPen(option.widget.namedColor('FINISHED'))
        week_bits = int(schedule, 16)
        for day in range(7):
            for hour in range(24):
                hour_bit = 2**(24*day + hour)
                if hour_bit & week_bits > 0:
                    p.drawPoint(23-hour, 6-day)

        # draw current time (yellow)
        now = datetime.datetime.now()
        x, y = now.hour, now.weekday()
        p.setPen(option.widget.namedColor('WORKING'))
        p.drawPoint(x, y)
        # p.setPen(Qt.white)
        # p.setOpacity(0.25)
        # p.drawLine(0, y, 23, y)
        # p.drawLine(x, 0, x, 6)
        # p.drawPoint(x, y)

        return canvas


class AffinityEditor(QtWidgets.QLineEdit):

    def __init__(self, parent=None):
        super(AffinityEditor, self).__init__(parent)
        self.setStyleSheet("border: none")


    def paintEvent(self, event):
        # paint a "base color" background behind the lineedit
        p = QtGui.QPainter(self)
        p.setPen(Qt.NoPen)
        p.setBrush(self.palette().base())
        p.drawRect(event.rect())
        super(AffinityEditor, self).paintEvent(event)


class AffinityDelegate(RowDelegate):

    def createEditor(self, parent, option, index):
        editor = AffinityEditor(parent)
        editor.setText(index.data())
        editor.editingFinished.connect(self.commitAndCloseEditor)
        return editor


    def setEditorData(self, editor, index):
        # override `setEditorData` to prevent editor data from being changed
        return


    def commitAndCloseEditor(self):
        editor = self.sender()
        self.commitData[QtWidgets.QWidget].emit(editor)
        self.closeEditor[QtWidgets.QWidget].emit(editor)


    def setModelData(self, editor, model, index):
        if index.data() != editor.text():
            model.setData(index, editor.text())
