import os, sys
import argparse
import pathlib
from PySide2.QtWidgets import QApplication

from .. import utils, logger
from .mainwindow import MainWindow, ColorTest
from ..api.qtapi import CoaliteAPI
from . import monitor_rcc


# print resources
# from PySide2.QtCore import QDirIterator
# it = QDirIterator(":", QDirIterator.Subdirectories)
# while it.hasNext():
#     print(it.next())

if __name__ == '__main__':

    host = os.getenv('COALITE_HOST')
    port = os.getenv('COALITE_PORT', 19215)

    # parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', metavar='LEVEL', type=int, default=0, help='Increase verbosity (default: 0)')
    server_name_help = f'(default:{host})' if host else '(broadcast if not set)'
    parser.add_argument('-s', '--server', metavar='NAME/IP', type=str, default=host, help='Server name ' + server_name_help)
    parser.add_argument('-p', '--port', metavar='NUMBER', type=int, default=port, help=f'Server port (default: {port})')

    parser.add_argument('--theme', default='light', help=f'Color theme')
    parser.add_argument('--colortest', action='store_true', help=f'Color test')
    args = parser.parse_args()

    path = pathlib.Path(__file__)

    # read CSS
    css = ''
    base_css_path = path.parent / 'coalite.css'
    with open(base_css_path) as fh:
        css += fh.read()

    theme_css_path = path.parent / 'coalite-{}.css'.format(args.theme)
    with open(theme_css_path) as fh:
        css += fh.read()

    if args.colortest:
        app = QApplication([])
        w = ColorTest()
        w.setStyleSheet(css)
        w.show()
        sys.exit(app.exec_())

    if args.server:
        logger.info(f'Server set to {args.server} (port:{args.port})')
    else:
        args.server = utils.broadcast(args.port, '255.255.255.255')
        logger.info(f'Server found at {args.server} (port:{args.port})')

    # setup Qt app
    app = QApplication([])
    api = CoaliteAPI(args.server, args.port)
    w = MainWindow(api)
    w.setStyleSheet(css)
    w.show()
    app.exec_()
    w.shutdown()
