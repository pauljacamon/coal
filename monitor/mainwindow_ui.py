# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainwindow.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from .models_views import WorkerView
from .models_views import AffinityView
from .models_views import TabBar
from .models_views import JobView


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1026, 750)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 20, 0, 0)
        self.tabWidget_main = QTabWidget(self.centralwidget)
        self.tabWidget_main.setObjectName(u"tabWidget_main")
        self.tab_jobs = QWidget()
        self.tab_jobs.setObjectName(u"tab_jobs")
        self.verticalLayout_2 = QVBoxLayout(self.tab_jobs)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 6, 0, 0)
        self.layout_actions = QHBoxLayout()
        self.layout_actions.setSpacing(0)
        self.layout_actions.setObjectName(u"layout_actions")
        self.layout_actions.setContentsMargins(-1, 0, -1, -1)
        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.layout_actions.addItem(self.horizontalSpacer_2)

        self.btn_filterJobs = QPushButton(self.tab_jobs)
        self.btn_filterJobs.setObjectName(u"btn_filterJobs")

        self.layout_actions.addWidget(self.btn_filterJobs)

        self.horizontalSpacer_16 = QSpacerItem(6, 20, QSizePolicy.Fixed, QSizePolicy.Minimum)

        self.layout_actions.addItem(self.horizontalSpacer_16)

        self.btn_resetJobs = QPushButton(self.tab_jobs)
        self.btn_resetJobs.setObjectName(u"btn_resetJobs")

        self.layout_actions.addWidget(self.btn_resetJobs)

        self.btn_resetJobErrors = QPushButton(self.tab_jobs)
        self.btn_resetJobErrors.setObjectName(u"btn_resetJobErrors")

        self.layout_actions.addWidget(self.btn_resetJobErrors)

        self.horizontalSpacer_7 = QSpacerItem(6, 20, QSizePolicy.Fixed, QSizePolicy.Minimum)

        self.layout_actions.addItem(self.horizontalSpacer_7)

        self.btn_startJobs = QPushButton(self.tab_jobs)
        self.btn_startJobs.setObjectName(u"btn_startJobs")

        self.layout_actions.addWidget(self.btn_startJobs)

        self.btn_pauseJobs = QPushButton(self.tab_jobs)
        self.btn_pauseJobs.setObjectName(u"btn_pauseJobs")

        self.layout_actions.addWidget(self.btn_pauseJobs)

        self.horizontalSpacer_6 = QSpacerItem(6, 20, QSizePolicy.Fixed, QSizePolicy.Minimum)

        self.layout_actions.addItem(self.horizontalSpacer_6)

        self.btn_newJob = QPushButton(self.tab_jobs)
        self.btn_newJob.setObjectName(u"btn_newJob")

        self.layout_actions.addWidget(self.btn_newJob)

        self.horizontalSpacer_10 = QSpacerItem(6, 20, QSizePolicy.Fixed, QSizePolicy.Minimum)

        self.layout_actions.addItem(self.horizontalSpacer_10)

        self.btn_editJobs = QPushButton(self.tab_jobs)
        self.btn_editJobs.setObjectName(u"btn_editJobs")

        self.layout_actions.addWidget(self.btn_editJobs)

        self.horizontalSpacer_11 = QSpacerItem(6, 20, QSizePolicy.Fixed, QSizePolicy.Minimum)

        self.layout_actions.addItem(self.horizontalSpacer_11)

        self.btn_cutJobs = QPushButton(self.tab_jobs)
        self.btn_cutJobs.setObjectName(u"btn_cutJobs")

        self.layout_actions.addWidget(self.btn_cutJobs)

        self.btn_pasteJobs = QPushButton(self.tab_jobs)
        self.btn_pasteJobs.setObjectName(u"btn_pasteJobs")

        self.layout_actions.addWidget(self.btn_pasteJobs)

        self.horizontalSpacer_12 = QSpacerItem(6, 20, QSizePolicy.Fixed, QSizePolicy.Minimum)

        self.layout_actions.addItem(self.horizontalSpacer_12)

        self.btn_deleteJobs = QPushButton(self.tab_jobs)
        self.btn_deleteJobs.setObjectName(u"btn_deleteJobs")

        self.layout_actions.addWidget(self.btn_deleteJobs)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.layout_actions.addItem(self.horizontalSpacer)


        self.verticalLayout_2.addLayout(self.layout_actions)

        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.horizontalLayout_6.setContentsMargins(10, -1, -1, -1)
        self.tabBar_jobHierarchy = TabBar(self.tab_jobs)
        self.tabBar_jobHierarchy.setObjectName(u"tabBar_jobHierarchy")
        self.tabBar_jobHierarchy.setMinimumSize(QSize(30, 0))

        self.horizontalLayout_6.addWidget(self.tabBar_jobHierarchy)

        self.horizontalSpacer_14 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_6.addItem(self.horizontalSpacer_14)


        self.verticalLayout_2.addLayout(self.horizontalLayout_6)

        self.tableView_jobs = JobView(self.tab_jobs)
        self.tableView_jobs.setObjectName(u"tableView_jobs")
        self.tableView_jobs.setFrameShape(QFrame.NoFrame)
        self.tableView_jobs.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tableView_jobs.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.tableView_jobs.setShowGrid(False)
        self.tableView_jobs.setSortingEnabled(True)
        self.tableView_jobs.horizontalHeader().setHighlightSections(False)
        self.tableView_jobs.horizontalHeader().setStretchLastSection(True)
        self.tableView_jobs.verticalHeader().setVisible(False)
        self.tableView_jobs.verticalHeader().setDefaultSectionSize(21)

        self.verticalLayout_2.addWidget(self.tableView_jobs)

        self.tabWidget_main.addTab(self.tab_jobs, "")
        self.tab_workers = QWidget()
        self.tab_workers.setObjectName(u"tab_workers")
        self.verticalLayout_3 = QVBoxLayout(self.tab_workers)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_3.setContentsMargins(0, 6, 0, 0)
        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setSpacing(0)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.horizontalLayout_4.setContentsMargins(-1, 0, -1, -1)
        self.horizontalSpacer_4 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_4.addItem(self.horizontalSpacer_4)

        self.btn_startWorkers = QPushButton(self.tab_workers)
        self.btn_startWorkers.setObjectName(u"btn_startWorkers")

        self.horizontalLayout_4.addWidget(self.btn_startWorkers)

        self.btn_stopWorkers = QPushButton(self.tab_workers)
        self.btn_stopWorkers.setObjectName(u"btn_stopWorkers")

        self.horizontalLayout_4.addWidget(self.btn_stopWorkers)

        self.horizontalSpacer_13 = QSpacerItem(6, 20, QSizePolicy.Fixed, QSizePolicy.Minimum)

        self.horizontalLayout_4.addItem(self.horizontalSpacer_13)

        self.btn_editWorkers = QPushButton(self.tab_workers)
        self.btn_editWorkers.setObjectName(u"btn_editWorkers")

        self.horizontalLayout_4.addWidget(self.btn_editWorkers)

        self.horizontalSpacer_15 = QSpacerItem(6, 20, QSizePolicy.Fixed, QSizePolicy.Minimum)

        self.horizontalLayout_4.addItem(self.horizontalSpacer_15)

        self.btn_forgetWorkers = QPushButton(self.tab_workers)
        self.btn_forgetWorkers.setObjectName(u"btn_forgetWorkers")

        self.horizontalLayout_4.addWidget(self.btn_forgetWorkers)

        self.horizontalSpacer_5 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_4.addItem(self.horizontalSpacer_5)


        self.verticalLayout_3.addLayout(self.horizontalLayout_4)

        self.tableView_workers = WorkerView(self.tab_workers)
        self.tableView_workers.setObjectName(u"tableView_workers")
        self.tableView_workers.setFrameShape(QFrame.NoFrame)
        self.tableView_workers.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tableView_workers.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.tableView_workers.setShowGrid(False)
        self.tableView_workers.setSortingEnabled(True)
        self.tableView_workers.horizontalHeader().setHighlightSections(False)
        self.tableView_workers.horizontalHeader().setStretchLastSection(True)
        self.tableView_workers.verticalHeader().setVisible(False)
        self.tableView_workers.verticalHeader().setDefaultSectionSize(21)

        self.verticalLayout_3.addWidget(self.tableView_workers)

        self.tabWidget_main.addTab(self.tab_workers, "")
        self.tab_log = QWidget()
        self.tab_log.setObjectName(u"tab_log")
        self.verticalLayout_4 = QVBoxLayout(self.tab_log)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalLayout_4.setContentsMargins(0, 6, 0, 0)
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer_8 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_8)

        self.lbl_log = QLabel(self.tab_log)
        self.lbl_log.setObjectName(u"lbl_log")
        self.lbl_log.setAlignment(Qt.AlignCenter)

        self.horizontalLayout.addWidget(self.lbl_log)

        self.cbb_attempts = QComboBox(self.tab_log)
        self.cbb_attempts.setObjectName(u"cbb_attempts")
        self.cbb_attempts.setSizeAdjustPolicy(QComboBox.AdjustToContents)

        self.horizontalLayout.addWidget(self.cbb_attempts)

        self.horizontalSpacer_9 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_9)


        self.verticalLayout_4.addLayout(self.horizontalLayout)

        self.textEdit_log = QPlainTextEdit(self.tab_log)
        self.textEdit_log.setObjectName(u"textEdit_log")
        self.textEdit_log.setFrameShape(QFrame.NoFrame)

        self.verticalLayout_4.addWidget(self.textEdit_log)

        self.tabWidget_main.addTab(self.tab_log, "")
        self.tab_affinities = QWidget()
        self.tab_affinities.setObjectName(u"tab_affinities")
        self.horizontalLayout_5 = QHBoxLayout(self.tab_affinities)
        self.horizontalLayout_5.setSpacing(0)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.horizontalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.tableView_affinities_A = AffinityView(self.tab_affinities)
        self.tableView_affinities_A.setObjectName(u"tableView_affinities_A")
        self.tableView_affinities_A.setFrameShape(QFrame.NoFrame)
        self.tableView_affinities_A.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tableView_affinities_A.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.tableView_affinities_A.setShowGrid(False)
        self.tableView_affinities_A.horizontalHeader().setHighlightSections(False)
        self.tableView_affinities_A.horizontalHeader().setStretchLastSection(True)
        self.tableView_affinities_A.verticalHeader().setVisible(False)
        self.tableView_affinities_A.verticalHeader().setDefaultSectionSize(21)

        self.horizontalLayout_5.addWidget(self.tableView_affinities_A)

        self.tableView_affinities_B = AffinityView(self.tab_affinities)
        self.tableView_affinities_B.setObjectName(u"tableView_affinities_B")
        self.tableView_affinities_B.setFrameShape(QFrame.NoFrame)
        self.tableView_affinities_B.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tableView_affinities_B.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.tableView_affinities_B.setShowGrid(False)
        self.tableView_affinities_B.horizontalHeader().setHighlightSections(False)
        self.tableView_affinities_B.horizontalHeader().setStretchLastSection(True)
        self.tableView_affinities_B.verticalHeader().setVisible(False)
        self.tableView_affinities_B.verticalHeader().setDefaultSectionSize(21)

        self.horizontalLayout_5.addWidget(self.tableView_affinities_B)

        self.tabWidget_main.addTab(self.tab_affinities, "")

        self.verticalLayout.addWidget(self.tabWidget_main)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        self.tabWidget_main.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        self.btn_filterJobs.setText(QCoreApplication.translate("MainWindow", u"filter", None))
        self.btn_resetJobs.setText(QCoreApplication.translate("MainWindow", u"reset", None))
        self.btn_resetJobs.setProperty("barpos", QCoreApplication.translate("MainWindow", u"left", None))
        self.btn_resetJobErrors.setText(QCoreApplication.translate("MainWindow", u"reset errors", None))
        self.btn_resetJobErrors.setProperty("barpos", QCoreApplication.translate("MainWindow", u"right", None))
        self.btn_startJobs.setText(QCoreApplication.translate("MainWindow", u"resume", None))
        self.btn_startJobs.setProperty("barpos", QCoreApplication.translate("MainWindow", u"left", None))
        self.btn_pauseJobs.setText(QCoreApplication.translate("MainWindow", u"pause", None))
        self.btn_pauseJobs.setProperty("barpos", QCoreApplication.translate("MainWindow", u"right", None))
        self.btn_newJob.setText(QCoreApplication.translate("MainWindow", u"new", None))
        self.btn_editJobs.setText(QCoreApplication.translate("MainWindow", u"edit", None))
        self.btn_cutJobs.setText(QCoreApplication.translate("MainWindow", u"cut", None))
        self.btn_cutJobs.setProperty("barpos", QCoreApplication.translate("MainWindow", u"left", None))
        self.btn_pasteJobs.setText(QCoreApplication.translate("MainWindow", u"paste", None))
        self.btn_pasteJobs.setProperty("barpos", QCoreApplication.translate("MainWindow", u"right", None))
        self.btn_deleteJobs.setText(QCoreApplication.translate("MainWindow", u"delete", None))
        self.tabWidget_main.setTabText(self.tabWidget_main.indexOf(self.tab_jobs), QCoreApplication.translate("MainWindow", u"Jobs", None))
        self.btn_startWorkers.setText(QCoreApplication.translate("MainWindow", u"start", None))
        self.btn_startWorkers.setProperty("barpos", QCoreApplication.translate("MainWindow", u"left", None))
        self.btn_stopWorkers.setText(QCoreApplication.translate("MainWindow", u"stop", None))
        self.btn_stopWorkers.setProperty("barpos", QCoreApplication.translate("MainWindow", u"right", None))
        self.btn_editWorkers.setText(QCoreApplication.translate("MainWindow", u"edit", None))
        self.btn_forgetWorkers.setText(QCoreApplication.translate("MainWindow", u"forget", None))
        self.tabWidget_main.setTabText(self.tabWidget_main.indexOf(self.tab_workers), QCoreApplication.translate("MainWindow", u"Workers", None))
        self.lbl_log.setText(QCoreApplication.translate("MainWindow", u"no log", None))
        self.tabWidget_main.setTabText(self.tabWidget_main.indexOf(self.tab_log), QCoreApplication.translate("MainWindow", u"Logs", None))
        self.tabWidget_main.setTabText(self.tabWidget_main.indexOf(self.tab_affinities), QCoreApplication.translate("MainWindow", u"Affinities", None))
        pass
    # retranslateUi

