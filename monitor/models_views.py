import sys, time

from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtCore import Qt

from . import delegates
from .csspropertiesmixin import CSSPropertiesMixin
from .constants import JOB_COLUMNS, JOB_COLUMNS_DATA, WORKER_COLUMNS, WORKER_COLUMNS_DATA, JOB_TYPE_NAMES

## MODELS ######################################################################


class AbstractModel(QtCore.QAbstractTableModel):

    def __init__(self, parent=None):
        super(AbstractModel, self).__init__(parent)
        self.items = []


    def load(self, items):
        # sort incoming data
        previous_ids = [ item['id'] for item in self.items ]
        new_ids = [ item['id'] for item in items ]
        to_remove = []

        # sort incoming data
        for row, id in enumerate(previous_ids):
            try:
                index = new_ids.index(id)
            except ValueError:
                to_remove.append(row)
            else:
                # update existing item data
                self.items[row].update(items[index])

        # remove missing items
        for row in reversed(to_remove):
            self.removeItem(row)

        # update existing items
        self.dataChanged.emit(self.index(0, 0), self.index(self.rowCount()-1, self.columnCount()-1))

        # append remaining items
        new_items = [ items[i] for i, id in enumerate(new_ids) if id not in previous_ids ]
        if new_items:
            self.appendItems(new_items)


    def removeItem(self, i, parent=QtCore.QModelIndex()):
        self.beginRemoveRows(parent, i, i)
        del(self.items[i])
        self.endRemoveRows()


    def appendItems(self, items, parent=QtCore.QModelIndex()):
        first = len(self.items)
        last = first + len(items)-1
        self.beginInsertRows(parent, first, last)
        self.items.extend(items)
        self.endInsertRows()


    def clear(self):
        self.beginResetModel()
        del self.items[:]
        self.endResetModel()


    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self.items)


    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.columns)

    @property
    def columns(self):
        raise NotImplementedError


    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if orientation == Qt.Horizontal:
            key = self.columns[section]
            data = self.columns_data.get(key)
            if role == Qt.DisplayRole:
                try:
                    return data['name']
                except:
                    return key
            elif role == Qt.ToolTipRole:
                return key
            elif role == Qt.UserRole:
                return data


    def indexesFromIds(self, ids):
        return [ self.index(i, 0) for i, item in enumerate(self.items) if item['id'] in ids ]


class JobModel(AbstractModel):

    def __init__(self, copy_buffer, parent=None):
        self.copy_buffer = copy_buffer
        self.columns_data = JOB_COLUMNS_DATA
        self._filters = {}
        self.attempts_limit = 0
        self.parent_id = 0
        super(JobModel, self).__init__(parent)


    def load(self, parent_id, items):
        self.parent_id = parent_id
        super(JobModel, self).load(items)


    def setAttemptsLimit(self, value):
        self.attempts_limit = value


    @property
    def columns(self):
        return JOB_COLUMNS


    def data(self, index, role=Qt.DisplayRole):
        if role == Qt.DisplayRole:
            job = self.items[index.row()]
            key = self.columns[index.column()]
            if key == 'title':
                if job['is_subtask']:
                    return '{}-{}'.format(job['subtask_start'], job['subtask_end'])
            elif key.startswith("total"):
                job = self.items[index.row()]
                if job["command"] != "":
                    return "-"
            elif key == 'run_done' and self.attempts_limit > 0:
                    return '{}/{}'.format(job[key], self.attempts_limit)
            elif key == 'type':
                type_index = self.getJobTypeIndex(job)
                return JOB_TYPE_NAMES.get(type_index)
            elif key == 'worker_name':
                return job['worker_name'] or '-'

            return job[key]
        elif role == Qt.TextAlignmentRole:
            key = self.columns[index.column()]
            try:
                return self.columns_data[key]["align"]
            except KeyError:
                return Qt.AlignCenter
        # return job item
        elif role == Qt.UserRole:
            return self.items[index.row()]
        # return copy state
        elif role == Qt.UserRole+1:
            return self.items[index.row()]["id"] in self.copy_buffer
        # job title color index
        elif role == Qt.UserRole+2:
            job = self.items[index.row()]
            return self.getJobTypeIndex(job)


    def getJobTypeIndex(self, job):
        is_group, is_subtask = job['is_group'], job['is_subtask']
        if is_subtask:
            if is_group:
                return 3 # invalid, can't be both
            return 1 # subtask
        if is_group:
            if is_subtask:
                return 3 # invalid, can't be both
            return 2 # group
        return 0 # single job


    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if orientation == Qt.Horizontal:
            # Qt.UserRole+1: return filter associated with column, if any
            if role == Qt.UserRole+1:
                key = self.columns[section]
                return self._filters.get(self.parent_id, {}).get(key)
            # Qt.UserRole+1: return True if any filter is enabled in any field
            elif role == Qt.UserRole+2:
                return bool(self._filters.get(self.parent_id, {}))
        return super(JobModel, self).headerData(section, orientation, role)


    def users(self):
        return list(set(item["user"] for item in self.items))


    def filters(self, parent_id):
        return self._filters.get(parent_id, {})


    def setFilters(self, filters):
        self._filters[self.parent_id] = filters
        self.headerDataChanged.emit(Qt.Horizontal, 0, len(self.columns)-1)


    def isFiltered(self):
        return bool(self._filters)


class WorkerModel(AbstractModel):

    def __init__(self, parent=None):
        self.columns_data = WORKER_COLUMNS_DATA
        self._workers_timeout = 0
        super(WorkerModel, self).__init__(parent)


    def workersTimeout(self):
        return self._workers_timeout


    def setWorkersTimeout(self, value):
        self._workers_timeout = value


    @property
    def columns(self):
        return WORKER_COLUMNS


    def data(self, index, role=Qt.DisplayRole):
        if role == Qt.DisplayRole:
            worker = self.items[index.row()]
            key = self.columns[index.column()]
            return worker.get(key)
        elif role == Qt.UserRole:
            return self.items[index.row()]
        elif role == Qt.UserRole+3:
            return self._workers_timeout


class AffinityModel(AbstractModel):

    affinityUpdateRequested = QtCore.Signal(int, str)

    @property
    def columns(self):
        return ['id', 'name']


    def data(self, index, role=Qt.DisplayRole):
        if role == Qt.DisplayRole:
            aff = self.items[index.row()]
            key = self.columns[index.column()]
            return aff[key]
        elif role == Qt.TextAlignmentRole:
            if index.column() == 1:
                return Qt.AlignLeft|Qt.AlignVCenter


    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if orientation == Qt.Horizontal:
            if role == Qt.DisplayRole:
                return self.columns[section]


    def flags(self, index):
        flags = super(AffinityModel, self).flags(index)
        if index.column() == 1:
            return flags|Qt.ItemIsEditable
        else:
            return flags


    def setData(self, index, value, role=Qt.EditRole):
        if role == Qt.EditRole:
            item = self.items[index.row()]
            self.affinityUpdateRequested.emit(item['id'], value)
            # r = self.client.setAffinity(aff["id"], value)
            # if r == 1:
            #     aff["name"] = value
            #     self.dataChanged.emit(index, index)
            return True


class AffinityProxyModel(QtCore.QSortFilterProxyModel):

    def __init__(self, parent=None):
        self.start = 1
        self.end = 64
        super(AffinityProxyModel, self).__init__(parent)


    def setRange(self, start, end):
        self.start = start
        self.end = end
        self.invalidateFilter()


    def filterAcceptsRow(self, source_row, source_parent):
        aff_id = self.sourceModel().index(source_row, 0, source_parent).data()
        return aff_id >= self.start and aff_id <= self.end


## VIEWS #######################################################################


class CoalHeaderView(QtWidgets.QHeaderView, CSSPropertiesMixin):

    def __init__(self, orientation, parent=None):
        super(CoalHeaderView, self).__init__(orientation, parent)
        self.setStretchLastSection(True)
        self.setSectionsClickable(True)
        self._down_arrow_pixmap = QtGui.QPixmap(':/arrow-down-dark.png')
        self._up_arrow_pixmap = QtGui.QPixmap(':/arrow-up-dark.png')
        # self._up_arrow_pixmap = self._down_arrow_pixmap.transformed(QtGui.QTransform(0, 0, 0,-1, 0, 0))


    def paintFilter(self, painter, rect, data):
        painter.save()
        # draw filter background
        painter.setPen(Qt.NoPen)
        painter.setBrush(Qt.gray)
        text = str(data)
        text_rect = self.fontMetrics().boundingRect(rect, Qt.AlignCenter, text)
        caps_rect = text_rect.adjusted(-4, 0, 4, 0)
        painter.setRenderHint(painter.Antialiasing)
        painter.drawRoundedRect(caps_rect, 4, 4)
        # draw filter text
        painter.setPen(Qt.white)
        painter.drawText(text_rect, 0, text)
        painter.restore()


    def paintTitle(self, painter, rect, section, text):
        painter.save()
        # self.setFont(QtGui.QFont('Cascadia Mono', 10))
        # get text rectangle
        text_rect = self.fontMetrics().boundingRect(rect, Qt.AlignCenter, text)
        if self.sortIndicatorSection() == section:
            space = 4 # space between text and arrow
            # get the correct arrow
            arrow_pixmap = self._down_arrow_pixmap if self.sortIndicatorOrder() == Qt.AscendingOrder else self._up_arrow_pixmap
            # get arrow rectangle
            arrow_rect = arrow_pixmap.rect()
            # translate text to the left to leave space for the arrow
            offset = (arrow_rect.width() + space) / 2
            text_rect.translate(-offset, 0)
            # draw text
            painter.drawText(text_rect, 0, text)
            # offset arrow destination
            arrow_rect.moveCenter(text_rect.center())
            arrow_rect.moveLeft(text_rect.right() + space)
            # draw arrow
            painter.drawPixmap(arrow_rect, arrow_pixmap)
        else:
            painter.drawText(text_rect, 0, text)
        painter.restore()


    def paintSection(self, painter, rect, section):
        # leave some space for the right separator line
        rect.adjust(0, 0,-1, 0)
        filterdata = self.model().headerData(section, Qt.Horizontal, Qt.UserRole+1)
        text = self.model().headerData(section, Qt.Horizontal, Qt.DisplayRole)
        if filterdata:
            self.paintTitle(painter, rect.adjusted(0,0,0,-rect.height()/2), section, text)
            self.paintFilter(painter, rect.adjusted(0,rect.height()/2,0,0), filterdata)
        else:
            self.paintTitle(painter, rect, section, text)
        # draw separator
        painter.setPen(self.bordercolor)
        painter.drawLine(rect.right()+1, rect.top(), rect.right()+1, rect.bottom())


    def sizeHint(self):
        size = super(CoalHeaderView, self).sizeHint()
        if self.model():
            is_filtered = self.model().headerData(0, Qt.Horizontal, Qt.UserRole+2)
            if is_filtered:
                return size + QtCore.QSize(0, self.fontMetrics().lineSpacing() + 2)
        return size


class JobHeaderView(CoalHeaderView): pass


class WorkerHeaderView(CoalHeaderView): pass


class CoalProxyTableView(QtWidgets.QTableView, CSSPropertiesMixin):

    menuRequested = QtCore.Signal(QtCore.QPoint)

    def __init__(self, parent=None):
        super(CoalProxyTableView, self).__init__(parent)

        self.initHeader()

        self.proxyModel = QtCore.QSortFilterProxyModel(self)
        self.proxyModel.setDynamicSortFilter(True)
        self.proxyModel.setSortCaseSensitivity(Qt.CaseInsensitive)
        super(CoalProxyTableView, self).setModel(self.proxyModel)

        delegate = delegates.RowDelegate(self)
        self.setItemDelegate(delegate)

        self.horizontalHeader().setSectionsMovable(True)

        self.columnsMenu = QtWidgets.QMenu(self)

        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested[QtCore.QPoint].connect(self.requestMenu)

        self.horizontalHeader().setContextMenuPolicy(Qt.CustomContextMenu)
        self.horizontalHeader().customContextMenuRequested[QtCore.QPoint].connect(self.showHeaderMenu)


    def initHeader(self):
        pass


    def setModel(self, model):
        # set source model
        self.proxyModel.setSourceModel(model)

        # reset context menu
        self.columnsMenu.clear()

        # setup columns
        for i, name in enumerate(model.columns):
            data = model.headerData(i, Qt.Horizontal, Qt.UserRole)
            hidden = data and data.get("hidden")
            if hidden:
                self.horizontalHeader().setSectionHidden(i, True)
            # setup column delegate
            delegateName = data and data.get("delegate")
            if delegateName:
                delegate = getattr(delegates, delegateName)
                self.setItemDelegateForColumn(i, delegate(self))

            # setup column show/hide action
            nicename = data and data.get("name")
            if nicename:
                name = "{} ({})".format(nicename, name)
            action = self.columnsMenu.addAction(name)
            action.setCheckable(True)
            action.setData(i) # attach column index to action
            action.triggered[bool].connect(self.sectionVisibilityChanged)

        model.modelAboutToBeReset.connect(self.holdSelection)
        model.modelReset.connect(self.restoreSelection)


    def selectedRows(self):
        return [self.model().mapToSource(proxy) for proxy in self.selectionModel().selectedRows()]


    def selectedItems(self):
        return [index.data(Qt.UserRole) for index in self.selectedRows()]


    def hasSelection(self):
        return self.selectionModel().hasSelection()


    def multiSelected(self):
        return len(self.selectionModel().selectedRows()) > 1


    def holdSelection(self):
        self.selected_ids = [item['id'] for item in self.selectedItems()]


    def restoreSelection(self):
        indexes = self.proxyModel.sourceModel().indexesFromIds(self.selected_ids)
        for index in indexes:
            proxy = self.proxyModel.mapFromSource(index)
            self.selectionModel().select(proxy, QtCore.QItemSelectionModel.Select|QtCore.QItemSelectionModel.Rows)


    def requestMenu(self, pos):
        self.menuRequested.emit(self.viewport().mapToGlobal(pos))


    def showHeaderMenu(self, pos):
        # update checkboxes
        for a in self.columnsMenu.actions():
            a.setChecked(not self.horizontalHeader().isSectionHidden(a.data()))

        self.columnsMenu.exec_(self.horizontalHeader().mapToGlobal(pos))


    def sectionVisibilityChanged(self, value):
        index = self.sender().data()
        self.horizontalHeader().setSectionHidden(index, not value)


class JobView(CoalProxyTableView):

    def initHeader(self):
        self.setHorizontalHeader(JobHeaderView(Qt.Horizontal, self))


class WorkerView(CoalProxyTableView):

    def initHeader(self):
        self.setHorizontalHeader(WorkerHeaderView(Qt.Horizontal, self))


class AffinityView(QtWidgets.QTableView, CSSPropertiesMixin):

    def __init__(self, parent=None):
        super().__init__(parent)

        self.horizontalHeader().setVisible(False)

        self.proxyModel = AffinityProxyModel(self)
        super().setModel(self.proxyModel)

        delegate = delegates.AffinityDelegate(self)
        self.setItemDelegate(delegate)


    def setModel(self, model):
        self.proxyModel.setSourceModel(model)


    def setRange(self, start, end):
        self.proxyModel.setRange(start, end)


class TabBar(QtWidgets.QTabBar):

    def minimumSizeHint(self):
        return QtCore.QSize(20,20)