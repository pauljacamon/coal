import re

from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtCore import Qt

from . import jobeditor_ui, workereditor_ui, jobfilters_ui

STATES = ["CANCELED", "ERROR", "FINISHED", "PAUSED", "PENDING", "WAITING", "WORKING"]

class Mixed: pass


class EditorDialog(QtWidgets.QDialog):

    def skimmedAffinities(self, affinities):
        return [ name for name in ( aff['name'] for aff in affinities ) if name ]


    def selectAffinities(self, csv):
        affinities = map(lambda s: s.strip(), csv.split(','))
        for i in range(self.lst_affinities.count()):
            item = self.lst_affinities.item(i)
            item.setSelected(item.text() in affinities)


class NewJobDialog(EditorDialog, jobeditor_ui.Ui_Dialog):

    def __init__(self, affinities, refjob, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.setWindowTitle("New job")
        self.widget_affmode.setVisible(False)

        self.lst_affinities.addItems(self.skimmedAffinities(affinities))

        # if new job from template (any other job) fill in params
        if refjob:
            self.edt_title.setText(refjob["title"])
            self.edt_command.setText(refjob["command"])
            self.edt_directory.setText(refjob["dir"])
            self.edt_env.setPlainText(refjob["environment"])
            self.spn_timeout.setValue(refjob["timeout"])
            self.spn_priority.setValue(refjob["priority"])
            self.selectAffinities(refjob["affinity"])
            self.edt_user.setText(refjob["user"])
            self.edt_url.setText(refjob["url"])
            self.edt_dependencies.setText(refjob["dependencies"])


    def accept(self):
        super().accept()


    def values(self):
        return {
            "title": self.edt_title.text(),
            "command": self.edt_command.text(),
            "dir": self.edt_directory.text(),
            "environment": self.edt_env.toPlainText(),
            "timeout": self.spn_timeout.value(),
            "priority": self.spn_priority.value(),
            "affinity": self.getAffinities(),
            "user": self.edt_user.text(),
            "url": self.edt_url.text(),
            "dependencies": self.edt_dependencies.text(),
        }


    def getAffinities(self):
        return ",".join(item.text() for item in self.lst_affinities.selectedItems())


class EditorAttribute(object):
    '''
        Wrap a job/worker editable attribute stuff, such as its initial value,
        its widget color according to its state (mixed values, modified),
        its 'modified' status according to initial value (indicating if its value
        should be updated)
    '''

    ORIGINAL = 'original'
    MIXED = 'mixed'
    MODIFIED = 'modified'

    def __init__(self, name, widget):
        self.name = name
        self.widget = widget
        self.orig = None


    def initValue(self, value):
        self.orig = value
        if type(value) is Mixed:
            self.setState(self.MIXED)
        else:
            self.setValue(value)


    def setState(self, state):
        self.widget.setProperty('state', state)
        style = self.widget.style()
        style.unpolish(self.widget)
        style.polish(self.widget)


    def updateState(self, value):
        if type(self.orig) is Mixed or value != self.orig:
            self.setState(self.MODIFIED)
        else:
            self.setState(self.ORIGINAL)


    def modified(self):
        return self.widget.property('state') == self.MODIFIED


class NumberAttribute(EditorAttribute):

    def __init__(self, name, widget):
        super().__init__(name, widget)
        self.widget.valueChanged.connect(self.updateState)


    def value(self):
        return self.widget.value()


    def setValue(self, value):
        self.widget.setValue(value)


class TextAttribute(EditorAttribute):

    def __init__(self, name, widget):
        super().__init__(name, widget)
        self.widget.textChanged.connect(self.updateState)


    def value(self):
        return self.widget.text()


    def setValue(self, value):
        self.widget.setText(value)


    def setState(self, state):
        super().setState(state)
        self.widget.setPlaceholderText('different values' if state == self.MIXED else '')


class PlainTextAttribute(EditorAttribute):

    def __init__(self, name, widget):
        super().__init__(name, widget)
        self.widget.textChanged.connect(self.updateState)


    def value(self):
        return self.widget.toPlainText()


    def setValue(self, value):
        self.widget.setPlainText(value)


    def updateState(self):
        super().updateState(self.widget.toPlainText())


class AffinitiesAttribute(EditorAttribute):

    SET = 0
    ADD = 1
    REMOVE = 2
    AFFINITY_MOD_OP = ['', '+', '-']

    def __init__(self, name, widget, op_widget):
        super().__init__(name, widget)
        self.op_widget = op_widget
        self.widget.itemSelectionChanged.connect(self.affinitiesChanged)
        self.op_widget.buttonClicked[int].connect(self.affinitiesChanged)


    def value(self):
        operator = self.AFFINITY_MOD_OP[self.op_widget.checkedId()]
        return operator + ','.join(item.text() for item in self.widget.selectedItems())


    def setValue(self, value):
        affinities = [ s.strip() for s in value.split(',') ]
        for i in range(self.widget.count()):
            item = self.widget.item(i)
            item.setSelected(item.text() in affinities)


    def affinitiesChanged(self):
        mode = self.op_widget.checkedId()
        if mode == self.SET:
            # raw compare between orig. comma separated affinities and selected ones
            cmp_string = ','.join(item.text() for item in self.widget.selectedItems())
            self.updateState(cmp_string)
        elif len(self.widget.selectedItems()) == 0:
            # nothing selected, add/remove won't do anything
            self.setState(self.ORIGINAL)
        else:
            self.setState(self.MODIFIED)


class ScheduleTableAttribute(EditorAttribute):

    def __init__(self, name, widget):
        super().__init__(name, widget)
        self.widget.selectionChanged.connect(self.scheduleTableChanged)


    def value(self):
        return self.widget.schedule()


    def setValue(self, value):
        if type(value) is str and len(value) == 42:
            self.widget.setSchedule(value)
        else:
            self.widget.setSchedule('f'*42)


    def scheduleTableChanged(self):
        self.updateState(self.value())


class ModifyEntityDialog(EditorDialog):

    def fillForm(self, entities):
        first = entities[0]
        for attr in self.attributes:
            if all(entity[attr.name] == first[attr.name] for entity in entities[1:]):
                attr.initValue(first[attr.name])
            else:
                attr.initValue(Mixed())


    def values(self):
        return { attr.name: attr.value() for attr in self.attributes if attr.modified() }


class ModifyJobDialog(ModifyEntityDialog, jobeditor_ui.Ui_Dialog):

    def __init__(self, affinities, jobs, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.setWindowTitle('Edit jobs' if len(jobs) > 1 else 'Edit job')

        self.grp_aff_mod_op = QtWidgets.QButtonGroup(self)
        self.grp_aff_mod_op.addButton(self.rdb_set, AffinitiesAttribute.SET)
        self.grp_aff_mod_op.addButton(self.rdb_add, AffinitiesAttribute.ADD)
        self.grp_aff_mod_op.addButton(self.rdb_remove, AffinitiesAttribute.REMOVE)

        self.lst_affinities.addItems(self.skimmedAffinities(affinities))

        self.attributes = [
            TextAttribute('title', self.edt_title),
            TextAttribute('command', self.edt_command),
            NumberAttribute('priority', self.spn_priority),
            NumberAttribute('timeout', self.spn_timeout),
            TextAttribute('dependencies', self.edt_dependencies),
            TextAttribute('user', self.edt_user),
            TextAttribute('url', self.edt_url),
            PlainTextAttribute('environment', self.edt_env),
            AffinitiesAttribute('affinity', self.lst_affinities, self.grp_aff_mod_op),
        ]

        self.fillForm(jobs)


class ModifyWorkerDialog(ModifyEntityDialog, workereditor_ui.Ui_Dialog):

    def __init__(self, affinities, workers, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.resize(400, 320)
        self.setWindowTitle('Edit workers' if len(workers) > 1 else 'Edit worker')

        self.grp_aff1_mod_op = QtWidgets.QButtonGroup(self)
        self.grp_aff1_mod_op.addButton(self.rdb_aff1_set, AffinitiesAttribute.SET)
        self.grp_aff1_mod_op.addButton(self.rdb_aff1_add, AffinitiesAttribute.ADD)
        self.grp_aff1_mod_op.addButton(self.rdb_aff1_remove, AffinitiesAttribute.REMOVE)

        self.grp_aff2_mod_op = QtWidgets.QButtonGroup(self)
        self.grp_aff2_mod_op.addButton(self.rdb_aff2_set, AffinitiesAttribute.SET)
        self.grp_aff2_mod_op.addButton(self.rdb_aff2_add, AffinitiesAttribute.ADD)
        self.grp_aff2_mod_op.addButton(self.rdb_aff2_remove, AffinitiesAttribute.REMOVE)

        skimmed_affs = self.skimmedAffinities(affinities)
        self.lst_affinities1.addItems(skimmed_affs)
        self.lst_affinities2.addItems(skimmed_affs)

        self.attributes = [
            TextAttribute('description', self.edt_desc),
            AffinitiesAttribute('affinity', self.lst_affinities1, self.grp_aff1_mod_op),
            AffinitiesAttribute('affinity2', self.lst_affinities2, self.grp_aff2_mod_op),
            ScheduleTableAttribute('schedule', self.schedulewidget),
        ]
        self.fillForm(workers)


class FilterJobDialog(EditorDialog, jobfilters_ui.Ui_Dialog):

    # date filter modes
    DATE_MODE = 0
    TIME_MODE = 1

    def __init__(self, affinities, users, filters, parent=None):
        super().__init__(parent)
        self.setupUi(self)

        self._filters = {}

        self.btn_priority_lt.setFixedSize(QtCore.QSize(20,20))
        self.grp_priorityOp = QtWidgets.QButtonGroup(self)
        self.grp_priorityOp.addButton(self.btn_priority_lt, 0)
        self.grp_priorityOp.addButton(self.btn_priority_eq, 1)
        self.grp_priorityOp.addButton(self.btn_priority_gt, 2)
        self.grp_startTimeOp = QtWidgets.QButtonGroup(self)
        self.grp_startTimeOp.addButton(self.btn_startTime_lt, 0)
        self.grp_startTimeOp.addButton(self.btn_startTime_gt, 1)

        self.spn_priority.valueChanged[int].connect(self.sld_priority.setValue)
        self.sld_priority.valueChanged[int].connect(self.spn_priority.setValue)

        self.lst_affinities.addItems(self.skimmedAffinities(affinities))
        self.lst_users.addItems(users)
        self.lst_states.addItems(STATES)

        # load previous filters
        self.loadFilters(filters)

        # events
        self.edt_title.textChanged.connect(self.titleChanged)

        self.grp_priorityOp.buttonClicked.connect(self.priorityChanged)
        self.spn_priority.valueChanged.connect(self.priorityChanged)

        self.edt_startTime.textChanged.connect(self.startTimeChanged)

        self.edt_dependencies.textChanged.connect(self.dependenciesChanged)

        self.lst_affinities.itemSelectionChanged.connect(self.affinityChanged)
        self.lst_users.itemSelectionChanged.connect(self.userChanged)
        self.lst_states.itemSelectionChanged.connect(self.stateChanged)



    def titleChanged(self):
        self.chk_title.setChecked(True)


    def priorityChanged(self):
        self.chk_priority.setChecked(True)


    def startTimeChanged(self):
        self.chk_startTime.setChecked(True)


    def dependenciesChanged(self):
        self.chk_dependencies.setChecked(True)


    def affinityChanged(self):
        self.chk_affinity.setChecked(True)


    def userChanged(self):
        self.chk_user.setChecked(True)


    def stateChanged(self):
        self.chk_state.setChecked(True)


    def loadFilters(self, filters):
        for name, value in filters.items():
            if name == "title":
                self.edt_title.setText(value)
                self.chk_title.setChecked(True)
            elif name == "priority":
                m = re.match(r"([><]?)(\d+)", value)
                if m.group(1) == "<":
                    self.btn_priority_lt.setChecked(True)
                elif m.group(1) == ">":
                    self.btn_priority_gt.setChecked(True)
                self.sld_priority.setValue(int(m.group(2)))
                self.chk_priority.setChecked(True)
            elif name == "start_time":
                if value[0] == '>':
                    self.btn_startTime_gt.setChecked(True)
                self.edt_startTime.setText(value[1:])
                self.chk_startTime.setChecked(True)
            elif name == "affinity":
                for aff in value.split(","):
                    for item in self.lst_affinities.findItems(aff, Qt.MatchExactly):
                        item.setSelected(True)
                self.chk_affinity.setChecked(True)
            elif name == "user":
                for user in value.split(","):
                    for item in self.lst_users.findItems(user, Qt.MatchExactly):
                        item.setSelected(True)
                self.chk_user.setChecked(True)
            elif name == "state":
                for state in value.split(","):
                    for item in self.lst_states.findItems(state, Qt.MatchExactly):
                        item.setSelected(True)
                self.chk_state.setChecked(True)


    def accept(self):
        self._filters.clear()
        if self.chk_title.isChecked():
            self._filters["title"] = self.edt_title.text()
        if self.chk_priority.isChecked():
            op = ">" if self.grp_priorityOp.checkedId() == 2 else "<" if self.grp_priorityOp.checkedId() == 0 else ""
            self._filters["priority"] = op + self.spn_priority.text()
        if self.chk_startTime.isChecked():
            op_bit = self.grp_startTimeOp.checkedId()
            op = ">" if self.grp_startTimeOp.checkedId() == 1 else "<"
            self._filters["start_time"] = op + self.edt_startTime.text()
        if self.chk_affinity.isChecked():
            self._filters["affinity"] = ",".join(item.text() for item in self.lst_affinities.selectedItems())
        if self.chk_user.isChecked():
            self._filters["user"] = ",".join(item.text() for item in self.lst_users.selectedItems())
        if self.chk_state.isChecked():
            self._filters["state"] = ",".join(item.text() for item in self.lst_states.selectedItems())
        super().accept()


    def filters(self):
        return self._filters