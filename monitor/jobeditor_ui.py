# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'jobeditor.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(377, 525)
        self.gridLayout = QGridLayout(Dialog)
        self.gridLayout.setObjectName(u"gridLayout")
        self.edt_env = QPlainTextEdit(Dialog)
        self.edt_env.setObjectName(u"edt_env")

        self.gridLayout.addWidget(self.edt_env, 8, 1, 1, 1)

        self.lbl_dep = QLabel(Dialog)
        self.lbl_dep.setObjectName(u"lbl_dep")
        self.lbl_dep.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.lbl_dep, 5, 0, 1, 1)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.sld_priority = QSlider(Dialog)
        self.sld_priority.setObjectName(u"sld_priority")
        self.sld_priority.setMinimum(1)
        self.sld_priority.setMaximum(100)
        self.sld_priority.setValue(50)
        self.sld_priority.setOrientation(Qt.Horizontal)

        self.horizontalLayout.addWidget(self.sld_priority)

        self.spn_priority = QSpinBox(Dialog)
        self.spn_priority.setObjectName(u"spn_priority")
        self.spn_priority.setMinimumSize(QSize(50, 0))
        self.spn_priority.setAlignment(Qt.AlignCenter)
        self.spn_priority.setButtonSymbols(QAbstractSpinBox.NoButtons)
        self.spn_priority.setMinimum(1)
        self.spn_priority.setMaximum(100)
        self.spn_priority.setValue(50)

        self.horizontalLayout.addWidget(self.spn_priority)


        self.gridLayout.addLayout(self.horizontalLayout, 2, 1, 1, 1)

        self.lbl_env_2 = QLabel(Dialog)
        self.lbl_env_2.setObjectName(u"lbl_env_2")
        self.lbl_env_2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.lbl_env_2, 9, 0, 1, 1)

        self.edt_dependencies = QLineEdit(Dialog)
        self.edt_dependencies.setObjectName(u"edt_dependencies")

        self.gridLayout.addWidget(self.edt_dependencies, 5, 1, 1, 1)

        self.lbl_title = QLabel(Dialog)
        self.lbl_title.setObjectName(u"lbl_title")
        self.lbl_title.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.lbl_title, 0, 0, 1, 1)

        self.lbl_url = QLabel(Dialog)
        self.lbl_url.setObjectName(u"lbl_url")
        self.lbl_url.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.lbl_url, 7, 0, 1, 1)

        self.spn_timeout = QSpinBox(Dialog)
        self.spn_timeout.setObjectName(u"spn_timeout")
        self.spn_timeout.setButtonSymbols(QAbstractSpinBox.NoButtons)

        self.gridLayout.addWidget(self.spn_timeout, 3, 1, 1, 1)

        self.lbl_timeout = QLabel(Dialog)
        self.lbl_timeout.setObjectName(u"lbl_timeout")
        self.lbl_timeout.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.lbl_timeout, 3, 0, 1, 1)

        self.edt_command = QLineEdit(Dialog)
        self.edt_command.setObjectName(u"edt_command")

        self.gridLayout.addWidget(self.edt_command, 1, 1, 1, 1)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setSpacing(4)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.lst_affinities = QListWidget(Dialog)
        self.lst_affinities.setObjectName(u"lst_affinities")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lst_affinities.sizePolicy().hasHeightForWidth())
        self.lst_affinities.setSizePolicy(sizePolicy)
        self.lst_affinities.setSelectionMode(QAbstractItemView.ExtendedSelection)

        self.horizontalLayout_2.addWidget(self.lst_affinities)

        self.widget_affmode = QWidget(Dialog)
        self.widget_affmode.setObjectName(u"widget_affmode")
        self.verticalLayout = QVBoxLayout(self.widget_affmode)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalSpacer_2 = QSpacerItem(10, 0, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_2)

        self.rdb_set = QRadioButton(self.widget_affmode)
        self.rdb_set.setObjectName(u"rdb_set")
        self.rdb_set.setChecked(True)

        self.verticalLayout.addWidget(self.rdb_set)

        self.rdb_add = QRadioButton(self.widget_affmode)
        self.rdb_add.setObjectName(u"rdb_add")

        self.verticalLayout.addWidget(self.rdb_add)

        self.rdb_remove = QRadioButton(self.widget_affmode)
        self.rdb_remove.setObjectName(u"rdb_remove")

        self.verticalLayout.addWidget(self.rdb_remove)

        self.verticalSpacer = QSpacerItem(10, 0, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)


        self.horizontalLayout_2.addWidget(self.widget_affmode)


        self.gridLayout.addLayout(self.horizontalLayout_2, 9, 1, 1, 1)

        self.edt_title = QLineEdit(Dialog)
        self.edt_title.setObjectName(u"edt_title")

        self.gridLayout.addWidget(self.edt_title, 0, 1, 1, 1)

        self.edt_user = QLineEdit(Dialog)
        self.edt_user.setObjectName(u"edt_user")

        self.gridLayout.addWidget(self.edt_user, 6, 1, 1, 1)

        self.edt_directory = QLineEdit(Dialog)
        self.edt_directory.setObjectName(u"edt_directory")

        self.gridLayout.addWidget(self.edt_directory, 4, 1, 1, 1)

        self.edt_url = QLineEdit(Dialog)
        self.edt_url.setObjectName(u"edt_url")

        self.gridLayout.addWidget(self.edt_url, 7, 1, 1, 1)

        self.buttonBox = QDialogButtonBox(Dialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)

        self.gridLayout.addWidget(self.buttonBox, 10, 0, 1, 2)

        self.lbl_env = QLabel(Dialog)
        self.lbl_env.setObjectName(u"lbl_env")
        self.lbl_env.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.lbl_env, 8, 0, 1, 1)

        self.lbl_priority = QLabel(Dialog)
        self.lbl_priority.setObjectName(u"lbl_priority")
        self.lbl_priority.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.lbl_priority, 2, 0, 1, 1)

        self.lbl_user = QLabel(Dialog)
        self.lbl_user.setObjectName(u"lbl_user")
        self.lbl_user.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.lbl_user, 6, 0, 1, 1)

        self.lbl_cmd = QLabel(Dialog)
        self.lbl_cmd.setObjectName(u"lbl_cmd")
        self.lbl_cmd.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.lbl_cmd, 1, 0, 1, 1)

        self.lbl_dir = QLabel(Dialog)
        self.lbl_dir.setObjectName(u"lbl_dir")
        self.lbl_dir.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.lbl_dir, 4, 0, 1, 1)

        QWidget.setTabOrder(self.edt_title, self.edt_command)
        QWidget.setTabOrder(self.edt_command, self.sld_priority)
        QWidget.setTabOrder(self.sld_priority, self.spn_priority)
        QWidget.setTabOrder(self.spn_priority, self.spn_timeout)
        QWidget.setTabOrder(self.spn_timeout, self.edt_directory)
        QWidget.setTabOrder(self.edt_directory, self.edt_dependencies)
        QWidget.setTabOrder(self.edt_dependencies, self.edt_user)
        QWidget.setTabOrder(self.edt_user, self.edt_url)
        QWidget.setTabOrder(self.edt_url, self.lst_affinities)
        QWidget.setTabOrder(self.lst_affinities, self.rdb_set)
        QWidget.setTabOrder(self.rdb_set, self.rdb_add)
        QWidget.setTabOrder(self.rdb_add, self.rdb_remove)

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        self.sld_priority.valueChanged.connect(self.spn_priority.setValue)
        self.spn_priority.valueChanged.connect(self.sld_priority.setValue)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Job Editor", None))
        self.lbl_dep.setText(QCoreApplication.translate("Dialog", u"dependencies", None))
        self.lbl_env_2.setText(QCoreApplication.translate("Dialog", u"affinity", None))
        self.lbl_title.setText(QCoreApplication.translate("Dialog", u"title", None))
        self.lbl_url.setText(QCoreApplication.translate("Dialog", u"url", None))
        self.lbl_timeout.setText(QCoreApplication.translate("Dialog", u"timeout", None))
        self.rdb_set.setText(QCoreApplication.translate("Dialog", u"set", None))
        self.rdb_add.setText(QCoreApplication.translate("Dialog", u"add", None))
        self.rdb_remove.setText(QCoreApplication.translate("Dialog", u"remove", None))
        self.edt_directory.setText(QCoreApplication.translate("Dialog", u".", None))
        self.lbl_env.setText(QCoreApplication.translate("Dialog", u"environment", None))
        self.lbl_priority.setText(QCoreApplication.translate("Dialog", u"priority", None))
        self.lbl_user.setText(QCoreApplication.translate("Dialog", u"user", None))
        self.lbl_cmd.setText(QCoreApplication.translate("Dialog", u"command", None))
        self.lbl_dir.setText(QCoreApplication.translate("Dialog", u"directory", None))
    # retranslateUi

