import sys
import math

from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtCore import Qt

from .csspropertiesmixin import CSSPropertiesMixin


class Index():
    def __init__(self, d, h):
        self.d = self.day = d
        self.h = self.hour = h


class ScheduleWidget(QtWidgets.QWidget, CSSPropertiesMixin):

    # drag modes
    NODRAG = 0
    DRAG_REMOVE = 1
    DRAG_ADD = 2
    DRAG_SET = 3

    HOURS_HEADER_HEIGHT = 20
    DAYS_HEADER_WIDTH = 20
    DAYS = ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su']

    selectionChanged = QtCore.Signal()

    def __init__(self, parent=None):
        super().__init__(parent)

        self._drag = self.NODRAG
        self._start_drag_pos = None
        self._start_index = QtCore.QPoint()
        self._drag_rect = QtCore.QRect()

        self._alt_color = QtGui.QColor(192, 192, 192)
        self._sel_color = QtGui.QColor(128, 255, 128)
        self._drag_rect_color = QtGui.QColor(255,255,255,64)

        self.setMouseTracking(True)

        self._grid_rect = QtCore.QRect()
        self._day_height = 1
        self._hour_width = 1

        self._schedule = 0


    def schedule(self):
        return format(self._schedule, '042x')


    def setSchedule(self, value):
        self._schedule = int(value, 16)


    def indexAt(self, pos):
        """ returns the index of the cell at `pos` """
        relpos = pos-self._grid_rect.topLeft()

        hour = math.floor(relpos.x() / self._hour_width)
        clamped_hour = min(max(hour, 0), 23)
        day = math.floor(relpos.y() / self._day_height)
        clamped_day = min(max(day, 0), 6)

        return Index(clamped_day, clamped_hour)


    def getBit(self, day, hour):
        return 2**(24*(6-day) + (23-hour))

    def paintEvent(self, event):

        painter = QtGui.QPainter(self)
        option = QtWidgets.QStyleOption()
        option.initFrom(self)

        painter.setPen(QtCore.Qt.NoPen)

        total_rect = self.rect()

        days_header_rect = QtCore.QRect(0, self.HOURS_HEADER_HEIGHT, self.DAYS_HEADER_WIDTH, total_rect.bottom() - self.HOURS_HEADER_HEIGHT)

        hours_header_rect = QtCore.QRect(self.DAYS_HEADER_WIDTH, 0, total_rect.right() - self.DAYS_HEADER_WIDTH, self.HOURS_HEADER_HEIGHT)

        self._grid_rect = QtCore.QRect(self.DAYS_HEADER_WIDTH, self.HOURS_HEADER_HEIGHT, total_rect.right() - self.DAYS_HEADER_WIDTH, total_rect.bottom() - self.HOURS_HEADER_HEIGHT)

        self._day_height = self._grid_rect.height() / 7.
        self._hour_width = self._grid_rect.width() / 24.

        # # draw alt columns
        # painter.setBrush(option.palette.color(QtGui.QPalette.AlternateBase))
        # for i in range(12):
        #     if i % 2:
        #         two_hour_rect = QtCore.QRectF(self._grid_rect.left() + i*self._hour_width*2, self._grid_rect.top(), self._hour_width*2, self._grid_rect.height())
        #         painter.drawRect(two_hour_rect)

        # draw selection
        painter.setBrush(option.palette.color(QtGui.QPalette.Highlight))
        for d in range(7):
            for h in range(24):
                bit = self.getBit(d, h)
                is_checked = bit & self._schedule > 0
                rect = QtCore.QRectF(
                    self._grid_rect.left() + h * self._hour_width,
                    self._grid_rect.top() + d * self._day_height,
                    self._hour_width,
                    self._day_height,
                )
                if self._drag != self.NODRAG:
                    if self._drag_rect.intersects(rect.toRect()) or rect.contains(self._drag_rect.topLeft()):
                        is_checked = self._drag == self.DRAG_SET or self._drag == self.DRAG_ADD
                if is_checked:
                    painter.drawRect(rect)

        # DRAW TEXTS
        # ==========
        painter.setPen(option.palette.color(QtGui.QPalette.WindowText))

        # draw hours
        hours_first_half_rect = hours_header_rect.adjusted(0, 0, -hours_header_rect.width()/2, -2)
        painter.drawText(hours_first_half_rect, QtCore.Qt.AlignLeft|QtCore.Qt.AlignBottom, '0h')
        painter.drawText(hours_first_half_rect, QtCore.Qt.AlignCenter|QtCore.Qt.AlignBottom, '6h')
        painter.drawText(hours_header_rect.adjusted(0, 0, 0, -2), QtCore.Qt.AlignCenter|QtCore.Qt.AlignBottom, '12h')
        hours_second_half_rect = hours_header_rect.adjusted(hours_header_rect.width()/2, 0, 0, -2)
        painter.drawText(hours_second_half_rect, QtCore.Qt.AlignCenter|QtCore.Qt.AlignBottom, '18h')
        painter.drawText(hours_second_half_rect, QtCore.Qt.AlignRight|QtCore.Qt.AlignBottom, '24h')

        # draw days
        for i, name in enumerate(self.DAYS):
            day_rect = QtCore.QRectF(days_header_rect.left(), days_header_rect.top() + self._day_height * i, days_header_rect.width()-2, self._day_height)
            painter.drawText(day_rect, QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter, name)

        # draw grid
        painter.setPen(self.bordercolor)
        for i in range(8):
            y_pos = self._grid_rect.top() + self._day_height * i
            painter.drawLine(self._grid_rect.left(), y_pos, self._grid_rect.right(), y_pos)
        for j in range(25):
            x_pos = self._grid_rect.left() + self._hour_width * j
            painter.drawLine(x_pos, self._grid_rect.top(), x_pos, self._grid_rect.bottom())

        # # paint selection rectangle
        # if self._drag:
        #     painter.setPen(Qt.NoPen)
        #     painter.setBrush(self._drag_rect_color)
        #     painter.drawRect(self._drag_rect)


    def sizeHint(self):
        return QtCore.QSize(400, 200)


    def mousePressEvent(self, e):
        if not self._grid_rect.contains(e.pos()):
            self._drag = self.NODRAG
        else:
            self._start_drag_pos = e.pos()
            self._start_index = self.indexAt(e.pos())

            if e.modifiers() & Qt.CTRL > 0:
                self._drag = self.DRAG_REMOVE
            elif e.modifiers() & Qt.SHIFT > 0:
                self._drag = self.DRAG_ADD
            else:
                bit = self.getBit(self._start_index.d, self._start_index.h)
                # if the initial hour pressed is active, mode is "remove/deselect" else mode is "add/select"
                if bit & self._schedule > 0:
                    self._drag = self.DRAG_REMOVE
                else:
                    self._drag = self.DRAG_ADD


    def mouseReleaseEvent(self, e):

        previous_schedule = self._schedule

        # define selection state
        check_state = self._drag == self.DRAG_SET or self._drag == self.DRAG_ADD

        # update schedule table
        current_index = self.indexAt(e.pos())
        h1 = min(self._start_index.h, current_index.h)
        h2 = max(self._start_index.h, current_index.h)
        d1 = min(self._start_index.d, current_index.d)
        d2 = max(self._start_index.d, current_index.d)

        for day in range(d1, d2+1):
            for hour in range(h1, h2+1):
                bit = 2**(24*(6-day) + (23-hour))
                if check_state:
                    # set bit to 1
                    self._schedule |= bit
                else:
                    # set bit to 0
                    self._schedule &= ~bit

        # reset "drag" state
        self._drag = self.NODRAG
        # reset selection rectangle
        self._drag_rect.setCoords(0,0,0,0)

        if self._schedule != previous_schedule:
            self.selectionChanged.emit()

        self.repaint()


    def mouseMoveEvent(self, e):
        if self._drag:
            # update selection rectangle
            self._drag_rect = QtCore.QRectF(self._start_drag_pos, e.pos())
            # repaint
            self.repaint()


if __name__ == '__main__':
    import random
    app = QtWidgets.QApplication([])
    # w = ScheduleWidget('fffffffffffffffffffff000000000000000000000')
    w = ScheduleWidget()
    w.show()
    sys.exit(app.exec_())




'''
zoo coalite python -- -m coalite.monitor.schedulewidget
'''