# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'jobfilters.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(306, 435)
        self.verticalLayout_2 = QVBoxLayout(Dialog)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.widget_jobFilters = QWidget(Dialog)
        self.widget_jobFilters.setObjectName(u"widget_jobFilters")
        self.gridLayout = QGridLayout(self.widget_jobFilters)
        self.gridLayout.setObjectName(u"gridLayout")
        self.chk_user = QCheckBox(self.widget_jobFilters)
        self.chk_user.setObjectName(u"chk_user")

        self.gridLayout.addWidget(self.chk_user, 4, 2, 1, 1)

        self.chk_dependencies = QCheckBox(self.widget_jobFilters)
        self.chk_dependencies.setObjectName(u"chk_dependencies")

        self.gridLayout.addWidget(self.chk_dependencies, 3, 2, 1, 1)

        self.chk_startTime = QCheckBox(self.widget_jobFilters)
        self.chk_startTime.setObjectName(u"chk_startTime")

        self.gridLayout.addWidget(self.chk_startTime, 2, 2, 1, 1)

        self.chk_affinity = QCheckBox(self.widget_jobFilters)
        self.chk_affinity.setObjectName(u"chk_affinity")

        self.gridLayout.addWidget(self.chk_affinity, 6, 2, 1, 1)

        self.lbl_priority = QLabel(self.widget_jobFilters)
        self.lbl_priority.setObjectName(u"lbl_priority")
        self.lbl_priority.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.lbl_priority, 1, 0, 1, 1)

        self.lbl_aff = QLabel(self.widget_jobFilters)
        self.lbl_aff.setObjectName(u"lbl_aff")
        self.lbl_aff.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.lbl_aff, 6, 0, 1, 1)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setSpacing(0)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.btn_priority_lt = QPushButton(self.widget_jobFilters)
        self.btn_priority_lt.setObjectName(u"btn_priority_lt")
        self.btn_priority_lt.setMinimumSize(QSize(20, 20))
        self.btn_priority_lt.setMaximumSize(QSize(20, 20))
        self.btn_priority_lt.setCheckable(True)

        self.horizontalLayout_3.addWidget(self.btn_priority_lt)

        self.btn_priority_eq = QPushButton(self.widget_jobFilters)
        self.btn_priority_eq.setObjectName(u"btn_priority_eq")
        self.btn_priority_eq.setMinimumSize(QSize(20, 20))
        self.btn_priority_eq.setMaximumSize(QSize(20, 20))
        self.btn_priority_eq.setCheckable(True)
        self.btn_priority_eq.setChecked(True)

        self.horizontalLayout_3.addWidget(self.btn_priority_eq)

        self.btn_priority_gt = QPushButton(self.widget_jobFilters)
        self.btn_priority_gt.setObjectName(u"btn_priority_gt")
        self.btn_priority_gt.setMinimumSize(QSize(20, 20))
        self.btn_priority_gt.setMaximumSize(QSize(20, 20))
        self.btn_priority_gt.setCheckable(True)

        self.horizontalLayout_3.addWidget(self.btn_priority_gt)


        self.horizontalLayout.addLayout(self.horizontalLayout_3)

        self.sld_priority = QSlider(self.widget_jobFilters)
        self.sld_priority.setObjectName(u"sld_priority")
        self.sld_priority.setMinimum(1)
        self.sld_priority.setMaximum(100)
        self.sld_priority.setValue(50)
        self.sld_priority.setOrientation(Qt.Horizontal)

        self.horizontalLayout.addWidget(self.sld_priority)

        self.spn_priority = QSpinBox(self.widget_jobFilters)
        self.spn_priority.setObjectName(u"spn_priority")
        self.spn_priority.setMinimumSize(QSize(40, 0))
        self.spn_priority.setAlignment(Qt.AlignCenter)
        self.spn_priority.setButtonSymbols(QAbstractSpinBox.NoButtons)
        self.spn_priority.setMinimum(1)
        self.spn_priority.setMaximum(100)
        self.spn_priority.setValue(50)

        self.horizontalLayout.addWidget(self.spn_priority)


        self.gridLayout.addLayout(self.horizontalLayout, 1, 1, 1, 1)

        self.lbl_user = QLabel(self.widget_jobFilters)
        self.lbl_user.setObjectName(u"lbl_user")
        self.lbl_user.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.lbl_user, 4, 0, 1, 1)

        self.lbl_start = QLabel(self.widget_jobFilters)
        self.lbl_start.setObjectName(u"lbl_start")
        self.lbl_start.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.lbl_start, 2, 0, 1, 1)

        self.lbl_dep = QLabel(self.widget_jobFilters)
        self.lbl_dep.setObjectName(u"lbl_dep")
        self.lbl_dep.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.lbl_dep, 3, 0, 1, 1)

        self.lst_users = QListWidget(self.widget_jobFilters)
        self.lst_users.setObjectName(u"lst_users")
        self.lst_users.setSelectionMode(QAbstractItemView.ExtendedSelection)

        self.gridLayout.addWidget(self.lst_users, 4, 1, 1, 1)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setSpacing(0)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.btn_startTime_lt = QPushButton(self.widget_jobFilters)
        self.btn_startTime_lt.setObjectName(u"btn_startTime_lt")
        self.btn_startTime_lt.setMinimumSize(QSize(20, 20))
        self.btn_startTime_lt.setMaximumSize(QSize(20, 20))
        self.btn_startTime_lt.setCheckable(True)
        self.btn_startTime_lt.setChecked(True)

        self.horizontalLayout_5.addWidget(self.btn_startTime_lt)

        self.btn_startTime_gt = QPushButton(self.widget_jobFilters)
        self.btn_startTime_gt.setObjectName(u"btn_startTime_gt")
        self.btn_startTime_gt.setMinimumSize(QSize(20, 20))
        self.btn_startTime_gt.setMaximumSize(QSize(20, 20))
        self.btn_startTime_gt.setCheckable(True)

        self.horizontalLayout_5.addWidget(self.btn_startTime_gt)


        self.horizontalLayout_4.addLayout(self.horizontalLayout_5)

        self.edt_startTime = QLineEdit(self.widget_jobFilters)
        self.edt_startTime.setObjectName(u"edt_startTime")

        self.horizontalLayout_4.addWidget(self.edt_startTime)


        self.gridLayout.addLayout(self.horizontalLayout_4, 2, 1, 1, 1)

        self.edt_title = QLineEdit(self.widget_jobFilters)
        self.edt_title.setObjectName(u"edt_title")

        self.gridLayout.addWidget(self.edt_title, 0, 1, 1, 1)

        self.edt_dependencies = QLineEdit(self.widget_jobFilters)
        self.edt_dependencies.setObjectName(u"edt_dependencies")

        self.gridLayout.addWidget(self.edt_dependencies, 3, 1, 1, 1)

        self.lbl_title = QLabel(self.widget_jobFilters)
        self.lbl_title.setObjectName(u"lbl_title")
        self.lbl_title.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.lbl_title, 0, 0, 1, 1)

        self.chk_priority = QCheckBox(self.widget_jobFilters)
        self.chk_priority.setObjectName(u"chk_priority")

        self.gridLayout.addWidget(self.chk_priority, 1, 2, 1, 1)

        self.chk_title = QCheckBox(self.widget_jobFilters)
        self.chk_title.setObjectName(u"chk_title")

        self.gridLayout.addWidget(self.chk_title, 0, 2, 1, 1)

        self.lst_affinities = QListWidget(self.widget_jobFilters)
        self.lst_affinities.setObjectName(u"lst_affinities")
        self.lst_affinities.setSelectionMode(QAbstractItemView.ExtendedSelection)

        self.gridLayout.addWidget(self.lst_affinities, 6, 1, 1, 1)

        self.lst_states = QListWidget(self.widget_jobFilters)
        self.lst_states.setObjectName(u"lst_states")
        self.lst_states.setSelectionMode(QAbstractItemView.ExtendedSelection)

        self.gridLayout.addWidget(self.lst_states, 7, 1, 1, 1)

        self.lbl_state = QLabel(self.widget_jobFilters)
        self.lbl_state.setObjectName(u"lbl_state")
        self.lbl_state.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.lbl_state, 7, 0, 1, 1)

        self.chk_state = QCheckBox(self.widget_jobFilters)
        self.chk_state.setObjectName(u"chk_state")

        self.gridLayout.addWidget(self.chk_state, 7, 2, 1, 1)


        self.verticalLayout_2.addWidget(self.widget_jobFilters)

        self.buttonBox = QDialogButtonBox(Dialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)

        self.verticalLayout_2.addWidget(self.buttonBox)

        QWidget.setTabOrder(self.edt_title, self.sld_priority)
        QWidget.setTabOrder(self.sld_priority, self.spn_priority)
        QWidget.setTabOrder(self.spn_priority, self.edt_dependencies)
        QWidget.setTabOrder(self.edt_dependencies, self.buttonBox)
        QWidget.setTabOrder(self.buttonBox, self.lst_users)
        QWidget.setTabOrder(self.lst_users, self.btn_priority_lt)
        QWidget.setTabOrder(self.btn_priority_lt, self.btn_priority_gt)
        QWidget.setTabOrder(self.btn_priority_gt, self.btn_priority_eq)
        QWidget.setTabOrder(self.btn_priority_eq, self.btn_startTime_lt)
        QWidget.setTabOrder(self.btn_startTime_lt, self.btn_startTime_gt)

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Job Filters", None))
        self.chk_user.setText("")
        self.chk_dependencies.setText("")
        self.chk_startTime.setText("")
        self.chk_affinity.setText("")
        self.lbl_priority.setText(QCoreApplication.translate("Dialog", u"priority", None))
        self.lbl_aff.setText(QCoreApplication.translate("Dialog", u"affinity", None))
        self.btn_priority_lt.setText(QCoreApplication.translate("Dialog", u"<", None))
        self.btn_priority_lt.setProperty("barpos", QCoreApplication.translate("Dialog", u"left", None))
        self.btn_priority_eq.setText(QCoreApplication.translate("Dialog", u"=", None))
        self.btn_priority_eq.setProperty("barpos", QCoreApplication.translate("Dialog", u"center", None))
        self.btn_priority_gt.setText(QCoreApplication.translate("Dialog", u">", None))
        self.btn_priority_gt.setProperty("barpos", QCoreApplication.translate("Dialog", u"right", None))
        self.lbl_user.setText(QCoreApplication.translate("Dialog", u"user", None))
        self.lbl_start.setText(QCoreApplication.translate("Dialog", u"start", None))
        self.lbl_dep.setText(QCoreApplication.translate("Dialog", u"dependencies", None))
        self.btn_startTime_lt.setText(QCoreApplication.translate("Dialog", u"<", None))
        self.btn_startTime_lt.setProperty("barpos", QCoreApplication.translate("Dialog", u"left", None))
        self.btn_startTime_gt.setText(QCoreApplication.translate("Dialog", u">", None))
        self.btn_startTime_gt.setProperty("barpos", QCoreApplication.translate("Dialog", u"right", None))
        self.edt_startTime.setPlaceholderText(QCoreApplication.translate("Dialog", u"dd/mm/yy, 5d, 3h, 10m", None))
        self.lbl_title.setText(QCoreApplication.translate("Dialog", u"title", None))
        self.chk_priority.setText("")
        self.chk_title.setText("")
        self.lbl_state.setText(QCoreApplication.translate("Dialog", u"state", None))
        self.chk_state.setText("")
    # retranslateUi

