## Coalition to Coalite

**Coalite** is a fork of [**Mercenaries Engineering**](http://www.mercenaries-engineering.com/) job manager [**Coalition**][coalition-url]

**Coalite** is a lightweight open source **job manager** client-server application whose role is to control **job execution in a set of computers**:

* A computer is acting as a **server**, centralizing the list of jobs to be done.
* Some other computers acting as **workers** ask the server for jobs to perform.
* The server decides which job to attribute to each worker according to **priorities** and **affinities**.
* Once a worker gets a job it executes it immediately, then it informs the server continuously of the job's execution status until it's done, and ask for a new job.
* A monitoring program can be launched anytime on any computer to manage jobs and workers.

*Coalite* is intended to stay easy to work with, it doesn't deal with users, permissions, groups for the moment, therefore it should not be used on the public Internet but on **private LANs**, **cloud VLANs** or **VPN** for security reasons.

*Coalite* provides:

- **Broadcast discovery** for workers to find the server without configuration.
- **RESTful / python API** (based on [**Werkzeug**][2] WSGI library) for server/client (workers and monitors) communication.
- **Monitoring UI** (based on **Qt5/PySide2**) to control jobs, workers, affinities, view status and logs, etc.
- **Unit tests** of critical code parts.

## About Coalite

Coalite originates from [Coalition][coalition-url] but has been mostly entirely rewritten for:

1. fun
2. simplification for easier extension, and thus to easily implement...
3. specific studios demands

It provides, among Coalition basic features:

* **a smoother and cleaner UI to monitor jobs** based on Qt, with customisable and resizable columns for jobs and workers tables.
* **enhanced job filters** to filter by date and priority with "before", "after", "lower than" and "greater than" operators.
* **per worker description**.
* **per worker GPU mermory and GPU load informations**.
* **a log filter system** to catch log informations for 1. enhanced progress report 2. pausing (error) jobs if a specific message is sent by a program.
* **a new Python API**, that is also used by the monitor (previous javascript code is not needed anymore)
* **utility functions** as part of Python API to create job groups with smart "sub-jobs priorities". (ex: higher priority to first/middle/last sub-jobs).
* **a blacklisting system** to allow jobs on error to be processed again by other workers. the max attempts allowed can be specified as a global server option. Workers can be "cleared" from their previous errors (in case the problem was coming from the job and not the workers). A job reset also reset its workers blacklist.

Some original features from [Coalition][coalition-url] have been modified / disabled in the process:

* **MySQL** is not supported, Coalite only works on **SQLite** for the moment. However the interface between the server functions and SQL calls has been refactored so that it would be easier to reimplement other database engines in the future.
* [**Twisted Matrix**](https://twistedmatrix.com/trac) framework (used for communication between the server, workers and monitors) has been replaced by [**Werkzeug**][2] web serving tools and url handling/parsing features: Werkzeug offers very simple serving tools and neat functions to manage a Restful API easily. _Note: Werkzeug documentation explicitly advises not to use it as a web server (or only for development purpose), but in the case of an intranet tool like Coalite, it proved to be sufficient._
* **Python API has changed** (most of the code using [Coalition][coalition-url] legacy API should be updated).
* **Cloud features have been removed**. Not needed for the time being.
* **Email notification system has been removed**. Not needed for the time being.
* **Interface with LDAP server has been removed**. Not needed for the time being.
* **Documentation is not ready yet**.

## Installation

* Coalite works on **Python 3.9**
* Coalite requires the following packages for Python:
	* [**Werkzeug**](https://pypi.org/project/Werkzeug/) (tested with Werkzeug 2.0.2)
	* [**PySide2**](https://pypi.org/project/PySide/) (tested with PySide2 5.15.2)
	* [**psutil**](https://pypi.org/project/psutil/) (tested with colorama 5.8.0)
	* [**colorama**](https://pypi.org/project/colorama/) (tested with colorama 0.4.4)
* Coalite **acts itself as a Python package**
* It is advised to create a [virtual environment](https://docs.python.org/3/library/venv.html) to install these modules (last versions should be OK) and launch coalite's own modules (server, workers and monitor) inside this environment.

## Examples of use

_note: in these examples, python virtual environment is setup inside the batch files directory (%~dp0), next to the coalite package directory:_
- `coalite-1.0/`
	- `coalite/`
	- `python/`
	- `server.bat`
	- `worker.bat`
	- `monitor.bat`

_batch files may be modified according to your setup!_

#### Start server from a batch script (Windows)

```batch
@echo off

:: change the terminal window title
title Coalite Server

:: setup working directory in "home" directory
set COALITE_WORKING_DIR=%USERPROFILE%\.coalite

:: make directory if it doesn't exist yet
if not exist "%COALITE_WORKING_DIR%" (
	mkdir "%COALITE_WORKING_DIR%"
)

echo start server...

:: launch server on port 19110
"%~dp0python\Scripts\python.exe" -m coalite.server --port 19110 --working-dir "%COALITE_WORKING_DIR%"

pause > nul
```

#### Start worker from a batch script (Windows)

```batch
@echo off

:: change the terminal window title
title Coalite Worker

echo start worker...

:: launch worker
"%~dp0python\Scripts\python.exe" -m coalite.worker --port 19110

pause > nul
```

#### Start monitor from a batch script (Windows)

```batch
@echo off

:: change the terminal window title
title Coalite Monitor

echo start monitor...

:: launch monitor
"%~dp0python\Scripts\python.exe" -m coalite.monitor --port 19110

if %ERRORLEVEL% neq 0 pause > nul
```

[coalition-url]: https://github.com/MercenariesEngineering/coalition

