import sys, io, time
import threading
import sys

try:
    import colorama
except ImportError as err:
    print(err)
    colorama = None
else:
    colorama.init()

__all__ = [
    'RESET', 'BRIGHT', 'DIM', 'BLACK', 'RED', 'GREEN', 'YELLOW', 'BLUE',
    'MAGENTA', 'CYAN', 'WHITE', 'cprint', 'timestamp', 'info',
    'error', 'warning', 'debug',
]

class Color():
    def __init__(self, value):
        self.value = '\033[' + str(value) + 'm'


RESET = Color(0)
BRIGHT = Color(1)
DIM = Color(2)
BLACK = Color(30)
RED = Color(31)
GREEN = Color(32)
YELLOW = Color(33)
BLUE = Color(34)
MAGENTA = Color(35)
CYAN = Color(36)
WHITE = Color(37)


lock = threading.Lock()

def cprint(*args, newline=True):
    lock.acquire()
    for arg in args:
        if type(arg) is Color:
            if colorama:
                sys.stdout.write(arg.value)
        else:
            sys.stdout.write(str(arg))
    if args and colorama:
        sys.stdout.write('\033[0m')
    if newline:
        sys.stdout.write('\n')
    lock.release()


def timestamp():
    return '[' + time.strftime("%d-%m-%y %H:%M:%S") + ']'


def info(*args):
    cprint(BRIGHT, timestamp(), '[INF] ', *args)


def error(*args):
    cprint(RED, timestamp(), '[ERR] ', *args)


def warning(*args):
    cprint(YELLOW, timestamp(), '[WRN] ', *args)


def debug(*args):
    cprint(MAGENTA, timestamp(), '[DBG] ', *args)

