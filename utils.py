import os
from . import common, logger


def getenv(name, default=None):
    try:
        return os.environ[name]
    except:
        return default


def getint(name, default=None):
    try:
        return int(os.environ[name])
    except:
        return default


def getbool(name, default=None):
    try:
        return bool(os.environ[name])
    except:
        return default


def broadcast(port, address):
    '''
    Search for Coalite server by broadcasting a key message on
    a given port on the whole LAN and seek for a remote response.
    '''
    import sys
    import socket

    # setup socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, True)
    s.bind(('0.0.0.0', 0))
    # set timeout to 1s
    s.settimeout(1)
    # init dots count
    dots = 1
    while True:
        try:
            sys.stdout.write('Looking for server {} on port {} {:<3}\r'.format(address, port, '.' * dots))
            s.sendto(common.CLIENT_MESSAGE, (address, port))
            data, addr = s.recvfrom(1024)
            if data == common.SERVER_MESSAGE:
                # clear stdout line
                sys.stdout.write(' ' * 80 + '\r')
                # close socket
                s.close()
                # return address of incoming response
                return addr[0]
        except socket.timeout:
            # update dots count
            dots = (dots % 3) + 1