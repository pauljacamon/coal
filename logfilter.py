# - a log filter pattern can contain a range, specified by start and end integer values separated by '->'
# - a log filter pattern can contain regular expressions
# - a log filter can output:
# 		- a progress milestone: its name must be an single percentage indicating the current progress
# 		- a running progress: its name must be an percentage range: 2 integers separated by '-', followed by '%'
# 		  It indicates the progress corresponding to the range found in the pattern: they can be different so
# 		  that a progress value output in the log can be mapped to a different progress value for the job
# 		- an 'error' message: the job is then set to ERROR (and may be picked by another worker)
# 		- a 'cancel' message: the job is then set to CANCELED (and can't be picked by another worker)

# - note: range values must be integers to keep the file simple, but are then treated as float by the parser
#   so that a 0->1 range can output 0.78, for instance

# .logfilters file mini language exemple:
#
# 	0-50%=PROGRESS:0->100
# 	50%=half the work done!
# 	50-100%=PROGRESS2:\s*0->1
# 	100%=done!
# 	error=can't find file
# 	cancel=failed!

import os, re

from . import common
ft = common.LOGFILTER_TYPES


def reverseRead(s):
	# obsolete
	n = len(s)
	i = n-1
	last_lf = n
	while i >= 0:
		if s[i] == "\n":
			yield s[i+1:last_lf]
			last_lf = i
		i-=1
	yield s[:last_lf]


class LogFilterer(object):

	def __init__(self, path):
		self.progress_filters = []
		self.error_filters = []

		# parse .logfilters file
		if os.path.exists(path):
			self.parseFilters(path)

		# init progress and error data
		self.error = [0, -1, -1]
		self.progress = [0, -1, -1]


	def scan(self, line):
		'''
			Scan a single log line to extract any relevant data,
			based on the patterns defined in ".logfilters" file.
		'''
		if self.error_filters and self.error[0] is not None:
			if self.extractError(line):
				return ft.STOP, self.error[1], self.error[2]

		if self.progress_filters:
			if self.extractProgress(line):
				return ft.PROGRESS, self.progress[1], self.progress[2]

		return 0, 0, 0


	def extractProgress(self, line):
		'''
			look for progress data, update `self.progress` and return `True` only if it's actually been updated.
		'''
		for f in self.progress_filters:
			if m:= f['pattern'].search(line):
				if f['type'] == ft.MAPPING:
					self.progress[0] = (float(m.group(1)) * f['slope'] + f['offset']) / 100
				elif f['type'] == ft.MILESTONE:
					self.progress[0] = f['value'] / 100
				self.progress[1] = m.start()
				self.progress[2] = m.end()
				return True
		return False


	def extractError(self, line):
		'''
			look for errors, update `self.error` and return `True` only if it actually found one.
		'''
		for f in self.error_filters:
			if m:= f['pattern'].search(line):
				self.error[0] = f['type']
				self.error[1] = m.start()
				self.error[2] = m.end()
				return True
		return False


	def parseFilters(self, filename):
		'''
			Read ".logfilters" file and call `parseFilter` on each valid line.
		'''
		with open(filename, 'r') as fh:
			for line in fh:
				if not line or line.isspace() or line.lstrip().startswith("#"):
					continue
				try:
					self.parseFilter(line.rstrip("\r\n"))
				except re.error:
					# a regex failed to be compiled
					continue


	def parseFilter(self, line):
		'''
			Parse a ".logfilters" line to extract its type and pattern, and store them
			in `self.pattern_filters` or `self.error_filters` lists accordingly.
		'''
		name, _, pattern = line.partition('=')
		if name == '' or pattern == '':
			return

		if name == 'ERROR':
			self.error_filters.append({ 'type': ft.ERROR, 'pattern': re.compile(pattern) })

		elif name == 'CANCEL':
			self.error_filters.append({ 'type': ft.CANCEL, 'pattern': re.compile(pattern) })

		elif m := re.match(r'^(\d+)%$', name):
			# MILESTONE (single percentage, e.g. "50%")
			self.progress_filters.append({'type': ft.MILESTONE, 'value': float(m.group(1)), 'pattern': re.compile(pattern)})

		elif m := re.match(r'^(\d+)-(\d+)%$', name):
			# MAPPING (percentage range, e.g. "0-50%", mapped to an incoming range, e.g. "0->10")
			m2 = re.search(r'(\d+)->(\d+)', pattern)
			if m2 is None:
				# if no mapped values found, pattern is not valid
				return
			# define the actual pattern to search in log : replace mapped values by a single float number pattern
			pattern = re.compile(pattern[:m2.start()] + r"([0-9.]+)" + pattern[m2.end():])
			# get linear equation slope/offset to compute mapped progress later
			xA, yA, xB, yB = float(m2.group(1)), float(m.group(1)), float(m2.group(2)), float(m.group(2))
			if xB - xA == 0: # the mapped values diff is null, thus invalid
				return
			slope = (yB - yA) / (xB - xA)
			offset = yA - (slope * xA)
			self.progress_filters.append({'type': ft.MAPPING, 'slope': slope, 'offset': offset, 'pattern': pattern})


# TEST
if __name__ == '__main__':
	testfile = __file__.replace("\\", "/").rsplit("/",1)[0] + "test.logfilters"
	with open(testfile, "w") as f:
		f.write("PIPO=/invalidfilter/\n")
		f.write("ERROR=pattern\n")
		f.write("CANCEL=/validregex/\n")
		f.write("CANCEL=/invalidregex(/\n")
		f.write("0-50%=range:0->100%\n")
		f.write("50%=milestone!\n")

	lf = LogFilterer(testfile)
	print(f'{lf.progress_filters=}')
	print(f'{lf.error_filters=}')
