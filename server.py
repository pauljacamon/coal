import os
import re
import json
import time
import datetime
import base64
import argparse
import shutil
import collections

# Web server (Twisted Matrix)
from twisted.web import server, resource, http
from twisted.internet import reactor, task, protocol, error
from twisted.enterprise import adbapi


from coalite import common, logger
js = common.JOB_STATES
rs = common.RUN_STATES
ft = common.LOGFILTER_TYPES

# workers default data (live data not stored in DB)
DEFAULT_WORKER_DATA = {
    'ping_time': 0,
    'cpu': [0],
    'free_memory': 0,
    'total_memory': 0,
    'gpu': [0],
    'gpu_free_mem': [0],
    'gpu_total_mem': [0],
    'ip': '-',
    'pickjob_index': -1,
}

# duration (seconds) since last ping after which a worker is considered offline
WORKER_TIMEOUT = 60
# max number of attempts on different workers before a job is set as canceled
ATTEMPTS_LIMIT = 5

workers_cache = {}
affinities_cache = {}
url_map = { 'GET': [], 'POST': [], 'PUT': [], 'DELETE': [] }


indices = collections.Counter()

dbapi = None


# REST API
# =============================================================================


class Url():

    def __init__(self, pattern, function, use_db):

        self.tokens = []

        def repl(m):
            self.tokens.append(m.group(1))
            return r'(\d+)'

        regex_pattern = '^' + re.sub(r'<([a-z_]+?)>', repl, pattern) + '$'
        self.regex = re.compile(regex_pattern)

        self.function = function
        self.use_db = use_db

        self.args = []
        self.kwargs = {}


    def match(self, path):
        m = self.regex.match(path)
        if m:
            self.args = { key : int(m.group(i+1)) for i, key in enumerate(self.tokens) }
            return True

        return False


def endpoint(method, pattern):
    def decorator(f):
        url_map[method].append(Url(pattern, f, False))
        return f
    return decorator


def db_endpoint(method, pattern):
    def decorator(f):
        url_map[method].append(Url(pattern, f, True))
        return f
    return decorator


def readContent(request):
    content = request.content.read()
    return json.loads(content)


def decodeArgs(request):
    return { k.decode() : v[0].decode() for k, v in request.args.items() }


# CLIENT ENDPOINT FUNCTIONS
# =====================================================


@endpoint('GET', '/infos')
def getInfos(request):
    return {
        'worker_timeout': WORKER_TIMEOUT,
        'attempts_limit': ATTEMPTS_LIMIT
    }


@db_endpoint('GET', '/jobs')
def getJobs(txn, request):
    args = decodeArgs(request)
    filters = getJobFilters(txn, args)
    jobs = dbapi.collectJobs(txn, filters)
    for job in jobs:
        adjustJobData(txn, job)
    return jobs


@db_endpoint('GET', '/jobs/<job_id>')
def getJob(txn, request, job_id):
    job = dbapi.getJobProps(txn, int(job_id))
    if job:
        adjustJobData(txn, job)
    return job


@db_endpoint('GET', '/jobs/<job_id>/dependencies')
def getDependencies(txn, request, job_id):
    return dbapi.collectDependentJobs(txn, int(job_id))


@db_endpoint('GET', '/jobs/<job_id>/childrendependencies')
def getChildrenDependencies(txn, request, job_id):
    return dbapi.getChildrenDependencyIds(txn, int(job_id))


@endpoint('GET', '/jobs/<job_id>/logs/<attempt_id>')
def getLog(request, job_id, attempt_id):
    args = decodeArgs(request)
    pos = int(args.get('pos', 0))
    log_path = getLogPath(int(job_id), int(attempt_id))
    if not os.path.exists(log_path):
        logger.warning('server.getLog : {} not found!'.format(log_path))
        return

    with open(log_path) as fh:

        fh.seek(pos)
        return {
            'job_id': int(job_id),
            'attempt_id': int(attempt_id),
            'log': fh.read(),
            'pos': fh.tell()
        }


@db_endpoint('GET', '/workers')
def getWorkers(txn, request):
    workers = dbapi.collectWorkers(txn)
    for worker in workers:
        # update with workers_cache data
        worker.update(workers_cache.get(worker['name'], DEFAULT_WORKER_DATA))
        worker['affinity'] = getAffinityString(txn, worker['affinity'])
        worker['affinity2'] = getAffinityString(txn, worker['affinity2'])
        worker['ping'] = int(time.time()) - worker['ping_time']
        worker['off_duty'] = int(isOffDuty(worker['schedule']))

        # set worker 'OFFLINE' if duration since last ping is greater than 'WORKER_TIMEOUT'
        if worker['ping'] > WORKER_TIMEOUT:
            worker['state'] = 'OFFLINE'
        elif worker['is_paused']:
            worker['state'] = 'PAUSED'
        elif worker['off_duty']:
            worker['state'] = 'OFFDUTY'
        elif worker['is_working']:
            worker['state'] = 'WORKING'
        else:
            worker['state'] = 'WAITING'

    return workers


@db_endpoint('GET', '/affinities')
def getAffinities(txn, request):
    return dbapi.collectAffinities(txn)


@db_endpoint('GET', '/users')
def getUsers(txn, request):
    return dbapi.listJobsUsers(txn)


@db_endpoint('GET', '/states')
def getStates(txn, request):
    return dbapi.listJobsStates(txn)


@db_endpoint('GET', '/blacklist/<job_id>')
def getBlacklist(txn, request, job_id):
    return dbapi.collectBlacklistedWorkers(txn, int(job_id))


@db_endpoint('GET', '/withdrawn/<worker_id>')
def getWithdrawnJobs(txn, request, worker_id):
    return dbapi.collectWithdrawnJobs(txn, int(worker_id))


@db_endpoint('GET', '/attempts/<job_id>')
def collectAttempts(txn, request, job_id):
    attempts = dbapi.collectAttempts(txn, int(job_id))
    for a in attempts:
        a['state'] = js.getname(a.pop('state_id', 0))
    return attempts


@db_endpoint('POST', '/jobs')
def editJobs(txn, request):
    data = readContent(request)
    for job_id, params in data.items():
        editJob(txn, job_id, params)
    indices['pickjob'] += 1


@db_endpoint('POST', '/jobs/<job_id>/dependencies')
def editJobDepedencies(txn, request, job_id):
    data = readContent(request)
    setJobDependencies(txn, job_id, data)
    indices['pickjob'] += 1


@db_endpoint('POST', '/workers')
def editWorkers(txn, request):
    data = readContent(request)
    for worker_id, params in data.items():
        editWorker(txn, worker_id, params)
    indices['pickjob'] += 1


@db_endpoint('POST', '/affinities')
def editAffinities(txn, request):
    affinities_cache.clear()
    data = readContent(request)
    dbapi.updateAffinities(txn, data)


@db_endpoint('PUT', '/job')
def addSingleJob(txn, request):
    data = readContent(request)
    params = { name : data.get(name, default) for name, default in common.JOB_PARAMS.items() }
    job_id = newJob(txn, params)
    indices['pickjob'] += 1
    return job_id


@db_endpoint('PUT', '/jobs')
def addJobs(txn, request):
    job_ids = []
    data = readContent(request)
    for d in data:
        params = { name : d.get(name, default) for name, default in common.JOB_PARAMS.items() }
        job_ids.append(newJob(txn, params))
    indices['pickjob'] += 1
    return job_ids


@db_endpoint('DELETE', '/jobs')
def deleteJobs(txn, request):
    data = readContent(request)
    for job_id in data:
        deleteJob(txn, job_id)
    indices['pickjob'] += 1


@db_endpoint('DELETE', '/workers')
def deleteWorkers(txn, request):
    data = readContent(request)
    for worker_id in data:
        dbapi.deleteWorker(txn, worker_id)


# WORKERS ENDPOINTS FUNCTIONS
# =====================================================


@db_endpoint('POST', '/w/heartbeat')
def heartbeat(txn, request):
    """ Worker sends heartbeats while running a job:
        Lookup for worker and job, update worker and job """

    remote_ip = request.getClientAddress().host
    data = readContent(request)

    # update worker status, CPU / GPU / memory usage, etc.
    updateWorkerCache(remote_ip, data)

    # check worker's job, has it been changed, deleted, paused, etc.
    worker_id = dbapi.getWorkerId(txn, data['worker_name'])
    run_state = checkRunningJob(txn, data['job_id'], worker_id)
    if run_state == rs.OK:
        if data['progress'] is not None:
            dbapi.updateJob(txn, data['job_id'], progress=data['progress'])
            # update job's log
            updateLog(data['job_id'], data['attempt_id'], data['log'], data['progress'])
    else:
        # set attempt final state to "unfinished"
        dbapi.setAttemptState(txn, data['attempt_id'], js.UNFINISHED)
        if run_state == rs.WORKER_MISSING:
            # insert the missing worker in database
            dbapi.insertWorker(txn, data['worker_name'])
        else:
            dbapi.updateWorker(txn, worker_id, is_working=False)

    return run_state


@db_endpoint('POST', '/w/pickjob')
def pickJob(txn, request):

    remote_ip = request.getClientAddress().host
    data = readContent(request)

    current_time = int(time.time())

    cache = updateWorkerCache(remote_ip, data)

    worker_id = dbapi.getWorkerId(txn, data['worker_name'])
    if worker_id is None:
        # create the worker entry in database
        worker_id = dbapi.insertWorker(txn, data['worker_name'])

    worker = dbapi.getWorkerActivity(txn, worker_id)

    # check if the worker is not already "working" from server POV
    # this can happen if the worker crashed and restarted before
    # timeout is detected
    if worker['is_working']:
        dbapi.updateWorker(txn, worker_id, is_working=False)
        # reset working job assigned to this worker
        job_id = dbapi.getRunningJob(txn, worker_id)
        if job_id:
            updateJobHierarchy(txn, job_id, js.WAITING)

    # worker is paused or is off duty...
    if worker['is_paused'] or isOffDuty(worker['schedule']):
        return

    # check if jobs have changed since last pickjob : if they haven't, return instead of querying database
    if cache['pickjob_index'] == indices['pickjob']:
        # logger.debug('{} pickjob_index up-to-date ({})'.format(data['worker_name'], cache['pickjob_index']))
        return
    # else:
    #     logger.debug('{} pickjob_index ({}) is out-of-date ({}) -> pickjob'.format(data['worker_name'], cache['pickjob_index'], indices['pickjob']))

    # fetch the 1st job matching the worker's affinities
    job = dbapi.pickJob(txn, worker_id)

    # return nothing if there is no job.
    if job is None:
        # update index
        cache['pickjob_index'] = indices['pickjob']
        return

    job_id = job['id']

    # check command
    if job['is_subtask']:
        parent_id = job['parent']
        if parent_id == 0:
            # root jobs can't be subtasks
            updateJobHierarchy(txn, job_id, state=js.CANCELED)
            return
        command = dbapi.getJobProp(txn, parent_id, 'command')
        if command == '':
            # parent command can't be empty
            updateJobHierarchy(txn, job_id, state=js.CANCELED)
            return
        job['command'] = command.replace('START', str(job['subtask_start'])).replace('END', str(job['subtask_end']))

    # job accepted !
    dbapi.updateJob(txn, job_id, worker_id=worker_id, start_time=current_time, duration=0)
    dbapi.updateWorker(txn, worker_id, last_job=job_id, is_working=True)
    updateJobHierarchy(txn, job_id, state=js.WORKING)
    job['attempt_id'] = dbapi.insertAttempt(txn, job_id, worker_id, js.WORKING, current_time)
    return job


@db_endpoint('POST', '/w/endjob')
def endJob(txn, request):
    data = readContent(request)

    # init job state
    job_state = js.FINISHED

    # check job from server point of view
    worker_id = dbapi.getWorkerId(txn, data['worker_name'])
    run_state = checkRunningJob(txn, data['job_id'], worker_id)

    if data['logerror'] == ft.CANCEL:
        # job ended with something in the log indicating it should be canceled
        job_state = js.CANCELED
    elif data['logerror'] == ft.ERROR or data['exitcode'] != 0:
        # job ended with an error (process code or found in the log)
        dbapi.blacklistWorker(txn, worker_id, data['job_id'])
        job_state = js.ERROR
    elif run_state == rs.JOB_PAUSED:
        # job is finished but has been paused in the meantime
        job_state = js.PAUSED
    else:
        # job is finished, set progress bar to 100%
        # dbapi.updateJob(txn, data['job_id'], progress=1)
        dbapi.updateJob(txn, data['job_id'], progress=data['progress'])
        job_state = js.FINISHED

    # update job state, along its parents and children relative states and counters
    updateJobHierarchy(txn, data['job_id'], state=job_state)

    # set attempt final state
    dbapi.setAttemptState(txn, data['attempt_id'], job_state)

    # change worker state to "not working"
    dbapi.updateWorker(txn, data['worker_name'], is_working=False)

    # update job's log
    updateLog(data['job_id'], data['attempt_id'], data['log'], data['progress'])

    # update "pickjob" index
    indices['pickjob'] += 1


@db_endpoint('POST', '/w/shutdown')
def shutdownWorker(txn, request):
    # TODO
    pass


# for method in url_map:
#     print(method)
#     for url in url_map[method]:
#         print(' ', url.function.__name__, url.tokens, url.regex.pattern)


# TWISTED RESOURCE OBJECT
# =============================================================================


class Resource(resource.Resource):

    isLeaf = True

    def __init__(self, dbpool):
        self.dbpool = dbpool


    def render(self, request):
        try:
            url = next( u for u in url_map.get(request.method.decode(), []) if u.match(request.path.decode()) )
        except:
            request.setResponseCode(http.BAD_REQUEST)
            return b''

        def onFinishWithError(failure):
            if failure.type is error.ConnectionLost:
                logger.warning('Connection to [{}] lost during request processing.'.format(request.getClientAddress().host))
            else:
                logger.error(failure)


        nf = request.notifyFinish() # "defered" triggered when request is finished, for any reason (including errors)
        nf.addErrback(onFinishWithError)

        def returnJSON(data):
            if not nf.called:
                request.setHeader(b'content-type', b'application/json')
                request.write(json.dumps(data, separators=(',', ':')).encode())
                request.finish()

        def returnErr(err):
            if not nf.called:
                logger.cprint(logger.RED, err)
                request.setResponseCode(http.INTERNAL_SERVER_ERROR)
                request.finish()

        if url.use_db:
            deferred = self.dbpool.runInteraction(url.function, request, **url.args)
            deferred.addCallback(returnJSON)
            deferred.addErrback(returnErr)

        else:
            data = url.function(request, **url.args)
            returnJSON(data)


        return server.NOT_DONE_YET



# SERVER COMMON FUNCTIONS
# =============================================================================


def isOffDuty(schedule):
    '''
        returns True if worker is "off duty", based on its "schedule" property.
        schedule is a 42 chars string representing each hour for each weekday
        ( 24 hours x 7 days = 168 bits = 21 bytes = 42 hex digits )
    '''
    if len(schedule) == 42:
        now = datetime.datetime.now()
        wd = now.weekday()
        day_schedule_bits = int(schedule[wd*6:(wd+1)*6], 16)
        hour_bit = 2**(23-now.hour)
        return hour_bit & day_schedule_bits == 0

    return False


def parseSince(s):
    try:
        t = time.strptime(s, "%d/%m/%y")
        return time.mktime(t)
    except:
        return


def parseFor(s):
    m = re.match(r'(\d+)([dhm])', s)
    if m is None:
        return
    value = int(m.group(1))
    unit = m.group(2)
    return value*60 if unit == "m" else value*3600 if unit == "h" else value*86400 # if unit == "d"


def getJobFilters(txn, params):
    filters = params.copy()
    filters['parent'] = params.get('parent', 0)
    # PARSE PRIORITY FILTER
    if "priority" in params:
        m = re.match(r"([<>]?)(\d+)", params['priority'])
        if m:
            filters["priority"] = {'op': m.group(1) or "=", 'value': int(m.group(2))}
    # PARSE START TIME FILTER
    if "start_time" in params:
        if m := re.match(r'([<>])(.+)', params['start_time']):
            gt = m.group(1) == '>'
            if since_sec := parseSince(m.group(2)):
                filters["start_time"] = {'op': '>' if gt else '<', 'value': since_sec}
            elif for_sec := parseFor(m.group(2)):
                # now = time.mktime(time.localtime())
                now = int(time.time())
                filters["start_time"] = {'op': '<' if gt else '>', 'value': now-for_sec}

    # PARSE STATES
    if "state" in params:
        filters['state_id'] = [ js.getvalue(name) for name in re.findall(r"\w+", params['state']) ]
    # PARSE USERS
    if "user" in params:
        filters['user'] = re.findall(r"[-\w+.]+", params['user'])
    # MAKE AFFINITY BITS
    if "affinity" in params:
        filters['affinity'] = getAffinityMask(txn, params['affinity'])
    # PARSE DEPENDENCIES
    if "dependencies" in params:
        filters['dependencies'] = re.findall(r"\d+", params['dependencies'])

    return filters


def adjustJobData(txn, job):
    # adjust duration if working and not a group
    state_id = job.pop('state_id')
    job['state'] = js.getname(state_id)
    if state_id == js.WORKING and job['total'] == 0:
        job['duration'] = int(time.time()) - job['start_time']
    # round durations
    job['start_time'] = int(job['start_time'])
    job['duration'] = int(job['duration'])
    #  make affinities string representation
    job['affinity'] = getAffinityString(txn, job['affinity'])


def getAffinityString(txn, affinity_bits):
    if affinity_bits == 0:
        return ""
    if affinity_bits in affinities_cache:
        return affinities_cache[affinity_bits]
    names = []
    # convert signed long to unsigned
    if affinity_bits < 0:
        affinity_bits = (affinity_bits * -1) | 2**63

    affinities = dbapi.collectAffinities(txn)
    for aff in affinities:
        bit = 1 << (aff['id']-1)
        if affinity_bits & bit != 0:
            if aff['name'] != '':
                names.append(aff['name'])
            else:
                # affinity placeholder: affinity has not been given a name yet
                names.append('#'+ str(aff['id']))
    # names.sort()
    result = ",".join(names)
    affinities_cache[affinity_bits] = result
    return result


def newJob(txn, params):
    parent = params['parent']
    # job's parent should be a group ("is_group")
    if parent > 0 and not dbapi.getJobProp(txn, parent, "is_group"):
        logger.error("Can't add job, parent {} is not a group".format(parent))
        return
    if parent < 0:
        logger.error("Can't add job, parent {} is not valid".format(parent))
        return

    state = params.pop("state")
    params["state_id"] = js.getvalue(state)

    # if "command" is empty and job is not a subtask ("is_subtask"), the job
    # is considered a group ("is_group")
    if not params['command'] and not params['is_subtask']:
        params['is_group'] = True

    # get affinity mask
    affinity = params["affinity"]
    params["affinity"] = getAffinityMask(txn, affinity) if affinity else 0

    # clamp priority
    params["priority"] = max(0, min(100, params["priority"]))

    # extract from params, expand & beautify dependency string
    dependencies = params.pop('dependencies')
    if isinstance(dependencies, str):
        dependencies = expandDependencies(dependencies)
    if dependencies:
        params['dependencies'] = shrinkDependencies(dependencies)

    # insert new job
    params['submit_time'] = int(time.time())
    job_id = dbapi.insertJob(txn, params)

    # delete existing logs if any
    # TODO: backup instead ?
    deleteLogs(job_id)

    # update hierarchical values
    updateChildrenHierarchyValues(txn, job_id)

    # update dependencies
    if dependencies:
        dbapi.updateJobDependencies(txn, job_id, dependencies)
        updateJobHierarchy(txn, job_id)

    updateParentCounters(txn, parent)

    return job_id


def editJob(txn, job_id, params):
    # actions on job
    action = params.pop('action', None)
    if action:
        if action == 'pause':
            pauseJob(txn, job_id)
        elif action == 'start':
            startJob(txn, job_id)
        elif action == 'reset':
            resetJob(txn, job_id)
        elif action == 'reseterrors':
            resetErrors(txn, job_id)
        elif action == 'finish':
            finishJob(txn, job_id)
        elif action == 'move':
            parent_id = params.get('parent', 0)
            moveJob(txn, job_id, parent_id)

    # filter out parameters
    params.pop('parent', None) # remove parent
    priority = params.pop('priority', None)
    affinity = params.pop('affinity', None)
    data = { k : params.pop(k) for k in common.JOB_PARAMS if k in params } # remove any name not in JOB_PARAMS
    for k in params:
        logger.warning("editJob(): unhandled parameter:", k)

    # special cases:
    update_hierarchy = False
    # 1. priority: clamp value and trigger hierarchy update
    if priority:
        data["priority"] = min(max(int(priority), 0), 100)
        update_hierarchy = True
    # 2. affinities: set/add/remove affinities and trigger hierarchy update
    if affinity:
        new_bits, op = parseAffinityString(txn, affinity)
        if op is not None:
            # fetch previous affinity mask
            previous_bits = dbapi.getJobProp(txn, job_id, "affinity")
            if op == "+":
                # union (a | b) with previous mask
                data["affinity"] = previous_bits | new_bits
            elif op == "-":
                # substract (a & ~b) from previous mask
                data["affinity"] = previous_bits & ~new_bits
        else:
            data["affinity"] = new_bits
        update_hierarchy = True

    raw_dependencies = params.pop("dependencies", None)

    # outputs a warning if some unhandled parameters remain
    for k in params:
        logger.warning("editJobs(): unhandled parameter: ", k)

    if data:
        # update job values
        dbapi.updateJob(txn, job_id, **data)
        # update hierarchical values (h_priority / h_affinity)
        if update_hierarchy:
            updateChildrenHierarchyValues(txn, job_id)

    # update dependencies
    if raw_dependencies:
        dependencies = expandDependencies(raw_dependencies)
        setJobDependencies(txn, job_id, dependencies)


def pauseJob(txn, job_id):
    if dbapi.getJobProp(txn, job_id, "state_id") != js.FINISHED:
        updateJobHierarchy(txn, job_id, state=js.PAUSED)


def startJob(txn, job_id):
    if dbapi.getJobProp(txn, job_id, "state_id") == js.PAUSED:
        updateJobHierarchy(txn, job_id, state=js.WAITING)


def _resetJob(txn, job_id, only_errors=False):

    def recurse(id):
        if not only_errors or dbapi.getJobProp(txn, id, 'state_id') in (js.CANCELED, js.ERROR):
            # remove job from blacklist
            dbapi.clearJob(txn, id)
            # reset start time and run count
            dbapi.updateJob(txn, id, start_time=0, run_done=0, worker_id=-1)
            # set as 'WAITING'
            updateJobHierarchy(txn, id, js.WAITING, skip_recount=True) # postpone recount

        for child_id in dbapi.listJobChildrenIds(txn, id):
            recurse(child_id)

    # recursively reset the job and its children
    recurse(job_id)

    # recount
    # updateChildrenHierarchyValues(txn, job_id)
    updateParentCounters(txn, job_id)


def resetJob(txn, job_id):
    _resetJob(txn, job_id, False)


def resetErrors(txn, job_id):
    _resetJob(txn, job_id, True)


def finishJob(txn, job_id):

    def recurse(id):
        updateJobHierarchy(txn, id, js.FINISHED, skip_recount=True) # postpone recount

        for child_id in dbapi.listJobChildrenIds(txn, id):
            recurse(child_id)

    # recursively mark the job and its children as "finished"
    recurse(job_id)

    # recount
    # updateChildrenHierarchyValues(txn, job_id)
    updateParentCounters(txn, job_id)


def deleteJob(txn, job_id):

    def recursive_delete(id):
        for child_id in dbapi.listJobChildrenIds(txn, id):
            recursive_delete(child_id)
        # delete job in DB
        dbapi.deleteJob(txn, id)
        # delete logs in filesystem
        deleteLogs(job_id)

    # keep aside parent id before deleting
    parent = dbapi.getJobProp(txn, job_id, "parent")

    # delete job and its children
    recursive_delete(job_id)

    if parent is not None:
        updateParentCounters(txn, parent)


def moveJob(txn, job_id, parent_id):

    if isValidParent(txn, job_id, parent_id):

        # put aside previous parent id
        previous_parent_id = dbapi.getJobProp(txn, job_id, "parent")
        # change job parent id
        dbapi.moveJob(txn, job_id, parent_id)
        # update its children
        updateChildrenHierarchyValues(txn, job_id)
        # update previous parent counters
        updateParentCounters(txn, previous_parent_id)
        # update new parent counters
        updateParentCounters(txn, parent_id)


def isValidParent(txn, id, parent):
    """ check for acyclic parenting graph. """
    while parent != 0:
        if id == parent:
            return False
        parent = dbapi.getJobProp(txn, parent, "parent")
    return True


def setJobDependencies(txn, job_id, dependencies):
    depstring = shrinkDependencies(dependencies)
    dbapi.updateJobDependencies(txn, job_id, dependencies)
    dbapi.updateJob(txn, job_id, dependencies=depstring)
    updateJobHierarchy(txn, job_id)


def shrinkDependencies(deplist):
    """
        Converts depency list of integers into a "comma-separated-values"
        string, with consecutive ids shrunken into #-# patterns.
    """
    items = []
    deplist.sort()
    n = len(deplist)
    i = 0
    while i < n:
        start = deplist[i]
        while i < n-1 and deplist[i]+1 == deplist[i+1]:
            i+=1
        end = deplist[i]
        if end - start >= 2:
            items.append(str(start)+"-"+str(end))
        elif end - start == 1: # avoid ranges of only 2 numbers (ex: 120-121 becomes 120,121)
            items.append(str(start))
            items.append(str(end))
        else:
            items.append(str(start))
        i+=1
    return ", ".join(items)


def expandDependencies(depstring):
    """
        Converts depency string into a list of integers.
    """
    result = []
    for item in re.findall(r"[\d-]+", depstring):
        if item.isdigit():
            result.append(int(item))
        else:
            m = re.match(r"^(\d+)-(\d+)$", item)
            if m:
                result.extend(range(int(m.group(1)), int(m.group(2))+1))
            # note: invalid patterns like 180-190- or -7--12 are simply ignored
    return list(set(result))


def getBitwiseAffinities(txn):
    result = {}
    for row in dbapi.collectAffinities(txn):
        id, name = row['id'], row['name']
        if name != '' and id >= 1 and id <= 64:
            result[name] = 1 << (id-1)
    return result


def parseAffinityString(txn, affinity_string):
    '''
        Parse affinity string coming from job or worker update parameters.
        This string may or may not begins with a + or - operator indicating
        the operation to perform on the previous bits. No operator means
        bits are replaced, +/- means union/substraction with previous bits.
    '''
    if len(affinity_string) > 0 and affinity_string[0] in '-+':
        return getAffinityMask(txn, affinity_string[1:]), affinity_string[0]
    else:
        return getAffinityMask(txn, affinity_string), None


def getAffinityMask(txn, affinity_string):
    if affinity_string == "":
        return 0
    # TODO: possible/useful to cache the result ?
    bitwise_affinities = getBitwiseAffinities(txn)
    mask = 0
    for m in re.finditer(r"(#(\d+)|\w+)", affinity_string):
        affinity = m.group(1).strip()
        if m.group(2) is not None:
            # is an affinity placeholder(i.e. #12)
            bit = (int(m.group(2)) - 1)
            mask |= (1 << bit)
        else:
            mask |= bitwise_affinities.get(affinity, 0)
        # convert to signed long
        if mask >= 2**63:
            mask = (mask & ~2**63) * -1
    return mask


def updateJobHierarchy(txn, job_id, state=None, skip_recount=False):
    """
        Update the job state if provided, it's children hierarchical values,
        (h_paused, h_affinity, h_priority) and it's dependencies states.
    """
    job = dbapi.getJobProps(txn, job_id, 'state_id', 'run_done', 'start_time', 'parent')

    if job is None:
        logger.debug("updateJobHierarchy called on a non-existing job ID!")
        return

    if state is None:
        state = job['state_id']
    # switch job from WAITING to PENDING or the opposite, depending on job
    if state in (js.WAITING, js.PENDING):
        state = js.PENDING if dbapi.isJobPending(txn, job_id) else js.WAITING
    if state == js.ERROR and job['run_done'] + 1 >= ATTEMPTS_LIMIT:
        state = js.CANCELED

    if state == job['state_id']:
        # state unchanged
        return

    params = {'state_id': state}

    if state in (js.FINISHED, js.ERROR, js.CANCELED):
        # update 'duration' and increment 'run_done' if job ended
        params['duration'] = int(time.time()) - job['start_time']
        params['run_done'] = job['run_done'] + 1
        # send notifications
        if state == js.FINISHED:
            notifyFinished(job)
        else:
            notifyError(job)

    dbapi.updateJob(txn, job_id, **params)
    updateDependentJobsState(txn, job_id)
    if not skip_recount:
        updateChildrenHierarchyValues(txn, job_id)
        updateParentCounters(txn, job["parent"])


def updateDependentJobsState(txn, job_id):
    for dep_id in dbapi.listDependentJobIds(txn, job_id):
        updateJobHierarchy(txn, dep_id)


def updateChildrenHierarchyValues(txn, job_id):
    """ update children hierarchical values (h_priority, h_affinity, h_paused) """
    data = dbapi.getJobHierarchyData(txn, job_id)
    h_depth = data['parent_depth'] + 1

    dbapi.updateJob(txn, job_id,
        h_depth=h_depth,
        h_affinity=data['parent_affinity'] | data['affinity'],
        h_priority=data['parent_priority'] + (data['priority'] << (56-h_depth*8)),
        h_paused=data['parent_paused'] or data['state_id'] in (js.PAUSED, js.PENDING) or 0,
    )
    for child_id in dbapi.listJobChildrenIds(txn, job_id):
        updateChildrenHierarchyValues(txn, child_id)


def updateParentCounters(txn, parent_id):
    """ update parent counters, state and progress
        according to its children actual states """
    if parent_id == 0:
        return

    # 1. UPDATE COUNTERS
    current_time = int(time.time())
    total = 0
    working = 0
    errors = 0
    finished = 0
    total_working = 0
    total_errors = 0
    total_finished = 0
    start_time = 0
    duration = 0
    for child in dbapi.collectJobChildrenStats(txn, parent_id):
        state = child['state_id']

        if child['is_group']:
            # child is a group: add group counts to current
            total_working += child['total_working']
            total_errors += child['total_errors']
            total_finished += child['total_finished']
            total += child['total']
        else:
            # child is a job: increment counters accordingly
            total += 1
            if state == js.WORKING:
                working += 1
            elif state in (js.ERROR, js.CANCELED):
                errors += 1
            elif state == js.FINISHED:
                finished += 1

        if child['start_time'] != 0:
            if start_time == 0:
                # init 'start_time'
                start_time = child['start_time']
            else:
                # set 'start_time' to minimum of all children start times
                start_time = min(start_time, child['start_time'])

        if state in (js.ERROR, js.CANCELED, js.FINISHED):
            duration += child['duration']
        elif state == js.WORKING:
            duration += current_time - child['start_time']

    # add job children working/error/finished counts to total counts (including subgroups counts)
    total_working += working
    total_errors += errors
    total_finished += finished

    data = {
        'working': working,
        'errors': errors,
        'finished': finished,
        'total_working': total_working,
        'total_errors': total_errors,
        'total_finished': total_finished,
        'total': total,
        'start_time': start_time,
        'duration': duration,
    }

    dbapi.updateJob(txn, parent_id, **data)

    if total > 0:
        # 2. UPDATE STATE
        job = dbapi.getJobProps(txn, parent_id, 'state_id', 'progress')
        if job and job['state_id'] != js.PAUSED:
            # define job state after counting/sorting its children...
            if total_errors > 0:
                new_state = js.ERROR
            elif total_finished == total:
                new_state = js.FINISHED
            elif total_working > 0:
                new_state = js.WORKING
            elif dbapi.isJobPending(txn, parent_id):
                new_state = js.PENDING
            else:
                new_state = js.WAITING
            # ... and if its state has changed
            if new_state != job['state_id']:
                dbapi.updateJob(txn, parent_id, state_id=new_state)
                # ...send notifications...
                if new_state == js.FINISHED:
                    notifyFinished(job)
                elif new_state == js.ERROR:
                    notifyError(job)
                # ...unpause children if no longer 'PENDING'
                if new_state == js.WAITING and job['state_id'] == js.PENDING:
                    updateChildrenHierarchyValues(txn, parent_id)
                # ... inform dependent jobs if 'FINISHED'
                if new_state == js.FINISHED:
                    updateDependentJobsState(txn, parent_id)

            # 3. UPDATE PROGRESS
            progress = float(total_finished) / total
            if progress != job['progress']:
                dbapi.updateJob(txn, parent_id, progress=progress)


    grand_parent_id = dbapi.getJobProp(txn, parent_id, 'parent')
    if grand_parent_id:
        updateParentCounters(txn, grand_parent_id)


def notifyFinished(job):
    pass


def notifyError(job):
    pass


def editWorker(txn, worker_id, params):
    action = params.pop('action', None)
    if action:
        if action == 'start':
            startWorker(txn, worker_id)
        elif action == 'stop':
            stopWorker(txn, worker_id)
        elif action == 'clear':
            clearWorker(txn, worker_id)

    data = { k: params.pop(k) for k in common.WORKER_PARAMS if k in params }

    for k in params:
        logger.warning("editWorker(): unhandled parameter:", k)

    for aff_name in ['affinity', 'affinity2']:
        aff_string = data.pop(aff_name, None)
        if aff_string is None:
            continue
        new_bits, op = parseAffinityString(txn, aff_string)
        if not op: # SET
            data[aff_name] = new_bits
        else: # ADD / REMOVE
            # fetch previous affinity mask
            prev_data = dbapi.getWorkerAffinities(txn, worker_id)
            previous_bits = prev_data[aff_name]
            if op == "+":
                # union (a | b) with previous mask
                data[aff_name] = previous_bits | new_bits
            elif op == "-":
                # substract (a & ~b) from previous mask
                data[aff_name] = previous_bits & ~new_bits

    if len(data) > 0:
        dbapi.updateWorker(txn, worker_id, **data)


def stopWorker(txn, worker_id):
    dbapi.updateWorker(txn, worker_id, is_paused=1)
    # set running job to WAITING
    job_id = dbapi.getRunningJob(txn, worker_id)
    if job_id is not None:
        updateJobHierarchy(txn, job_id, js.WAITING)


def startWorker(txn, worker_id):
    dbapi.updateWorker(txn, worker_id, is_paused=0)


def clearWorker(txn, worker_id):
    dbapi.clearWorker(txn, worker_id)


def getWorkerCache(name):
    ''' return existing cached data or init new data from `DEFAULT_WORKER_DATA` '''
    return workers_cache.get(name) or workers_cache.setdefault(name, DEFAULT_WORKER_DATA.copy())


def updateWorkerCache(ip, data):
    cache = getWorkerCache(data['worker_name'])
    cache['ip'] = ip
    cache['ping_time'] = int(time.time())
    cache.update(data)
    return cache


def checkRunningJob(txn, job_id, worker_id):
    '''
        Check if heartbeat sent by worker match data in database,
        otherwise try to identify and repair any inconsistency.
    '''
    if worker_id is None:
        return rs.WORKER_MISSING

    if dbapi.isWorkerRunningJob(txn, worker_id, job_id):
        return rs.OK

    activity = dbapi.getWorkerActivity(txn, worker_id)
    if activity['is_paused']:
        # worker is paused, switch its `is_working` property off
        dbapi.updateWorker(txn, worker_id, is_working=False)
        return rs.WORKER_PAUSED

    job = dbapi.getJobProps(txn, job_id, 'h_paused', 'state_id', 'worker_id')

    if job is None:
        return rs.JOB_DELETED

    elif job['h_paused'] == 1:
        return rs.JOB_PAUSED

    elif job['state_id'] == js.FINISHED:
        # job has been marked as finished
        return rs.JOB_ENDED

    elif job['state_id'] != js.WORKING:
        logger.error('worker {} runs job {} but job state is {}'.format(worker_id, job_id, js.getname(job['state_id'])))
        return rs.WRONG_STATE

    elif job['worker_id'] != worker_id:
        logger.error('worker {} runs job {} but job worker_id is {}'.format(worker_id, job_id, job['worker_id']))
        return rs.WRONG_WORKER

    logger.error('worker {} runs job {} but `isWorkerRunningJob()` returned `False`'.format(worker_id, job_id))
    return rs.OK


def getLogDir(job_id):
    fmt_id = str(job_id).zfill(5)
    return './logs/{}/{}/{}'.format(fmt_id[:-4], fmt_id[-4:-2], fmt_id[-2:])


def getLogPath(job_id, attempt_id, mkdir=False):
    log_dir = getLogDir(job_id)
    if mkdir:
        os.makedirs(log_dir, exist_ok=True)
    return '{}/{}.log'.format(log_dir, attempt_id)


def updateLog(job_id, attempt_id, log, progress, **extra):
    decoded = base64.b64decode(log)
    log_path = getLogPath(job_id, attempt_id, mkdir=True)
    with open(log_path, 'ab') as fh:
        # write log
        fh.write(decoded)
        # # write the cause of the job's termination.
        # if run_state == rs.WORKER_PAUSED:
        #     fh.write('[server] worker is paused.\n'.encode())
        # elif run_state == rs.JOB_DELETED:
        #     fh.write('[server] job doesn\'t exist anymore.\n'.encode())
        # elif run_state == rs.JOB_PAUSED:
        #     fh.write('[server] job hierarchy is paused.\n'.encode())
        # elif run_state == rs.WRONG_STATE:
        #     fh.write('[server] wrong job state.\n'.encode())
        # elif run_state == rs.WRONG_WORKER:
        #     fh.write('[server] wrong worker.\n'.encode())


def deleteLogs(job_id):
    logs_dir = getLogDir(job_id)
    if os.path.exists(logs_dir):
        shutil.rmtree(logs_dir, ignore_errors=True)


def watchWorkers(txn):
    """
        Check if workers are still "alive" and reset their task if
        they don't give sign of life until `WORKER_TIMEOUT` is reached
    """
    for worker in dbapi.collectWorkers(txn):
        if not worker['is_working']:
            # skip workers records which are not "working"
            continue
        reset = False
        cache = workers_cache.get(worker['name'])
        if cache is None:
            # logger.debug('watcher: {} not found in active workers cache'.format(worker_name))
            # worker has not been registered in cache, must be offline
            reset = True
        else:
            # if delta between now and worker's last ping is higher than `WORKER_TIMEOUT`, reset job
            delta = int(time.time()) - cache['ping_time']
            if delta > WORKER_TIMEOUT:
                # logger.debug('watcher: {} didn\'t respond for {:.2f}s'.format(worker_name, delta))
                reset = True

        if reset:
            dbapi.updateWorker(txn, worker['id'], is_working=False)
            job_id = dbapi.getRunningJob(txn, worker['id'])
            if job_id:
                logger.debug('watcher: reset job #{}'.format(job_id))
                resetJob(txn, job_id)


class WorkerListener(protocol.DatagramProtocol):
    """
        Listen to workers broadcast signals and returns the server adress.
    """
    def datagramReceived(self, data, addr):
        if data == common.CLIENT_MESSAGE:
            self.transport.write(common.SERVER_MESSAGE, addr)


if __name__ == '__main__':
    # parse arguments
    parser = argparse.ArgumentParser(prog='coal server', description='Start Coalite server.')
    parser.add_argument('-p', '--port', metavar='PORT', type=int, default=19111, help='Port used by the server (default: 19211)')
    parser.add_argument('-v', '--verbose', metavar='LEVEL', type=int, default=0, help='Increase verbosity (default: 0)')
    parser.add_argument('-e', '--engine', metavar='PATH', choices=['sqlite'], default='sqlite', help='Specify database engine (default: sqlite)')
    parser.add_argument('-w', '--working-dir', metavar='PATH', default='.', help='Specify working directory (default: .)')
    args = parser.parse_args()

    logger.info(f'change directory to {os.path.abspath(args.working_dir)}')
    os.chdir(args.working_dir)

    if args.engine == 'sqlite':
        from coalite import db_sqlite
        if not os.path.exists(db_sqlite.DB_NAME):
            logger.debug('init DB...')
            db_sqlite.init()
        dbpool = adbapi.ConnectionPool('sqlite3', db_sqlite.DB_NAME, check_same_thread=False, cp_min=1, cp_max=1)
        dbapi = db_sqlite

    # Start the UDP listener used to answers workers trying to connect.
    logger.info(f'start "worker listener"...')
    reactor.listenUDP(args.port, WorkerListener())

    # worker watcher setup
    def start_ww_loop():
        logger.info(f'start "worker check"...')
        # start "worker watcher" loop, which checks for workers "timeout"
        task.LoopingCall(dbpool.runInteraction, watchWorkers).start(1) # TODO defer start ?

    # defer "worker watcher" loop `WORKER_TIMEOUT` seconds after reactor
    # starts to avoid resetting jobs while workers are reconnecting
    task.deferLater(reactor, WORKER_TIMEOUT, start_ww_loop)

    def startup_message():
        logger.info(f'Coalite server is running on port {args.port}')

    # run server
    site = server.Site(Resource(dbpool))
    reactor.listenTCP(args.port, site)
    reactor.callLater(0, startup_message)
    logger.info(f'start "reactor"...')
    reactor.run()