import os
import getpass

from PySide2 import QtCore, QtGui, QtWidgets

import coalite.api.utils
import coalite.api.urllib3api


COALITE_HOST = os.getenv('COALITE_HOST')
COALITE_PORT = os.getenv('COALITE_PORT')

PRIORITY_COLORS = [
    "#6D7EB1",
    "#6E9BB6",
    "#6FBBB9",
    "#70BFA0",
    "#71C583",
    "#80CA71",
    "#A4CF72",
    "#CCD373",
    "#D9B973",
    "#DE9473",
    "#E3747C",
]

SPLIT_MODES = {
    0: 'chunk',     # frames per chunk
    1: 'total',     # number of chunks
    2: 'balanced',  # frames per chunk balanced
}

DISTRIB_MODES = {
    0: 'linear',    # frame order
    1: 'flm',       # first, last, middle
    2: 'vdc',       # Van Der Corput sequence
    3: 'fractal',   # fractal distribution
}


MANDATORY_KEYS = {'mode', 'cmd', 'title'}

def mix(X, Y, A):
    ''' QColor mixer: return a interpolated QColor between X and Y according to value A '''
    def _mix(x, y, a):
        return x*(1-a) + y*a
    return QtGui.QColor(
        _mix(X.red(), Y.red(), A),
        _mix(X.green(), Y.green(), A),
        _mix(X.blue(), Y.blue(), A),
        _mix(X.alpha(), Y.alpha(), A)
    )


def polish(widget):
    widget.style().unpolish(widget)
    widget.style().polish(widget)


class Error(Exception): pass


class ColorRamp(QtCore.QVariantAnimation):

    def __init__(self, colorset, parent=None):
        super(ColorRamp, self).__init__(parent)
        self.setDuration(100)
        n = float(len(colorset)-1)
        for i, code in enumerate(colorset):
            self.setKeyValueAt(i/n, QtGui.QColor(code))


    def updateCurrentValue(self, value):
        pass


    def valueAt(self, time):
        self.setCurrentTime(time)
        return self.currentValue()


class Settings(dict):

    FALSE = {'false', 'no', 'off', '0'}

    def getbool(self, key, default=False):
        try:
            value = self.__getitem__(key)
        except:
            return default

        if isinstance(value, str):
            return len(value) > 0 and value.lower() not in self.FALSE
        return bool(value)

    def getint(self, key, default=0):
        try:
            value = self.__getitem__(key)
        except:
            return default

        return int(value)


class Label(QtWidgets.QLabel):

    def __init__(self, text, parent=None, width=80):

        super(Label, self).__init__(parent)
        self.setText(text)
        self.setFixedWidth(width)
        self.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)


class LineEdit(QtWidgets.QLineEdit):

    def __init__(self, parent=None):
        super(LineEdit, self).__init__(parent)
        self.setFrame(False)


class SpinBox(QtWidgets.QSpinBox):

    def __init__(self, parent=None):

        super(SpinBox, self).__init__(parent)
        self.setMinimumSize(QtCore.QSize(60, 0))
        self.setFrame(False)
        self.setButtonSymbols(QtWidgets.QAbstractSpinBox.NoButtons)


class GridLayout(QtWidgets.QGridLayout):
    """
        Conveniency class to help GridLayout setup
    """
    def __init__(self, parent=None):
        self._row = 0
        self._col = 0
        super(GridLayout, self).__init__(parent)


    def add(self, item, span=1):
        if isinstance(item, QtWidgets.QLayout):
            self.addLayout(item, self._row, self._col, 1, span)
        else:
            self.addWidget(item, self._row, self._col, 1, span)
        self._col += span


    def addRow(self):
        self._row += 1
        self._col = 0


class BasePathEdit(QtWidgets.QWidget):
    """
        Provide a simple widget to select and edit a system path.
    """

    def __init__(self, mode, parent=None):

        self.mode = mode

        super(BasePathEdit, self).__init__(parent)

        layout = QtWidgets.QHBoxLayout(self)
        layout.setContentsMargins(0,0,0,0)
        layout.setSpacing(4)

        self.lineEdit = LineEdit(self)
        layout.addWidget(self.lineEdit)

        self.button = QtWidgets.QPushButton(self)
        self.button.setObjectName('PathEditButton')
        layout.addWidget(self.button)

        self.button.clicked.connect(self.onButtonClicked)


    def onButtonClicked(self):
        raise NotImplementedError


    def text(self):
        return self.lineEdit.text()


    def setText(self, text):
        self.lineEdit.setText(text)


class DirPathEdit(BasePathEdit):

    def __init__(self, parent=None):
        super(DirPathEdit, self).__init__(parent)
        self.button.setText('...')


    def onButtonClicked(self):
        result = QtWidgets.QFileDialog.getExistingDirectory(self, 'Select Directory...', self.lineEdit.text())
        if result:
            self.setText(result.replace('\\', '/'))


class FilePathEdit(BasePathEdit):

    def __init__(self, parent=None):
        super(DirPathEdit, self).__init__(parent)
        self.button.setText('...')


    def onButtonClicked(self):
        result, _ = QtWidgets.QFileDialog.getOpenFileName(self, 'Select File...', self.lineEdit.text())
        if result:
            self.setText(result.replace('\\', '/'))


class BaseSubmitterDialog(QtWidgets.QDialog):

    TITLE = "Coalite Submitter"

    # job mode
    SINGLE = 1      # a single job is sent
    SEQUENCE = 2    # a parent job with subtasks are sent

    def __init__(self, api, parent=None):

        self.api = api
        self.settings = Settings()

        super(BaseSubmitterDialog, self).__init__(parent=parent)
        self.setupUi()

        self.setWindowTitle('{} - {}'.format(self.TITLE, self.api.host))

        self.priority_palette = self.spn_priority.palette()
        base = self.priority_palette.color(QtGui.QPalette.Base)
        self.ramp = ColorRamp([mix(base, QtGui.QColor(c), 0.8) for c in PRIORITY_COLORS])

        self.sld_priority.valueChanged[int].connect(self.updatePriority)
        self.spn_priority.valueChanged[int].connect(self.updatePriority)

        self.cbb_distrib_mode.currentIndexChanged.connect(self.updateDistribPriorities)

        self.reset()


    def setupUi(self):

        # self.resize(400, 600)

        main_layout = QtWidgets.QVBoxLayout(self)

        self.grpbox_job_opts = QtWidgets.QGroupBox(self)
        self.setupJobOptionsUi(self.grpbox_job_opts)
        main_layout.addWidget(self.grpbox_job_opts)

        self.grpbox_seq_opts = QtWidgets.QGroupBox(self)
        self.setupSequenceUi(self.grpbox_seq_opts)
        main_layout.addWidget(self.grpbox_seq_opts)

        self.grpbox_app_opts = QtWidgets.QGroupBox(self)
        self.setupAppUi(self.grpbox_app_opts)
        main_layout.addWidget(self.grpbox_app_opts)

        self.btn_submit = QtWidgets.QPushButton(self)
        self.btn_submit.setText("Submit")
        self.btn_submit.setMinimumSize(QtCore.QSize(0, 30))
        main_layout.addWidget(self.btn_submit)

        self.btn_submit.clicked.connect(self.accept)
        self.sld_priority.valueChanged[int].connect(self.spn_priority.setValue)
        self.spn_priority.valueChanged[int].connect(self.sld_priority.setValue)


    def setupJobOptionsUi(self, grpbox):

        grpbox.setTitle('job options')

        layout = GridLayout(grpbox)

        # NAME

        job_name_label = Label("name", self)
        layout.add(job_name_label)

        self.edt_name = LineEdit(grpbox)
        layout.add(self.edt_name)

        # GROUP

        layout.addRow()

        lbl_group = Label("group", self)
        layout.add(lbl_group)

        self.edt_group = LineEdit(grpbox)
        layout.add(self.edt_group)

        # PRIORITY

        layout.addRow()

        lbl_job_priority = Label('priority', self)
        layout.add(lbl_job_priority)

        lay_priority = QtWidgets.QHBoxLayout()

        self.sld_priority = QtWidgets.QSlider(grpbox)
        self.sld_priority.setMinimum(0)
        self.sld_priority.setMaximum(100)
        self.sld_priority.setPageStep(1)
        self.sld_priority.setProperty('value', 50)
        self.sld_priority.setOrientation(QtCore.Qt.Horizontal)
        lay_priority.addWidget(self.sld_priority)

        self.spn_priority = SpinBox(grpbox)
        self.spn_priority.setMaximum(100)
        self.spn_priority.setProperty('value', 50)
        lay_priority.addWidget(self.spn_priority)

        layout.add(lay_priority)

        # AFFINITY

        layout.addRow()

        lbl_affinities = Label('affinity', self)
        layout.add(lbl_affinities)

        self.lst_affinities = QtWidgets.QListWidget(grpbox)
        self.lst_affinities.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.lst_affinities.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        layout.add(self.lst_affinities)



    def setupSequenceUi(self, grpbox):

        grpbox.setTitle('range options')

        layout = GridLayout(grpbox)

        # START / END

        lbl_start = Label('start', self)
        layout.add(lbl_start)

        lay_range = QtWidgets.QHBoxLayout()

        self.spn_start = SpinBox(grpbox)
        self.spn_start.setMaximum(16777215)
        lay_range.addWidget(self.spn_start)

        lbl_end = Label('end', self, 40)
        lay_range.addWidget(lbl_end)

        self.spn_end = SpinBox(grpbox)
        self.spn_end.setMaximum(16777215)
        lay_range.addWidget(self.spn_end)

        layout.add(lay_range)

        # SPLIT MODE

        layout.addRow()

        lbl_split_mode = Label('split mode', self)
        layout.add(lbl_split_mode)

        lay_split_mode = QtWidgets.QHBoxLayout()

        self.cbb_split_mode = QtWidgets.QComboBox(grpbox)
        self.cbb_split_mode.addItem("frames per chunk:")
        self.cbb_split_mode.addItem("frames per chunk (balanced):")
        self.cbb_split_mode.addItem("number of chunks:")
        lay_split_mode.addWidget(self.cbb_split_mode)

        self.spn_split_count = SpinBox(grpbox)
        self.spn_split_count.setMinimum(1)
        self.spn_split_count.setMaximum(100)
        self.spn_split_count.setValue(5)
        lay_split_mode.addWidget(self.spn_split_count)

        layout.add(lay_split_mode)

        # DISTRIB MODE

        layout.addRow()

        lbl_distrib_mode = Label('distrib mode', self)
        layout.add(lbl_distrib_mode)

        self.cbb_distrib_mode = QtWidgets.QComboBox(grpbox)
        self.cbb_distrib_mode.addItem("linear (frame order)")
        self.cbb_distrib_mode.addItem("first, last, middle")
        self.cbb_distrib_mode.addItem("random (van der Corput)")
        # self.cbb_distrib_mode.addItem("Fractal (test)")
        layout.add(self.cbb_distrib_mode)

        layout.addRow()

        lbl_from = Label('low', self)
        layout.add(lbl_from)

        lay_distrib_priorities = QtWidgets.QHBoxLayout()

        self.spn_distrib_low = SpinBox(grpbox)
        self.spn_distrib_low.setMaximum(100)
        self.spn_distrib_low.setValue(20)
        lay_distrib_priorities.addWidget(self.spn_distrib_low)

        lbl_to = Label('high', self, 40)
        lay_distrib_priorities.addWidget(lbl_to)

        self.spn_distrib_high = SpinBox(grpbox)
        self.spn_distrib_high.setMaximum(100)
        self.spn_distrib_high.setValue(80)
        lay_distrib_priorities.addWidget(self.spn_distrib_high)

        layout.add(lay_distrib_priorities)


    def setupAppUi(self, grpbox): # type: (QGroupBox) -> None
        """
            Setup the "app" specific options which could be used inside `buildJobs`
        """
        raise NotImplementedError


    def loadSettings(self): # type: () -> Mapping[str, Any]
        """
            Reimplement to return the settings dict from the current context.
        """
        raise NotImplementedError


    def saveSettings(self): # type: () -> None
        """
            Reimplement to save settings in the current context.
        """
        raise NotImplementedError


    def getDefaultJobName(self): # type: () -> str
        """
            Reimplement to return a default job name from the current context
        """
        raise NotImplementedError


    def getDefaultGroup(self): # type: () -> str
        raise NotImplementedError


    def getDefaultAffinities(self): # type: () -> Sequence[str]
        raise NotImplementedError


    def getDefaultPriority(self): # type: () -> int
        raise NotImplementedError


    def getFramerange(self): # type: () -> tuple[int, int]
        raise NotImplementedError


    def buildJobs(self): # type: () -> Sequence[Mapping[str, Any]]
        raise NotImplementedError


    def reset(self):

        # EXTRACT SETTINGS FROM SCENE
        self.settings.clear()
        previous_settings = self.loadSettings()
        if previous_settings:
            self.settings.update(previous_settings)

        # JOB NAME
        job_name = self.getDefaultJobName()
        # self.edt_name.setText(self.settings.get('name', job_name))
        self.edt_name.setText(job_name)

        # JOB GROUP
        job_group = self.getDefaultGroup()
        # self.edt_group.setText(self.settings.get('group', PROJECT_NAME))
        self.edt_group.setText(job_group)

        # FETCH AFFINITIES
        self.lst_affinities.clear()
        affinities = self.api.collectAffinities()
        named_affinities = [ name for name in ( aff['name'] for aff in affinities ) if name ]
        self.lst_affinities.addItems(named_affinities)
        self.restoreListSelection(self.lst_affinities, 'pools', self.getDefaultAffinities())

        # SET PRIORITY
        priority = self.settings.getint('priority', self.getDefaultPriority())
        self.sld_priority.setValue(priority)
        self.updatePriority(priority)

        # SET START / END
        start, end = self.getFramerange()
        # self.spn_start.setValue(self.settings.getint('start', start))
        self.spn_start.setValue(start)
        # self.spn_end.setValue(self.settings.getint('end', end))
        self.spn_end.setValue(end)

        # SET SPLIT MODE/COUNT
        self.cbb_split_mode.setCurrentIndex(self.settings.getint('split_mode', 0))
        self.spn_split_count.setValue(self.settings.getint('split_count', 5))

        # SET DISTRIB MODE
        distrib_mode = self.settings.getint('distrib_mode', 0)
        self.cbb_distrib_mode.setCurrentIndex(distrib_mode)
        self.updateDistribPriorities(distrib_mode)

        # SET START / END
        start, end = self.getFramerange()
        # self.spn_start.setValue(self.settings.getint('start', start))
        self.spn_start.setValue(start)
        # self.spn_end.setValue(self.settings.getint('end', end))
        self.spn_end.setValue(end)


    def restoreListSelection(self, list_widget, setting_name, default=[]):
        s_list = self.settings.get(setting_name)
        L = s_list.split(",") if s_list else default
        for i in range(list_widget.count()):
            item = list_widget.item(i)
            if item.text() in L:
                item.setSelected(True)


    def closeEvent(self, e):
        # save settings
        self.saveSettings()
        return super(BaseSubmitterDialog, self).closeEvent(e)


    def updatePriority(self, value):
        self.spn_priority.setStyleSheet('background-color:' + self.ramp.valueAt(value).name())
        polish(self.spn_priority)


    def updateDistribPriorities(self, index):
        use_priorities = index > 0
        self.spn_distrib_low.setEnabled(use_priorities)
        self.spn_distrib_high.setEnabled(use_priorities)


    def getGroupJobId(self, name):
        # look for an existing group
        jobs = self.api.collectJobs({'title': name, 'parent': 0})
        if jobs:
            return jobs[0]['id']
        else:
            # group doesn't exist, create it
            return self.api.newJob(title=name, parent=0)


    def accept(self):

        try:
            jobs = self.buildJobs()
        except Error as err:
            QtWidgets.QMessageBox.warning(self, self.TITLE, str(err))
            return

        # check jobs
        if any( key not in job for job in jobs for key in MANDATORY_KEYS ):
            QtWidgets.QMessageBox.warning(self, self.TITLE, 'Invalid job(s) provided')
            return

        group_name = self.edt_group.text()
        if group_name:
            # get / create group
            parent_id = self.getGroupJobId(group_name)
        else:
            parent_id = 0

        priority = self.sld_priority.value()
        affinity = ';'.join(item.text() for item in self.lst_affinities.selectedItems())
        split = SPLIT_MODES.get(self.cbb_split_mode.currentIndex())
        size = self.spn_split_count.value()
        distrib = DISTRIB_MODES.get(self.cbb_distrib_mode.currentIndex())
        low_prio = self.spn_distrib_low.value()
        high_prio = self.spn_distrib_high.value()

        job_ids = []

        for job in jobs:
            is_group = job['mode'] == self.SEQUENCE
            params = {
                'command' :job['cmd'],
                'title': job['title'],
                'user': getpass.getuser(),
                'priority': priority,
                'affinity': affinity,
                'parent': parent_id,
                'is_group': is_group,
            }
            job_id = self.api.newJob(**params)

            if is_group:
                # 2. create tasks (children)
                tasks = coalite.api.utils.split_by_chunks_2(job_id, job['start'], job['end'], split, size, distrib, low_prio, high_prio)
                self.api.newJobs(tasks)

            job_ids.append(job_id)

        super(BaseSubmitterDialog, self).accept()

        QtWidgets.QMessageBox.warning(self, self.TITLE, '{} job(s) created!'.format(len(job_ids)))


    def submitSingle(self):
        return False, 'single job not implemented yet!'


    @classmethod
    def showDialog(cls, parent=None):
        # init api instance
        api = coalite.api.urllib3api.CoaliteAPI(COALITE_HOST, COALITE_PORT)
        try:
            # test connection
            api.getInfos()
        except:
            text = 'Connection to Coalite ({}) timed out'.format(COALITE_HOST)
            QtWidgets.QMessageBox.warning(parent, cls.TITLE, text)
            return

        dlg = cls(api, parent)
        dlg.show()


class TestSubmitterDialog(BaseSubmitterDialog):

    def setupAppUi(self, grpbox):
        grpbox.setTitle("app options")
        layout = QtWidgets.QVBoxLayout(grpbox)
        layout.addStretch()


    def loadSettings(self):
        return {}


    def saveSettings(self):
        pass


    def getDefaultJobName(self):
        return ''


    def getDefaultGroup(self):
        return ''


    def getDefaultAffinities(self):
        return []


    def getDefaultPriority(self):
        return 50


    def getFramerange(self):
        return 1, 100


    def buildJobs(self):
        return []


class MockupCoaliteAPI():

    def __init__(self, host, port):

        self.host = host
        self.port = port

    def collectAffinities(self):
        return [{'id': 1, 'name': 'pif'}, {'id': 2, 'name': 'paf'}]


    def collectJobs(self, params):
        return []


    def newJob(self, **params):
        return {}


    def newJob(self, new_jobs):
        return []


if __name__ == '__main__':
    import sys
    # api = coalite.api.urllib3api.CoaliteAPI(COALITE_HOST, COALITE_PORT)
    # try:
    #     # test connection
    #     api.getInfos()
    # except:
    #     print('Connection to Coalite ({}) timed out'.format(COALITE_HOST))
    # else:
    #     app = QtWidgets.QApplication([])
    #     dlg = SubmitterDialog(api)
    #     dlg.show()
    #     sys.exit(app.exec_())

    api = MockupCoaliteAPI(COALITE_HOST, COALITE_PORT)

    app = QtWidgets.QApplication([])
    dlg = TestSubmitterDialog(api)
    dlg.show()
    sys.exit(app.exec_())