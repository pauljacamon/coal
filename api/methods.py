from coalite import common

class MethodsMixin(object):

	def get(path, params):
		raise NotImplementedError


	def post(path, params):
		raise NotImplementedError


	def put(path, params):
		raise NotImplementedError


	def delete(path, params):
		raise NotImplementedError


	# GET METHODS

	def getInfos(self):
		return self.get("/infos")


	def collectJobs(self, params):
		return self.get("/jobs", params)


	def getJob(self, job_id):
		return self.get("/jobs/{}".format(job_id))


	def getJobChildren(self, job_id):
		return self.get("/jobs", {"parent": job_id})


	def getLog(self, job_id, attempt_id, pos=0):
		return self.get("/jobs/{}/logs/{}".format(job_id, attempt_id), {'pos': pos})


	def collectUsers(self):
		return self.get("/users")


	def collectWorkers(self):
		return self.get("/workers")


	def getJobDependencies(self, job_id):
		return self.get("/jobs/{}/dependencies".format(job_id))


	def collectAffinities(self):
		return self.get("/affinities")


	def collectBlacklistedWorkers(self, job_id):
		return self.get("/blacklist/{}".format(job_id))


	def collectAttempts(self, job_id):
		return self.get("/attempts/{}".format(job_id))

	# POST METHODS

	def setJobDependencies(self, job_id, dep_ids):
		return self.post("/jobs/{}/dependencies".format(job_id), dep_ids)


	def setAffinities(self, data):
		return self.post("/affinities", data)


	def editJobs(self, data):
		# filter parameters before sending to server
		for job_id, params in data.items():
			params.pop('parent', None)
			data[job_id] = { k : params[k] for k in params if k in common.JOB_PARAMS }
		return self.post("/jobs", data)


	def jobAction(self, job_ids, action):
		data = { job_id : { 'action': action } for job_id in job_ids }
		return self.post("/jobs", data)


	def pauseJobs(self, job_ids):
		return self.jobAction(job_ids, 'pause')


	def resumeJobs(self, job_ids):
		return self.jobAction(job_ids, 'start')


	def resetJobs(self, job_ids):
		return self.jobAction(job_ids, 'reset')


	def resetErrors(self, job_ids):
		return self.jobAction(job_ids, 'reseterrors')


	def finishJobs(self, job_ids):
		return self.jobAction(job_ids, 'finish')


	def moveJobs(self, job_ids, parent):
		data = { job_id : {'action': 'move', 'parent': parent} for job_id in job_ids }
		return self.post("/jobs", data)


	def editWorkers(self, data):
		# filter parameters before sending to server
		for wrk_id, params in data.items():
			data[wrk_id] = { k : params[k] for k in params if k in common.WORKER_PARAMS }
		return self.post("/workers", data)


	def workerAction(self, names, action):
		data = { name : { 'action': action } for name in names }
		return self.post("/workers", data)


	def startWorkers(self, names):
		return self.workerAction(names, 'start')


	def stopWorkers(self, names):
		return self.workerAction(names, 'stop')


	def clearWorkers(self, names):
		return self.workerAction(names, 'clear')

	# PUT METHODS

	def newJob(self, **params):
		job = { name : params.get(name, default) for name, default in common.JOB_PARAMS.items() }
		return self.put("/job", job)


	def newJobs(self, new_jobs):
		""" new_jobs is a list of param. dictionaries for each job to be added
			param. dictionaries can be incomplete, since they are updated with
			JOB_DEFAULT dictionary """
		jobs = []

		if type(new_jobs) is not list:
			raise ValueError('`new_jobs` type must be `list`')

		if any(type(item) is not dict for item in new_jobs):
			raise ValueError('`new_jobs` items must be `dict`')

		for params in new_jobs:
			jobs.append({ name : params.get(name, default) for name, default in common.JOB_PARAMS.items() })
		if jobs:
			return self.put("/jobs", jobs)

	# DELETE METHODS

	def deleteJobs(self, job_ids):
		return self.delete("/jobs", job_ids)


	def deleteWorkers(self, names):
		return self.delete("/workers", names)
