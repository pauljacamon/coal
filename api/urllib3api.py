import urllib3
try:
	# python 3
	from urllib.parse import urlencode
except:
	# python 2
	from urllib import urlencode
import json

from .methods import MethodsMixin
from . import utils

# timeout before request raising an error
REQUEST_TIMEOUT = urllib3.util.Timeout(connect=5.0, read=10.0)

class Error(Exception): pass


class CoaliteAPI(MethodsMixin):

	def __init__(self, host, port):
		self.http = urllib3.PoolManager(retries=False, timeout=REQUEST_TIMEOUT)
		if host is None:
			host = utils.broadcast(port)
		self.port = port
		self.host = host


	def _send(self, method, path, params=None, data=None):
		url = 'http://{}:{}{}'.format(self.host, self.port, path)
		headers = {}

		if params:
			url += '?' + urlencode(params)

		if data:
			body = json.dumps(data)
			headers['Content-Type'] = 'application/json'
		else:
			body = None

		resp = self.http.request(method, url, body=body, headers=headers)

		if resp.status == 200:
			if resp.data:
				return json.loads(resp.data)
		else:
			raise Error('HTTP {} Error: {}'.format(resp.status, resp.reason))


	def get(self, path, params=None):
		return self._send('GET', path, params=params)


	def post(self, path, data):
		return self._send('POST', path, data=data)


	def put(self, path, data):
		return self._send('PUT', path, data=data)


	def delete(self, path, data):
		return self._send('DELETE', path, data=data)
