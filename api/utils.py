import re
import math
import socket

from .. import common


def split_by_frames(params, start, end, distrib='linear', low_p=20, high_p=80):
	""" Builds a list of sub-jobs (represented as parameter dictionaries) based
		on a given "master" parameter dictionary containing at least a command
		including the FRAME keyword """
	assert "command" in params and re.search(r"\bFRAME\b", params["command"])
	result = []
	length = end-start+1

	priorities = get_priorities(length, distrib, low_p, high_p)

	for i in range(length):
		job = params.copy()
		job["title"] = "frame {}".format(start+i)
		job["priority"] = priorities[i]
		job["command"] = re.sub(r"\bFRAME\b", str(start+i) , job["command"])
		result.append(job)
	return result


def split_by_chunks(params, start, end, split='total', size=5, distrib='linear', low_p=20, high_p=80):
	""" Builds a list of sub-jobs (represented as parameter dictionaries) based
		on a given "master" parameter dictionary containing at least a command
		including START and END keywords """
	assert "command" in params and re.search(r"\bSTART\b", params["command"]) and re.search(r"\bEND\b", params["command"])
	result = []

	# MAKE CHUNKS
	chunks = get_chunks(start, end, split, size)

	# MAKE PRIORITIES
	priorities = get_priorities(len(chunks), distrib, low_p, high_p)

	command = params.get("command")
	is_subtask = params.get("is_subtask")

	# MAKE CHUNKS DATA
	for i, chunk in enumerate(chunks):
		job = params.copy()
		if chunk[0] == chunk[1]:
			job["title"] = "frame {}".format(chunk[0])
		else:
			job["title"] = "chunk #{:03d} ({:04d}-{:04d})".format(i+1, chunk[0], chunk[1])
		job["priority"] = int(priorities[i])
		job["command"] = re.sub(r"\bEND\b", str(chunk[1]), re.sub(r"\bSTART\b", str(chunk[0]), command))
		result.append(job)

	return result

def split_by_chunks_2(parent, start, end, split='total', size=5, distrib='linear', low_p=20, high_p=80):
	"""
		Builds a list of sub-jobs (represented as parameter dictionaries) based
		on a given "master" parameter dictionary
	"""
	result = []

	# MAKE CHUNKS
	chunks = get_chunks(start, end, split, size)

	# MAKE PRIORITIES
	priorities = get_priorities(len(chunks), distrib, low_p, high_p)

	# MAKE CHUNKS DATA
	for i, (start, end) in enumerate(chunks):
		# make title, depending on start/end (single or multi)
		if start == end:
			title = "frame {}".format(start)
		else:
			title = "chunk #{:03d} ({:04d}-{:04d})".format(i+1, start, end)
		# make/append data dict
		result.append({
			'parent': parent,
			'title': title,
			'priority': priorities[i],
			'is_subtask': True,
			'subtask_start': start,
			'subtask_end': end,
		})

	return result


def get_chunks(start, end, split='total', size=5):

	total = end - start + 1
	if size >= total:
		return [(i, i) for i in range(start, end+1)]

	chunks = []
	# total: job is split in [size] chunks of the same size.
	# last chunk size may be smaller.
	if split == 'total':
		chunk_size = total / float(size)
		i = start
		while i < end:
			first = int(i+0.5)
			last = int(i+chunk_size-0.5)
			chunks.append((first, last))
			i += chunk_size
	# chunk size: job is split in chunks of size [size]
	# last chunk size may be smaller.
	elif split == 'chunk':
		i = start
		while i <= end:
			chunks.append((i, min(end, i+size-1)))
			i += size
	# normalized chunk size: same as "chunk size", but chunks,
	# despite being slightly smaller than specified size, are
	# balanced to avoid last chunk size difference.
	elif split == 'balanced':
		chunk_num = total / float(size)
		chunk_size = total / float(int(chunk_num+1)) # +0.5 would be even more balanced, but size may be bigger than specified, which may not desired...
		i = start
		while i < end:
			first = int(i+0.5)
			last = int(i+chunk_size-0.5)
			chunks.append((first, last))
			i += chunk_size

	return chunks


def lerp(a, b, t):
    ''' Linear interpolated value of t between a and b '''
    return (1 - t) * a + t * b


def flm_priorities(n, low, high):
	''' first, then last, then middle, then frame order '''
	L = [int(low)] * n
	L[0] = int(high)
	L[-1] = int(high)
	L[int(n/2)] = int((high+low) / 2)
	return L


def fractal_priorities(n, low, high):
    """ return a list of priorities with fractally distributed values between 20 and 80 """
    import math
    octaves = int(math.log(n, 2))
    L = [0] * n
    for o in range(octaves):
        offset = (math.pi*(pow(2, o+1))) / (n-1)
        for i in range(n):
            f = math.cos(i*offset)/2 + 0.5
            L[i] += f
    # scale resulting priorities between `low` and `high`
    return [ int(lerp(low, high, p/octaves)) for p in L ]


def vdc(n, base):
    ''' Van Der Corput number generator '''
    result = 0
    denom = 1
    while n > 0:
        denom *= base
        n, remainder = divmod(n, base)
        result += remainder / float(denom)
    return result


def vdc_priorities(n, low, high, base):
    return [ int(lerp(low, high, 1-vdc(i, base))) for i in range(n) ]


def get_priorities(n, mode, low, high):
	if mode == 'flm':
		return flm_priorities(n, low, high)
	if mode == 'vdc':
		return vdc_priorities(n, low, high, 3)
	if mode == 'fractal':
		return fractal_priorities(n, low, high)
	if mode == 'linear':
		return [50] * n

	raise Exception('Invalid distrib mode: {}'.format(mode))


def broadcast(port, limit=4, mask='192.168.20.11'):
	'''
	Search for Coalite server by broadcasting a key message on
	a given port on the whole LAN and seek for a remote response.
	'''
	# setup socket
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, True)
	s.bind(('0.0.0.0', 0))
	# set timeout to 1s
	s.settimeout(1)
	for i in range(limit):
		try:
			s.sendto(common.CLIENT_MESSAGE, (mask, int(port)))
			data, addr = s.recvfrom(1024)
			if data == common.SERVER_MESSAGE:
				# close socket
				s.close()
				# return address of incoming response
				return addr[0]
		except socket.timeout:
			continue