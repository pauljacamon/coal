import sys
import urllib.parse
import json

from PySide2.QtCore import QUrl
from PySide2.QtNetwork import QNetworkAccessManager, QNetworkRequest

from coalite.api.methods import MethodsMixin

QT_OP_TO_HTTP_METHOD = {
    QNetworkAccessManager.GetOperation : 'GET',
    QNetworkAccessManager.PutOperation : 'PUT',
    QNetworkAccessManager.PostOperation : 'POST',
    QNetworkAccessManager.CustomOperation : 'DELETE',
}
class CoaliteAPI(QNetworkAccessManager, MethodsMixin):

    def __init__(self, host, port):
        super().__init__()
        self.host = host
        self.port = port
        self.base_url = QUrl()
        self.base_url.setScheme('http')
        self.base_url.setHost(host)
        self.base_url.setPort(port)

        self.query_count = 0


    def _build_request(self, path, params=None):
        url = QUrl(self.base_url)
        url.setPath(path)
        if params:
            query = urllib.parse.urlencode(params)
            url.setQuery(query, QUrl.StrictMode)
        request = QNetworkRequest(url)
        request.setHeader(QNetworkRequest.ContentTypeHeader, 'application/json')
        return request


    def get(self, path, params=None):
        return super().get(self._build_request(path, params))


    def post(self, path, data):
        return super().post(self._build_request(path), json.dumps(data).encode())


    def put(self, path, data):
        return super().put(self._build_request(path), json.dumps(data).encode())


    def delete(self, path, data):
        # deleteResource
        return super().sendCustomRequest(self._build_request(path), b'DELETE', json.dumps(data).encode())


    def bind(self, reply, callback, errback):
        """ bind ``reply`` object to a ``callback`` function which
            accepts a single pos. argument, representing the received data.
        """
        def formaterror(message):
            method = QT_OP_TO_HTTP_METHOD.get(reply.operation())
            url = reply.url().toDisplayString()
            return '({}) {} : {}'.format(method, url, message)

        def onFinished():
            self.query_count -= 1
            # handle returned data
            raw = reply.readAll()
            if raw.isEmpty():
                errback(formaterror('Server didn\'t return data.'))
                return
            content_type = reply.header(QNetworkRequest.ContentTypeHeader)
            if content_type != 'application/json':
                errback(formaterror('Server didn\'t return JSON.'))
                return
            try:
                data = json.loads(raw.data())
            except:
                errback(formaterror('returned JSON is not valid.'))
                return
            if callback.__code__.co_argcount > 0:
                callback(data)
            else:
                callback()

        self.query_count += 1
        if reply.isFinished():
            onFinished()
        else:
            reply.finished.connect(onFinished)


    def pending_reply(self):
        return self.query_count > 0