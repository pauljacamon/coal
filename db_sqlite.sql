PRAGMA foreign_keys = ON;

BEGIN TRANSACTION;

CREATE TABLE IF NOT EXISTS `affinities` (
	`id` INTEGER PRIMARY KEY,
	`name` TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS `workers` (
	`id` INTEGER PRIMARY KEY,
	`name` TEXT NOT NULL,
	`description` TEXT DEFAULT '',
	`is_working` INTEGER,
	`is_paused` INTEGER DEFAULT 0,
	`last_job` INTEGER DEFAULT -1,
	`affinity` INTEGER DEFAULT 0,
	`affinity2` INTEGER DEFAULT 0,
	`schedule` TEXT DEFAULT ''
);
CREATE TABLE IF NOT EXISTS `jobs` (
	`id` INTEGER PRIMARY KEY,
	`parent` INTEGER DEFAULT 0,
	`title` TEXT DEFAULT '',
	`command` TEXT DEFAULT '',
	`is_group` INTEGER DEFAULT 0,
	`is_subtask` INTEGER DEFAULT 0,
	`subtask_start` INTEGER DEFAULT 0,
	`subtask_end` INTEGER DEFAULT 0,
	`dir` TEXT DEFAULT '.',
	`environment` TEXT DEFAULT '',
	`state_id` INTEGER DEFAULT 0,
	`worker_id` INTEGER DEFAULT -1,
	`submit_time` INTEGER DEFAULT 0,
	`start_time` INTEGER DEFAULT 0,
	`duration` INTEGER DEFAULT 0,
	`run_done` INTEGER DEFAULT 0,
	`timeout` INTEGER DEFAULT 0,
	`priority` INTEGER DEFAULT 0,
	`affinity` INTEGER DEFAULT 0,
	`user` TEXT DEFAULT '',
	`finished` INTEGER DEFAULT 0,
	`errors` INTEGER DEFAULT 0,
	`working` INTEGER DEFAULT 0,
	`total` INTEGER DEFAULT 0,
	`total_finished` INTEGER DEFAULT 0,
	`total_errors` INTEGER DEFAULT 0,
	`total_working` INTEGER DEFAULT 0,
	`url` TEXT DEFAULT '',
	`progress` REAL,
	`log_filters` TEXT DEFAULT '',
	`h_affinity` INTEGER DEFAULT 0,
	`h_priority` INTEGER DEFAULT 0,
	`h_paused` INTEGER DEFAULT 0,
	`h_depth` INTEGER DEFAULT 0,
	`dependencies` TEXT DEFAULT ''
);
CREATE TABLE IF NOT EXISTS `dependencies` (
	`job_id` INTEGER NOT NULL,
	`dep_id` INTEGER NOT NULL,
	UNIQUE (`job_id` ,`dep_id`) ON CONFLICT IGNORE
);
CREATE TABLE IF NOT EXISTS `blacklist` (
	`job_id` INTEGER REFERENCES jobs(id) ON DELETE CASCADE,
	`worker_id` INTEGER REFERENCES workers(id) ON DELETE CASCADE,
	UNIQUE (`job_id` ,`worker_id`) ON CONFLICT IGNORE
);
CREATE TABLE IF NOT EXISTS `attempts` (
	`id` INTEGER PRIMARY KEY,
	`job_id` INTEGER REFERENCES jobs(id) ON DELETE CASCADE,
	`worker_id` INTEGER  NOT NULL,
	`state_id` INTEGER  NOT NULL,
	`start_time` INTEGER NOT NULL
);
CREATE INDEX IF NOT EXISTS `jobs_parent_index` ON `jobs` (
	`parent`
);
CREATE UNIQUE INDEX IF NOT EXISTS `workers_name_index` ON `workers` (
	`name`
);
CREATE INDEX IF NOT EXISTS `attempts_job_id_index` ON `attempts` (
	`job_id`
);
CREATE INDEX IF NOT EXISTS `blacklist_job_id_worker_id_index` ON `blacklist` (
	`job_id`, `worker_id`
);

COMMIT;
