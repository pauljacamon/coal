import sys
import os
import time
import subprocess
import socket
import traceback
import atexit
import shutil
import argparse
import json

from coalite.api.urllib3api import CoaliteAPI
from coalite import common, logger, db_sqlite
# from coalite.logger import cprint, BRIGHT, GREEN, RED

LOCAL_HOST = '127.0.0.1'

# list of processes to kill at the end of tests or tests abortion
to_kill = []

def launch_server(port):
	# remove previous database
	if os.path.exists(common.SQLITE_DB):
		try:
			os.remove(common.SQLITE_DB)
		except OSError:
			logger.cprint(logger.RED, 'Can\'t reset coalite.db, make sure it\'s not locked by another process!')
			sys.exit(1)

	db_sqlite.init()

	# remove previous logs
	if os.path.exists('logs'):
		shutil.rmtree('logs', ignore_errors=True)

	# spawn coalite server with test parameters
	cmd = [sys.executable, '-m', 'coalite.server', '--port', str(port)]
	logger.cprint(logger.CYAN, subprocess.list2cmdline(cmd))
	p = subprocess.Popen(cmd, creationflags=subprocess.CREATE_NEW_CONSOLE)
	to_kill.append(p)


def wait_for_server(port, seconds):
	dots = 0
	timeout = time.time() + seconds
	while time.time() < timeout:
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.settimeout(1)
		try:
			s.connect((LOCAL_HOST, port))
			s.close()
			sys.stdout.write(" " * 30 + "\r") # clear line
			return True
		except socket.timeout:
			dots = (dots % 3) + 1
			sys.stdout.write("Waiting for server{:<3}\r".format("."*dots))
		except socket.error as err:
			time.sleep(1)
		s.close()
	return False


def launch_worker(port, identifier):
	env = os.environ.copy()
	env['WORKER_NAME'] = identifier
	cmd = [sys.executable, '-m', 'coalite.worker', '--server', LOCAL_HOST, '--port', str(port), '--name', identifier]
	logger.cprint(logger.CYAN, subprocess.list2cmdline(cmd))
	p = subprocess.Popen(cmd, env=env, creationflags=subprocess.CREATE_NEW_CONSOLE)
	to_kill.append(p)


def wait_for_workers(api, seconds):
	dots = 0
	timeout = time.time() + seconds
	while time.time() < timeout:
		workers = api.collectWorkers()
		if len(workers) == 2:
			sys.stdout.write(' ' * 40 + '\r')
			return True
		dots = (dots % 3) + 1
		sys.stdout.write("Waiting for workers{:<3}\r".format("."*dots))
		time.sleep(1)
	return False


def kill_server_and_workers():
	for p in to_kill:
		if p.poll() is None:
			p.terminate()


def getSleepCmd(seconds):
	if sys.platform == 'win32':
		return 'ping 127.0.0.1 -n {} > nul'.format(seconds+1)
	else:
		return 'sleep {}'.format(seconds)


def test(func):
	def wrapper(*args, **kwargs):
		logger.cprint(logger.BRIGHT, "test {}...".format(func.__name__), newline=False)
		try:
			r = func(*args, **kwargs)
			logger.cprint(logger.GREEN, "ok")
			return r
		except AssertionError as err:
			message = str(err)
			if message:
				logger.cprint(logger.RED, "failed: ", message)
			else:
				logger.cprint(logger.RED, "failed")
		except Exception:
			logger.cprint(logger.RED, "error:")
			logger.cprint(logger.RED, traceback.format_exc())
	return wrapper

# TEST FUNCTIONS

@test
def get_job(api):
	job_id = api.newJob(title="[getjob] Paused Job", command="echo \"I'm useless!\"", state="PAUSED", priority=0)
	assert job_id is not None, '`api.newJob()` did\'t return a job id'

	job = api.getJob(job_id)
	assert job is not None, '`api.getJob()` returned `None`'
	assert job['id'] == job_id
	assert job['title'] == "[getjob] Paused Job"
	assert job['command'] == "echo \"I'm useless!\"", 'returned job command mismatch'
	assert job['state'] == "PAUSED", 'returned job state is not "PAUSED"'


@test
def priority(api):
	# create jobs with increasing priority
	job_ids = [
		api.newJob(title="[priority] Low Priority Job", command=getSleepCmd(2), user="pierre", state="PAUSED", priority=40),			# low priority
		api.newJob(title="[priority] Normal Priority Job", command=getSleepCmd(2), user="paul", state="PAUSED", priority=50),		# normal priority
		api.newJob(title="[priority] High Priority Job", command=getSleepCmd(2), user="josianne", state="PAUSED", priority=60),		# high priority
	]
	# unpause jobs
	api.resumeJobs(job_ids)

	testPassed = False
	timeout = time.time() + 30 # the test has 30s to proceed
	while time.time() < timeout:
		jobs = [api.getJob(id) for id in job_ids]
		if all(j['state'] == "FINISHED" for j in jobs):
			# assert the 3 jobs started according to their priority
			assert jobs[0]['start_time'] >= jobs[1]['start_time'], 'job #0 started before job #1'
			assert jobs[1]['start_time'] >= jobs[2]['start_time'], 'job #1 started before job #2'
			testPassed = True
			break
		time.sleep(1)

	assert testPassed, 'timeout'

@test
def affinities_first(api):
	"""
		setup:
			set worker 0 affinity to maya
			set worker 1 affinity to nuke
			create a high priority job with no affinity
			create 2 jobs with low affinity but each with one of the previously set affinities
		test:
			jobs with affinities should start before the 1st job, despite having a highest priority
	"""
	workers = api.collectWorkers()
	params = {
		workers[0]['id']: {'affinity': "maya"},
		workers[1]['id']: {'affinity': "nuke"},
	}
	api.editWorkers(params)

	job_ids = [
		api.newJob(title="[affinity-1st] No Affinity Job", command=getSleepCmd(2), state="PAUSED", priority=80),			# no affinity job, high priority
		api.newJob(title="[affinity-1st] Maya Job", command=getSleepCmd(2), state="PAUSED", affinity="maya", priority=20),	# maya job with low priority
		api.newJob(title="[affinity-1st] Nuke Job", command=getSleepCmd(2), state="PAUSED", affinity="nuke", priority=20),	# nuke job with low priority
	]
	# unpause jobs
	api.resumeJobs(job_ids)

	testPassed = False
	timeout = time.time() + 30 # the test has 30s to proceed
	while time.time() < timeout:
		jobs = [api.getJob(id) for id in job_ids]
		if all(j['state'] == "FINISHED" for j in jobs):
			# assert "no aff. job" started after maya and nuke jobs, although its high priority
			assert jobs[0]['start_time'] > jobs[1]['start_time'], 'job #0 started before job #1'
			assert jobs[0]['start_time'] > jobs[2]['start_time'], 'job #0 started before job #2'
			testPassed = True
			break
		time.sleep(1)

	assert testPassed, 'timeout'


@test
def secondary_affinities(api):
	"""
		setup:
			create 4 jobs
			job #1 : should start later even with high prio because "projetB,maya" are secondary affinities for worker 0 and 1
			job #2 : should start later even with high prio because "projetA,nuke" are secondary affinities for worker 0 and 1
			job #3 : should start sooner even with low prio because "projetA,maya" are primary affinities for worker 0
			job #4 : should start sooner even with low prio because "projetB,nuke" are secondary affinities for worker 1
		test:
			check that job #1 and #2 starts after #3 and #4
	"""
	workers = api.collectWorkers()
	params = {
		workers[0]['id']: { 'affinity': 'projetA,maya', 'affinity2': 'projetB' },
		workers[1]['id']: { 'affinity': 'projetB,nuke', 'affinity2': 'projetA' },
	}
	api.editWorkers(params)

	job_ids = [
		api.newJob(title='[secondary-affinities] projetB,maya', affinity='projetB,maya', command=getSleepCmd(2), state='PAUSED', priority=80),
		api.newJob(title='[secondary-affinities] projetA,nuke', affinity='projetA,nuke', command=getSleepCmd(2), state='PAUSED', priority=80),
		api.newJob(title='[secondary-affinities] projetA,maya', affinity='projetA,maya', command=getSleepCmd(2), state='PAUSED', priority=20),
		api.newJob(title='[secondary-affinities] projetB,nuke', affinity='projetB,nuke', command=getSleepCmd(2), state='PAUSED', priority=20),
	]
	# unpause jobs
	api.resumeJobs(job_ids)

	testPassed = False
	timeout = time.time() + 30 # the test has 30s to proceed
	while time.time() < timeout:
		jobs = [ api.getJob(id) for id in job_ids ]
		if all(j['state'] == "FINISHED" for j in jobs):
			assert jobs[0]['start_time'] > jobs[2]['start_time'], 'job #1 started before job #3'
			assert jobs[0]['start_time'] > jobs[3]['start_time'], 'job #1 started before job #4'
			assert jobs[1]['start_time'] > jobs[2]['start_time'], 'job #2 started before job #3'
			assert jobs[1]['start_time'] > jobs[3]['start_time'], 'job #2 started before job #4'
			testPassed = True
			break
		time.sleep(1)

	assert testPassed, 'timeout'


@test
def hierarchy_and_dependency(api):
	"""
		setup:
			- creates a paused job named "Dependent Job"
			- creates an empty job named "parent"
			- creates 10 children jobs to "parent", paused
			- set "Dependent Job" dependent of all children
			- unpause "Dependent Job"
			- unpause children
		tests:
			- "Dependent Job" state should switch to "PENDING" after dependencies is set
			- "Dependent Job" state should stay "PENDING" until parent job is "FINISHED"
			- "Dependent Job" state should start as soon as parent job is "FINISHED"
	"""
	dep_job_id, parent_id = api.newJobs([
		{'title': "[hiearchy-and-dependency] Dependent Job", 'command': "echo dependencies", 'state': "PAUSED"},
		{'title': "[hiearchy-and-dependency] Parent Job"},
	])
	children_ids = api.newJobs([ {'title': "[hiearchy-and-dependency] Child Job #{}".format(i), 'command': "echo 'job-{}'".format(i), 'parent': parent_id, 'state': 'PAUSED'} for i in range(10) ])

	# set depjob dependent of parentjob children
	api.setJobDependencies(dep_job_id, children_ids)

	# test #1: queried dep. jobs ids == children ids
	dep_jobs = api.getJobDependencies(dep_job_id)
	dep_jobs_ids = [job['id'] for job in dep_jobs]
	assert any(map(lambda v: v in children_ids, dep_jobs_ids)), 'dependent job is not depending on all children'

	# unpause dep job
	api.resumeJobs([dep_job_id])

	# test #2: dep. job state == PENDING
	dep_job = api.getJob(dep_job_id)
	assert dep_job['state'] == "PENDING", 'dependent job status is not PENDING ({})'.format(dep_job['state'])

	# unpause children
	children = api.getJobChildren(parent_id)
	api.resumeJobs([ child['id'] for child in children ])

	# test #3: dep.job should stay "PENDING" until parent job is "FINISHED"
	timeout = time.time() + 30
	parent_status = None
	while time.time() < timeout:
		dep_job = api.getJob(dep_job_id)
		parentJob = api.getJob(parent_id)
		parent_status = parentJob['state']
		if parent_status != "FINISHED":
			assert dep_job['state'] == "PENDING", 'dependent job status changed to {} before parent is finished'.format(dep_job['state'])
		else:
			break
		time.sleep(1)

	assert parent_status == 'FINISHED', 'parent job status is not FINISHED ({})'.format(parent_status)

	# test #4: dep.job should be picked and run in less than 30s
	timeout = time.time() + 30
	while time.time() < timeout:
		dep_job = api.getJob(dep_job_id)
		if dep_job['state'] == "FINISHED":
			break
		time.sleep(1)

	assert dep_job['state'] == "FINISHED", 'dependent job status is not FINISHED ({})'.format(dep_job['state'])


@test
def log_filters(api):
	python = sys.executable
	test_jobs_path = os.path.join(os.path.dirname(__file__), 'test_jobs.py').replace('\\', '/')
	test_logfilters_path = os.path.join(os.path.dirname(__file__), 'test.logfilters').replace('\\', '/')
	env = 'COALITE_LOG_FILTERS=' + test_logfilters_path
	progressJobId = api.newJob(title="[logfilters] Progress Job", command='"{}" -u "{}" progress'.format(python, test_jobs_path), state="WAITING", priority=20, environment=env)
	errorJobId = api.newJob(title="[logfilters] Error Job", command='"{}" -u "{}" error'.format(python, test_jobs_path), state="WAITING", priority=20, environment=env)
	cancelJobId = api.newJob(title="[logfilters] Cancel Job", command='"{}" -u "{}" cancel'.format(python, test_jobs_path), state="WAITING", priority=20, environment=env)

	progressJobOk = False
	errorJobOk = False
	cancelJobOk = False
	timeout = time.time() + 30

	while time.time() < timeout:

		progressJob = api.getJob(progressJobId)
		if not progressJobOk and progressJob['state'] == "FINISHED":
			# expects the job to end at 80%
			assert progressJob['progress'] == 0.8, 'progress did not stop at 80%'
			progressJobOk = True

		errorJob = api.getJob(errorJobId)
		if errorJob['state'] not in ("WAITING", "WORKING"):
			# assert that error pattern has been found in the job's log, and that the server changed it's state accordingly
			assert errorJob['state'] == "ERROR", 'job did not changed to ERROR'
			errorJobOk = True

		cancelJob = api.getJob(cancelJobId)
		if cancelJob['state'] not in ("WAITING", "WORKING"):
			# assert that cancel pattern has been found in the job's log, and that the server changed it's state accordingly
			assert cancelJob['state'] == "CANCELED", 'job did not changed to CANCELED'
			cancelJobOk = True

		if progressJobOk and errorJobOk and cancelJobOk: return

		time.sleep(1)

	assert False, 'timeout'

	testPassed = False
	timeout = time.time() + 30
	while time.time() < timeout:
		cancelJob = api.getJob(cancelJobId)
		if cancelJob['state'] in ("FINISHED", "ERROR", "CANCELED"):
			# assert that cancel pattern has been found in the job's log, and that the server changed it's state accordingly
			assert cancelJob['state'] == "CANCELED", 'job did not changed to CANCELED'
			testPassed = True
			break
		time.sleep(1)
	assert testPassed, 'timeout'


@test
def subtasks(api):

	job_id = api.newJob(title='[subtasks] parent', command='echo start=START end=END', is_group=True)
	task_ids = [ api.newJob(title='[subtasks] subtask #{}'.format(i), parent=job_id, is_subtask=True, subtask_start=i, subtask_end=i) for i in range(10) ]

	testPassed = False
	timeout = time.time() + 10 # the test has 10s to proceed
	while time.time() < timeout:
		tasks = [ api.getJob(id) for id in task_ids ]
		if all(j['state'] == "FINISHED" for j in tasks):
			# get task #5 log data
			attempts = api.collectAttempts(tasks[5]['id'])
			assert len(attempts) > 0, 'No attempt found in DB'
			assert len(attempts) < 2, 'More than 1 attempt found in DB'
			log_data = api.getLog(tasks[5]['id'], attempts[0]['id'])
			assert log_data is not None, 'Can\'t fetch log data'
			# check that log data contains the output "start=5 end=5"
			assert any(line == 'start=5 end=5' for line in log_data['log'].splitlines())
			testPassed = True
			break
		time.sleep(1)

	assert testPassed, 'timeout'


@test
def stresstest(api, group_count=200, child_count=200):
	group_ids = []
	for i in range(group_count):
		group_id = api.newJob(title="[stresstest] Group #{}".format(i+1))
		api.newJobs([ {'title':"[stresstest] Child #{}".format(j+1), 'command':"echo \"I'm job #{}, my parent is #{}\"".format(j+1, group_id), 'parent':group_id} for j in range(child_count) ])
		group_ids.append(group_id)
	dep_job_id = api.newJob(title="[stresstest] Ultimate Job", dependencies=group_ids, command="echo \"ULTIMATE!\"")

	dep_job = api.getJob(dep_job_id)
	assert dep_job['state'] == "PENDING"

	while True:
		dep_job = api.getJob(dep_job_id)
		if dep_job['state'] == "FINISHED":
			break
		time.sleep(1)


@test
def attempts_and_blacklist(api):
	job_ids = api.newJobs([
		{'title': '[attempts-and-blacklist] Error Job', 'command': 'exit 1'},
		{'title': '[attempts-and-blacklist] Good Job', 'command': 'exit 0'},
	])
	time.sleep(5)

	r = api.collectBlacklistedWorkers(job_ids[0])
	assert len(r) == 2, 'should be 2 workers in "error" job blacklist'

	r = api.collectAttempts(job_ids[0])
	assert len(r) == 2, 'should be 2 attempts for "error" job'

	r = api.collectBlacklistedWorkers(job_ids[1])
	assert len(r) == 0, 'should not be any worker in "good" job blacklist'

	r = api.collectAttempts(job_ids[1])
	assert len(r) == 1, 'should be a single attempt for "good" job'


@test
def move_jobs(api):
	assert False, 'test is not ready!'


@test
def worker_timeout(api):
	"""
	Test that when a worker timeouts (it as stopped for some reason)
	worker is correctly set to TIMEOUT and running jobs are set to ERROR
	"""
	assert False, 'test is not ready!'


@test
def setup_get_api_test_jobs(api):
	dep_job_id, parent_id = api.newJobs([
		{'title': '[GET-methods] Post Job', 'state': 'PAUSED', 'command': 'exit 0'},
		{'title': '[GET-methods] Parent Job'},
	])
	children_ids = api.newJobs([ {'title': '[GET-methods] Child Job #{}'.format(i+1), 'command': 'exit 0'.format(i), 'parent': parent_id, 'state': 'PAUSED', 'user': 'jacqueline'} for i in range(2) ])

	api.setJobDependencies(dep_job_id, children_ids)
	api.resumeJobs([dep_job_id] + children_ids)

	# wait for jobs to finish
	timeout = time.time() + 10
	while time.time() < timeout:
		dep_job = api.getJob(dep_job_id)
		if dep_job['state'] == 'FINISHED':
			break
		time.sleep(1)

	assert dep_job['state'] == 'FINISHED'

	return dep_job_id, parent_id


@test
def api_get_infos(api):
	r = api.getInfos()


@test
def api_collect_jobs(api):
	r = api.collectJobs({})


@test
def api_get_job(api, job_id):
	r = api.getJob(job_id)


@test
def api_get_job_children(api, job_id):
	r = api.getJobChildren(job_id)


@test
def api_get_log(api, job_id):
	r = api.getLog(job_id, 1, pos=0)


@test
def api_collect_users(api):
	r = api.collectUsers()


@test
def api_collect_workers(api):
	r = api.collectWorkers()


@test
def api_get_job_dependencies(api, job_id):
	r = api.getJobDependencies(job_id)
	fmt_r = ','.join(j['title'] for j in sorted(r, key=lambda x: x['id']))
	assert fmt_r == '[GET-methods] Child Job #1,[GET-methods] Child Job #2'


@test
def api_collect_affinities(api):
	r = api.collectAffinities()
	fmt_r = ','.join(a['name'] for a in sorted(r, key=lambda x: x['id']) if a['name'])
	assert fmt_r == 'maya,nuke,projetA,projetB'


def main(args):
	# disable this line to keep server and workers running
	# atexit.register(kill_server_and_workers)
	# init server
	os.chdir(args.working_dir)
	launch_server(args.port)

	if not wait_for_server(args.port, 60):
		print("Server failed to start.")
		return


	for i in range(2):
		launch_worker(args.port, "TEST-WORKER-{}".format(i))

	api = CoaliteAPI(LOCAL_HOST, args.port)

	api.setAffinities({"1": "maya", "2": "nuke", "3":"projetA", "4":"projetB"})

	if not wait_for_workers(api, 60):
		print("Workers failed to start.")
		return


	# RUN TESTS
	# ==========================

	# get_job(api)
	# priority(api)
	# affinities_first(api)
	# secondary_affinities(api)
	# subtasks(api)
	# hierarchy_and_dependency(api)
	log_filters(api)
	# attempts_and_blacklist(api) # to finish
	# move_jobs(api) # to do!
	# worker_timeout(api) # to do!

	# stresstest(api, 200, 200)

	# API GET METHODS TESTS
	# ids = setup_get_api_test_jobs(api)
	# if ids:
	# 	api_get_infos(api)
	# 	api_collect_jobs(api)
	# 	api_get_job(api, ids[0])
	# 	api_get_job_children(api, ids[1])
	# 	api_get_log(api, ids[0])
	# 	api_collect_users(api)
	# 	api_collect_workers(api)
	# 	api_get_job_dependencies(api, ids[0])
	# 	api_collect_affinities(api)

	print("done")


if __name__ == "__main__":

	parser = argparse.ArgumentParser(description="Coalition unit-tests.")
	parser.add_argument("-p", "--port", metavar="NUMBER", type=int, required=True, help=f"unit-tests port")
	parser.add_argument('-w', '--working-dir', metavar='PATH', required=True, help='unit-tests working directory')

	args = parser.parse_args()
	main(args)