import sys, time

def progressJob():
	''' print progress from 0 to 100% '''
	for i in range(100):
		time.sleep(0.02)
		print(f'Hello, I\'m at {i+1}% !!')
	sys.exit(0)


def errorJob():
	''' print a pretty bad error at 40% '''
	for i in range(10):
		if i == 5:
			print('Something pretty bad happened!')
		time.sleep(1)
		print(f'PROGRESS:{(i+1)*0.1:.01f}')
	sys.exit(0)

def cancelJob():
	''' print a really bad error at 70% '''
	for i in range(10):
		if i == 5:
			print('Something really bad happened!')
		time.sleep(1)
		print(f'PROGRESS:{(i+1)*0.1:.01f}')
	sys.exit(0)


if __name__ == '__main__':
	if len(sys.argv) == 2:
		if sys.argv[1] == 'progress':
			progressJob()
		elif sys.argv[1] == 'error':
			errorJob()
		elif sys.argv[1] == 'cancel':
			cancelJob()
	print('wrong use of test jobs!')
	sys.exit(1)
