#!/usr/bin/env python
# coding: utf-8

# builtins
import sys, os, re
import io
import socket
import time
import subprocess
import base64
import argparse
import http.client
import threading
import random
import json
import traceback
import glob

# 3rd party
import urllib3
from urllib3.exceptions import MaxRetryError
import psutil

# relatives
from . import logfilter, logger, common
rs = common.RUN_STATES
ft = common.LOGFILTER_TYPES

WORKER_COLORS = {
    1: logger.CYAN,
    2: logger.GREEN,
    3: logger.YELLOW,
    4: logger.MAGENTA,
}
HIGHLIGHT_COLORS = { ft.STOP : logger.RED, ft.PROGRESS : logger.GREEN }

NVSMI_LOC = [
    'T:/ext/apps/nvidia-smi-471.35/nvidia-smi.exe',
    'C:/Program Files/NVIDIA Corporation/NVSMI/nvidia-smi.exe',
    'C:/Windows/System32/DriverStore/FileRepository/*/nvidia-smi.exe'
]
NVSMI_ARGS = ['--query-gpu=index,utilization.gpu,memory.free,memory.total', '--format=csv,noheader,nounits', '--loop=1']

LOCK_PATH = os.path.join(os.getenv('TMP'), '.coal-worker.lock')
REGEX_ENV_VAR = re.compile(r'%([A-Z-a-z0-9_]*)%|\$\{([A-Z-a-z0-9_]*)\}|\$\(([A-Z-a-z0-9_]*)\)|\$([A-Z-a-z0-9_]*)')


def replace(string, pattern, **tokens):
    def repl(m):
        return tokens.get(m.group(1), m.group())
    return re.sub(pattern, repl, string)


def shuffle(t):
    return t * (1.0 + (random.random()-0.5)*0.2)


def getstats():
    mem = psutil.virtual_memory()
    return psutil.cpu_percent(percpu=True), mem.available/(1024**2), mem.total/(1024**2)


def expandvars(path, env):
    def repl(m):
        for name in m.groups():
            if name:
                return env.get(name, m.group())

    return re.sub(REGEX_ENV_VAR, repl, path)


def kill_process_tree(pid):
    try:
        parent = psutil.Process(pid)
    except:
        return

    processes = parent.children(recursive=True)
    processes.append(parent)

    for p in processes:
        # logger.debug('terminate pid #{}...'.format(p.pid))
        try:
            p.terminate()
        except psutil.NoSuchProcess:
            pass
        except psutil.AccessDenied:
            pass

    gone, alive = psutil.wait_procs(processes, timeout=3)

    for p in alive:
        # logger.debug('kill pid #{}...'.format(p.pid))
        try:
            p.kill()
        except psutil.NoSuchProcess:
            pass


def get_fdate(seconds):
    return time.strftime("%d/%m/%y %H:%M:%S", time.localtime(seconds))


def get_fduration(seconds):
    M, S = divmod(seconds, 60)
    if M == 0:
        return "{}s".format(int(S))
    H, M = divmod(M, 60)
    if H == 0:
        return "{}m{:02d}s".format(int(M), int(S))

    return "{}h{:02d}m{:02d}s".format(int(H), int(M), int(S))


def get_nvsmi_process():
    candidates = []
    for pattern in NVSMI_LOC:
        for path in glob.glob(pattern):
            p = subprocess.Popen([path] + NVSMI_ARGS, stdout=subprocess.PIPE)
            # test if the process outputs valid data (comma-separated digits)
            stdout = p.stdout.readline().strip()
            if all(i.strip().isdigit() for i in stdout.split(b',') ):
                return p
            p.terminate()


def iteroutput(p):
    # read during process
    while p.poll() is None:
        yield p.stdout.readline()
    # remaining lines
    for l in p.stdout.readlines():
        yield l

class GPUStatsThread(threading.Thread):

    def __init__(self):
        self.data = {}
        self.lock = threading.Lock()
        super(GPUStatsThread, self).__init__()


    def run(self):
        p = get_nvsmi_process()
        if p is None:
            logger.warning('Could not locate NVSMI: GPU stats won\'t be sent to server')
            return

        while p.poll() is None:
            stdout = p.stdout.readline().strip()
            try:
                gpu = [ int(i.strip()) for i in stdout.split(b',') ]
            except:
                continue
            with self.lock:
                self.data[gpu[0]] = gpu[1:]


    def get(self):
        gpu_load, free_mem, total_mem = [], [], []
        with self.lock:
            for i in sorted(self.data):
                gpu = self.data[i]
                gpu_load.append(gpu[0])
                free_mem.append(gpu[1])
                total_mem.append(gpu[2])
        return gpu_load, free_mem, total_mem


class JobThread(threading.Thread):

    def __init__(self, jobdata, color):
        super().__init__()

        self.jobdata = jobdata
        self.color = color

        # update job environment
        self.env = os.environ.copy()
        for line in jobdata['environment'].splitlines():
            key, _, value = line.partition('=')
            if key and value:
                self.env[key] = value

        self.cmd = expandvars(jobdata['command'], self.env)
        # self.dir = expandvars(jobdata['dir'], self.env)

        self.pid = 0
        self.exitcode = -1
        self.abort = False
        self.log = io.StringIO()
        self.lock = threading.Lock()
        self.finished = threading.Event()

        # init job log filters
        if 'COALITE_LOG_FILTERS' in self.env:
            self.logfilter = logfilter.LogFilterer(self.env['COALITE_LOG_FILTERS'])
        else:
            self.logfilter = logfilter.LogFilterer(os.path.join(os.path.dirname(__file__), 'default.logfilters'))


    def run(self):
        # if self.dir:
        #     try:
        #         os.chdir(self.dir)
        #     except OSError:
        #         self.writelog("[worker] Can't change dir to {}".format(self.dir))
        #         self.finished.set()
        #         return

        # Run the job
        start_ts = time.time()
        self.writelog("[worker] start: {}".format(get_fdate(start_ts)))
        self.writelog("[worker] command: {}".format(self.cmd))

        if not self.abort:

            p = subprocess.Popen(self.cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env=self.env)

            # Get the pid
            self.pid = int(p.pid)

            for line in iteroutput(p):
                self.writelog(line.decode(errors='ignore').strip())

            # Get the error code of the job
            self.exitcode = p.poll()

        end_ts = time.time()
        self.writelog("[worker] end: {}".format(get_fdate(end_ts)))
        self.writelog("[worker] total time: {}".format(get_fduration(end_ts - start_ts)))
        self.writelog("[worker] exit code: {}".format(self.exitcode))

        self.finished.set()


    def writelog(self, line):
        # scan output
        lftype, start, end = self.logfilter.scan(line)
        # write log to stdout
        if lftype != ft.NULL:
            # highlight logfilter matches in the worker output
            highlight = HIGHLIGHT_COLORS.get(lftype, logger.RESET)
            logger.cprint(self.color, '~ ', logger.RESET, line[:start], highlight, line[start:end], logger.RESET, line[end:])
        else:
            logger.cprint(self.color, '~ ', logger.RESET, line)
        # write log to send to server
        with self.lock:
            self.log.write(line)
            self.log.write('\n')


    def readlog(self):
        with self.lock:
            log = self.log.getvalue()   # fetch current log content
            self.log = io.StringIO()    # make a new empty log object
        return log


    def progress(self):
        with self.lock:
            return self.logfilter.progress[0]


    def logerror(self):
        with self.lock:
            return self.logfilter.error[0]


    def stop(self, message):
        self.writelog("[worker] {}".format(message))
        self.abort = True
        if self.pid > 0:
            kill_process_tree(self.pid)


class Worker(threading.Thread):

    def __init__(self, name, num, server, port, gpu_stats):
        super().__init__()
        self.name = name
        self.color = WORKER_COLORS.get(num, logger.RESET)

        self.heartbeat_interval = 1

        # self.conn = http.client.HTTPConnection(server, port)

        self.poolmanager = urllib3.PoolManager()
        self.base_url = 'http://{}:{}'.format(server, port)
        self.headers = {'Content-Type': 'application/json'}

        self.gpu_stats = gpu_stats



    def post(self, path, params):
        params = json.dumps(params)
        while True:
            try:
                resp = self.poolmanager.request('POST', self.base_url + path, body=params, headers=self.headers)
                return json.loads(resp.data)
            except Exception as err:
                logger.error(err)


    def workerStats(self):
        cpu_load, free_memory, total_memory = getstats()
        gpu_load, gpu_free_mem, gpu_total_mem = self.gpu_stats.get()
        return {
            'worker_name': self.name,
            'cpu': cpu_load,
            'free_memory': free_memory,
            'total_memory': total_memory,
            'gpu': gpu_load,
            'gpu_free_mem': gpu_free_mem,
            'gpu_total_mem': gpu_total_mem,
        }


    def writelog(self, message):
        logger.cprint(self.color, '[{}] '.format(self.name), logger.RESET, message)


    def pickJob(self):
        cpu_load, free_memory, total_memory = getstats()
        gpu_load, gpu_free_mem, gpu_total_mem = self.gpu_stats.get()
        stats = self.workerStats()
        job_data = self.post('/w/pickjob', stats)

        if job_data is None:
            return False

        # jobenv = os.environ.copy()
        # for line in job_data['environment'].splitlines():
        #     name, _, value = line.partition('=')
        #     if name and value:
        #         jobenv[name] = value

        self.writelog("start job #{} \"{}\"".format(job_data['id'], job_data['title']))

        # Launch a new thread to run the process
        job = JobThread(job_data, self.color)
        job.start()

        while job.is_alive():

            if job.abort:
                self.writelog('waiting for job thread to terminate...')
                job.join() # blocks until thread terminates properly, writes "end" log, etc.
                break

            log = job.readlog()

            # send "heartbeat" to server
            hb_data = self.workerStats()
            hb_data.update({
                'job_id': job_data['id'],
                'attempt_id': job_data['attempt_id'],
                'progress': job.progress(),
                # 'logerror': job.logerror(),
                'log': base64.b64encode(log.encode()).decode(),
            })
            result = self.post('/w/heartbeat', hb_data)
            if result != rs.OK:
                job.stop('server request to stop the job...')
            elif job.logerror() == ft.ERROR:
                job.stop('log filter request to stop the job...')
            elif job.logerror() == ft.CANCEL:
                job.stop('log filter request to cancel the job...')

            # sleep, or exit immediatly if "finished" event is set
            if job.finished.wait(shuffle(self.heartbeat_interval)):
                break

        # send last batch of stats/log to server
        endjob_stats = self.workerStats()
        log = job.readlog()
        # tell the server the job is finished
        endjob_stats.update({
            'job_id': job_data['id'],
            'attempt_id': job_data['attempt_id'],
            'progress': job.progress(),
            'logerror': job.logerror(),
            'exitcode': job.exitcode,
            'log': base64.b64encode(log.encode()).decode()
        })
        self.post('/w/endjob', endjob_stats)

        self.writelog("done job #{} \"{}\" ({})".format(job_data['id'], job_data['title'], job.exitcode))
        return True


    def run(self):
        dots = 0
        # main loop
        while True:
            if self.pickJob():
                # don't wait before picking a new job after successful result
                continue
            dots = (dots % 3) + 1
            sys.stdout.write("Waiting for a job {:<3}\r".format("."*dots))
            time.sleep(shuffle(self.heartbeat_interval))


class lock(object):

    def __init__(self):
        if os.path.exists(LOCK_PATH):
            try:
                os.remove(LOCK_PATH)
            except:
                raise Exception("Another worker seems to be running!")

    def __enter__(self):
        self.file = open(LOCK_PATH, "w")

    def __exit__(self, type, value, traceback):
        self.file.close()


def main():

    host = os.environ["COALITE_HOST"]
    port = os.environ["COALITE_PORT"]
    name = socket.gethostname()

    parser = argparse.ArgumentParser(description="Start a Coalition worker.")
    parser.add_argument("-s", "--server", metavar="NAME/IP", dest='host', default=host, help="Server name/ip")
    parser.add_argument("-p", "--port", metavar="PORT", type=int, default=int(port), help=f"Server port")
    parser.add_argument("-i", "--instances", metavar="NUMBER", type=int, default=1, help=f"Instances count")
    parser.add_argument("-n", "--name", metavar="NUMBER", default=name, help=f"Worker name")
    args = parser.parse_args()

    # ensure another worker is not already running
    with lock():
        threads = []
        gpu_stats = GPUStatsThread()
        gpu_stats.start()
        # TODO: implement multi instances here
        for i in range(args.instances):
            if args.instances > 1:
                name = '{}~{}'.format(args.name, i+1)
            else:
                name = args.name
            wrk = Worker(name, i+1, args.host, args.port, gpu_stats)
            wrk.start()
            threads.append(wrk)

        # Wait for all worker threads to complete
        for thread in threads:
            thread.join()


if __name__ == "__main__":
    main()



